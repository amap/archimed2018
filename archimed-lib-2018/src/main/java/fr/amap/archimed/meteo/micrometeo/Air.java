/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo.micrometeo;

/**
 * Air properties, including water vapour functions
 * @author dauzat
 */
public class Air
{
    /**
     * Get the psychrometric constant (noted gamma) [kPa °C-1] from atmospheric pressure [kPa]. (More
     * details :
     * <a href = https://en.wikipedia.org/wiki/Psychrometric_constant>Wikipedia</a>)
     *
     * @param atmPressure: atmospheric pressure [kPa]
     * @return the psychrometric constant [kPa °C-1]      
     * <p>gamma = (Cp * P) / (e * lambda), with:
     * <ul style=list-style-type:none>
     * <li>Cp = specific heat of air at constant pressure, 1.013 10-3 [MJ kg-1 °C-1]</li>
     * <li>P = atmospheric pressure [kPa]</li>
     * <li>e = ratio molecular weight of water vapor/dry air = 0.622</li>
     * <li>lambda = latent heat of vaporization at 20°C, 2.45 [MJ kg-1]</li>
     * </ul>
     * </p>
     */
    public static double psychrometricConstant( double atmPressure )
    {
        return 0.647417 * atmPressure / 1000;
    }

    /**
    * Get saturation vapour pressure (es).
    * @param tC temperature in degrees Celsius
    * @return water vapour pressure [kPa]
    */
    public static float waterSaturationVapourPressure( double tC )
    {
        return (float)(0.6108 * Math.exp( 17.27 * tC / (tC + 237.3) ));
    }

    /**
     * Get actual water vapor pressure, vapour pressure exerted by the water in the air.
     * @param rH relative humidity (percentage between 0 and 100)
     * @param tC air temperature in degrees Celsius
     * @return actual water vapor pressure [kPa]
     */
    public static double waterVaporPressureFromRh( double rH, double tC )
    {
        return (rH / 100) * waterSaturationVapourPressure( tC );
    }

    /**
     * Get slope of saturation vapour pressure curve at air temperature tK.
     * @param tC: air temperature in Kelvin
     * @return slope [kPa °C-1]
     */
    public static double slopeOfSaturationVapourPressureCurve( double tC )
    {
        return (4098. * waterSaturationVapourPressure( tC )) / Math.pow( (tC + 237.3), 2 );
    }

    /**
     * @param tCleaf	leaf temperature	[°C]
     * @param tCCair	air temperature		[°C]
     * @param rh	relative humidity	[%]
     * @return vpd	vapor pressure deficit	[kPa]
     */
    public static double vpdAirLeafFromRh( double tCleaf, double tCCair, double rh )
    {
        double VPD = (0.6108 * Math.exp( 17.27 * (tCleaf) / (tCleaf + 237.3) ));
        VPD -= (0.6108 * Math.exp( 17.27 * (tCCair) / (tCCair + 237.3) )) * rh / 100;
        return VPD;
    }

}
