/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo;

import fr.amap.archimed.meteo.micrometeo.Air;

/**
 * Contains meteo informations for a time period.
 *
 * @author dauzat
 */
public class MeteoRecord
{
    //    MeteoData data;

    private double airTemperature; // °C

    private double clearness; // 0 (covered) to 0.75 (clear)

    private double relativeHumidity; // % 

    private double wind; // km/h

    //    static double atmPressure= 101.3;   // atmospheric pressure [kPa]
    //    static double gamma= 0.647417;      // default value for pressure= 1atm

    /** Constructor.
     * 
     * @param data stores all the meteo data for each step
     * @param airTemperature air temperature (°C)
     * @param clearness sky clearness
     * @param relativeHumidity relative humidity (%)
     * @param wind wind speed (km/h)
     */
    //    public MeteoRecord( MeteoData data, double airTemperature, double clearness, double relativeHumidity, double wind )
    public MeteoRecord( double airTemperature, double clearness, double relativeHumidity, double wind )
    {
        //        this.data = data;
        this.airTemperature = airTemperature;
        this.clearness = clearness;
        this.relativeHumidity = relativeHumidity;
        this.wind = wind;
    }

    /** Get the air temperature (°C).
     * 
     * @return the air temperature
     */
    public double getAirTemperature()
    {
        return airTemperature;
    }

    /** Get the clearness.
     * 
     * @return the clearness
     */
    public double getClearness()
    {
        return clearness;
    }

    /** Get the relative humidity (%).
     * 
     * @return the relative humidity
     */
    public double getRelativeHumidity()
    {
        return relativeHumidity;
    }

    /** Get the wind (km/h).
     * 
     * @return the wind
     */
    public double getWind()
    {
        return wind;
    }

    //    /** Get the atmospheric pressure from the MeteoData attribute.
    //     * 
    //     * @return the atmospheric pressure
    //     */
    //    public float getAtmPressure()
    //    {
    //        return data.getAtmPressure();
    //    }

    //    /** Get the psychometric constant from the MeteoData attribute.
    //     * 
    //     * @return the psychometric constant
    //     */
    //    public float getPsychrometricConstant()
    //    {
    //        return data.getPsychrometricConstant();
    //    }

    /** Computes and returns the water vapor pressure from the rh and airTemperature.
     * 
     * @return the water vapor pressure
     */
    public double getWaterVaporPressure()
    {
        return Air.waterVaporPressureFromRh( relativeHumidity, airTemperature );
    }
}
