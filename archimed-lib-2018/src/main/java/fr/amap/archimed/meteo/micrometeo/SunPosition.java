/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo.micrometeo;

import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.util.CoordinateSystem;
import fr.amap.archimed.math.geometry.util.CoordinatesConversion;

/**
 *
 * @author Julien Heurtebize
 */
public class SunPosition
{

    /* TODO : 
     * implements methods to get position of the sun in latitude and longitude
     */

    private Vector3f direction;

    private double zenith;

    private double elevation;

    private double azimut;

    private boolean updated;

    /**
     *
     * @return The sun direction from an earth's observer to atmosphere.
     */
    public Vector3f getDirection()
    {
        if ( !updated )
        {
            updateDirection();
            updated = true;
        }

        return direction;
    }

    /**
     *
     * @return zenith in radians
     */
    public double getZenith()
    {
        return zenith;
    }

    /**
     *
     * @return elevation in radians
     */
    public double getElevation()
    {
        return elevation;
    }

    /**
     *
     * @return azimut in radians
     */
    public double getAzimut()
    {
        return azimut;
    }

    public final void setZenith( double zenith )
    {
        this.zenith = zenith;
        elevation = Math.PI / 2.0 - this.zenith;
        updated = false;
        //firePositionChanged();
    }

    public void setElevation( double elevation )
    {
        this.elevation = elevation;
        zenith = (Math.PI / 2.0) - this.elevation;
        updated = false;
        //firePositionChanged();
    }

    public final void setAzimut( double azimut )
    {
        this.azimut = azimut;
        updated = false;
        //firePositionChanged();
    }

    private void updateDirection()
    {
        direction = CoordinatesConversion.polarToCartesian( zenith, azimut, CoordinateSystem.COMPASS );
    }
}
