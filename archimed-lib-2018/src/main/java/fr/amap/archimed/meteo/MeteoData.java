/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import fr.amap.archimed.meteo.micrometeo.Air;
import fr.amap.archimed.meteo.micrometeo.Atmosphere;
import fr.amap.archimed.time.Time;
import fr.amap.archimed.time.TimePeriod;

/**
 * Object that contains most of the informations about the climate of the scene.
 * Data are retrivied from a .csv file filled by the user beforehand, cf paramètre meteoFile dans ArchimedConfiguration.properties.
 *
 * @author Florian Hilaire
 */
public class MeteoData
{
    //static values
    private final double latitude; // °

    private final double altitude; // m

    private final float atmPressure; // atmospheric pressure [kPa]

    /**
     * Début et fin de la simulation, récupérés depuis le fichier météo.
     */
    private Time startTime;

    private Time endTime;

    /**
     * Détail des lignes du fichier méteo.
     */
    Map<TimePeriod, MeteoRecord> meteoData;

    /** Constructor.
     * 
     * @param altitude altitude (°)
     * @param latitude latitute (°)
     */
    public MeteoData( double altitude, double latitude )
    {
        this.altitude = altitude;
        this.latitude = latitude;
        this.atmPressure = (float)Atmosphere.pressureAtAltitude( altitude );
    }

    /** Reads the meteo.csv file and gets all the hourly data from there.
     * 
     * @param path csv file path
     * @throws FileNotFoundException if the file is not found
     * @throws IOException if reading the file fails
     */
    public void readMeteoFile( String path ) throws FileNotFoundException, IOException
    {
        meteoData = new LinkedHashMap<>(); // Linked because we need to keep the insertion order to get the timestep
        try (BufferedReader br = new BufferedReader( new FileReader( path ) ))
        {
            String line;
            String split = ";";
            line = br.readLine(); // skip the header
            Time startTime = null, endTime = null;
            int year = 0;
            int month = 0;
            int day = 0;
            int i = 0;
            while ( (line = br.readLine()) != null )
            {
                String[] csvLine = line.split( split, 7 );
                String hourStartString = csvLine[ 1 ].substring( 0, csvLine[ 1 ].indexOf( ":" ) );
                int hourStart = Integer.parseInt( hourStartString );
                String minuteStartString = csvLine[ 1 ].substring( csvLine[ 1 ].indexOf( ":" ) + 1, csvLine[ 1 ].lastIndexOf( ":" ) );
                int minuteStart = Integer.parseInt( minuteStartString );
                String secondStartString = csvLine[ 1 ].substring( csvLine[ 1 ].lastIndexOf( ":" ) + 1, csvLine[ 1 ].length() );
                int secondStart = Integer.parseInt( secondStartString );
                if ( csvLine[ 0 ] != null && !"".equals( csvLine[ 0 ] ) )
                {
                    String yearString = csvLine[ 0 ].substring( 0, csvLine[ 0 ].indexOf( "/" ) );
                    year = Integer.parseInt( yearString );
                    String monthString = csvLine[ 0 ].substring( csvLine[ 0 ].indexOf( "/" ) + 1, csvLine[ 0 ].lastIndexOf( "/" ) );
                    month = Integer.parseInt( monthString );
                    String dayString = csvLine[ 0 ].substring( csvLine[ 0 ].lastIndexOf( "/" ) + 1, csvLine[ 0 ].length() );
                    day = Integer.parseInt( dayString );
                }
                startTime = new Time( year, month, day, hourStart, minuteStart, secondStart );
                if ( i == 0 )
                {
                    this.startTime = startTime;
                }
                String hourEndString = csvLine[ 2 ].substring( 0, csvLine[ 2 ].indexOf( ":" ) );
                int hourEnd = Integer.parseInt( hourEndString );
                String minuteEndString = csvLine[ 2 ].substring( csvLine[ 2 ].indexOf( ":" ) + 1, csvLine[ 2 ].lastIndexOf( ":" ) );
                int minuteEnd = Integer.parseInt( minuteEndString );
                String secondEndString = csvLine[ 2 ].substring( csvLine[ 2 ].lastIndexOf( ":" ) + 1, csvLine[ 2 ].length() );
                int secondEnd = Integer.parseInt( secondEndString );
                if ( csvLine[ 0 ] != null && !"".equals( csvLine[ 0 ] ) )
                {
                    String yearString = csvLine[ 0 ].substring( 0, csvLine[ 0 ].indexOf( "/" ) );
                    year = Integer.parseInt( yearString );
                    String monthString = csvLine[ 0 ].substring( csvLine[ 0 ].indexOf( "/" ) + 1, csvLine[ 0 ].lastIndexOf( "/" ) );
                    month = Integer.parseInt( monthString );
                    String dayString = csvLine[ 0 ].substring( csvLine[ 0 ].lastIndexOf( "/" ) + 1, csvLine[ 0 ].length() );
                    day = Integer.parseInt( dayString );
                }
                endTime = new Time( year, month, day, hourEnd, minuteEnd, secondEnd );

                TimePeriod period = new TimePeriod( startTime, endTime );
                double temp = Double.parseDouble( csvLine[ 3 ] ); //temperature
                double rh = Double.parseDouble( csvLine[ 4 ] ); //relativeHumidity
                double clrn = Double.parseDouble( csvLine[ 5 ] ); //clearness
                double wind = Double.parseDouble( csvLine[ 6 ] ); //wind
                MeteoRecord record = new MeteoRecord( temp, clrn, rh, wind );
                meteoData.put( period, record );
                i++;
            }
            this.endTime = endTime;
            br.close();
        }
    }

    /** Gets the altitude.
     * 
     * @return the altitude
     */
    public double getAltitude()
    {
        return altitude;
    }

    /** Gets the latitude.
     * 
     * @return the latitude
     */
    public double getLatitude()
    {
        return latitude;
    }

    /** Gets the first time of the simulation.
     * 
     * @return the start time
     */
    public Time getStartTime()
    {
        return startTime;
    }

    /** Gets the last time of the simulation.
     * 
     * @return the end time
     */
    public Time getEndTime()
    {
        return endTime;
    }

    /** Gets temperature for a given period.
     * 
     * @param t the period
     * @return the temperature for this period
     */
    public double getTemperature_fromPeriod( TimePeriod t )
    {
        return meteoData.get( t ).getAirTemperature();
    }

    /** Gets the temperature for a given step.
     * 
     * @param step the step
     * @return the temperature for this step
     */
    public double getTemperature_fromStep( int step )
    {
        TimePeriod p = null;
        int i = 0;
        for ( TimePeriod period : meteoData.keySet() )
        {
            if ( i == step )
                p = period;
            i++;
        }
        return meteoData.get( p ).getAirTemperature();
    }

    /** Gets the relative humidity for a given period.
     * 
     * @param t the period
     * @return the relative humidity for this period
     */
    public double getRh_fromPeriod( TimePeriod t )
    {
        return meteoData.get( t ).getRelativeHumidity();
    }

    /** Gets the relative humidity for a given step.
     * 
     * @param step the step
     * @return the relative humidity for this step
     */
    public double getRh_fromStep( int step )
    {
        TimePeriod p = null;
        int i = 0;
        for ( TimePeriod period : meteoData.keySet() )
        {
            if ( i == step )
                p = period;
            i++;
        }
        return meteoData.get( p ).getRelativeHumidity();
    }

    /** Gets the clearness for a given period.
     * 
     * @param t the period
     * @return the clearness for this period
     */
    public double getClearness_fromPeriod( TimePeriod t )
    {
        return meteoData.get( t ).getClearness();
    }

    /** Gets the clearness for a given step.
     * 
     * @param step the step
     * @return the clearness for this step
     */
    public double getClearness_fromStep( int step )
    {
        TimePeriod p = null;
        int i = 0;
        for ( TimePeriod period : meteoData.keySet() )
        {
            if ( i == step )
                p = period;
            i++;
        }
        return meteoData.get( p ).getClearness();
    }

    /** Gets the wind speed for a given period.
     * 
     * @param t the period
     * @return the wind speed for this period
     */
    public double getWind_fromPeriod( TimePeriod t )
    {
        return meteoData.get( t ).getWind();
    }

    /** Gets the wind speed for a given step.
     * 
     * @param step the step
     * @return the wind speed for this step
     */
    public double getWind_fromStep( int step )
    {
        TimePeriod p = null;
        int i = 0;
        for ( TimePeriod period : meteoData.keySet() )
        {
            if ( i == step )
                p = period;
            i++;
        }
        return meteoData.get( p ).getWind();
    }

    /** Gets the meteo data map.
     * 
     * @return a map of <TimePeriod, MeteoRecord>
     */
    public Map<TimePeriod, MeteoRecord> getMeteoData()
    {
        return meteoData;
    }

    /** Gets the atmospheric pressure.
     * 
     * @return the atmospheric pressure
     */
    public float getAtmPressure()
    {
        return atmPressure;
    }

    /** Gets the psychometric constant.
     * 
     * @return the psychometric constant
     */
    public float getPsychrometricConstant()
    {
        return (float)Air.psychrometricConstant( atmPressure );
    }
}
