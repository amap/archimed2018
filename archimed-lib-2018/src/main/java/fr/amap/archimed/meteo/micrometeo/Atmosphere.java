/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo.micrometeo;

/**
 * Pressure, downwelling far infrared ...
 * @author dauzat
 */
public class Atmosphere
{
    /**
     * Stefan-Boltzmann constant
     */
    public final static double SIGMA = 5.67477E-08;

    /**
     * Get the atmospheric pressure (kPa) from elevation above sea level (m).
     * @param altitude: elevation above sea level [m]
     * @return atmospheric pressure [kPa]
     */
    public static double pressureAtAltitude( double altitude )
    {
        double P = (293 - (0.0065 * altitude)) / 293;
        P = 101.3 * Math.pow( P, 5.26 );
        return P;
    }

    /**
     * Total intensity radiated over all wavelengths depending on temperature
     * <a href = https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_law>Wikipedia</a>)
     *
     * @param epsilon the emissivity of the grey body; if it is a perfect blackbody
     * epsi = 1
     *
     * @param tK temperature in Kelvin
     * @return radiated energy in W m-2
     */
    public static double radiantEmittance( double epsilon, double tK )
    {
        return SIGMA * epsilon * Math.pow( tK, 4 );
    }

    /**
     * Estimation of downwelling longwave radiation received from atmosphere under clear sky conditions
     * from Brutsaert (1975)
     * @param tAir_C air temperature at ground level [C]
     * @param eAir water vapour pressure [kPa]
     * @return longwave radiation [W m-2]
     */
    public static double downwellingRadiationClear( double tAir_C, double eAir )
    {
        double ta = tAir_C + 273.15;
        double ea = eAir * 10; // convert kPa -> mb
        double emissivity = 1.24 * Math.pow( ea / ta, 1 / 7. );
        return emissivity * SIGMA * Math.pow( ta, 4 );
    }

    /**
     * Estimation of downwelling longwave radiation received from atmosphere under any condition
     * from Crawford and Duchon (1999). Assessment in Choi et al (2008)
     * @param tAir_C air temperature within the scene [C]
     * @param eAir water vapour pressure [kPa]
     * @param cloudCover fractional cloud cover
     * @return longwave radiation in W m-2
     */
    public static double skyRadiation( double tAir_C, double eAir, double cloudCover )
    {
        double ra = downwellingRadiationClear( tAir_C, eAir );
        ra *= (1 - cloudCover);
        ra += cloudCover * SIGMA * Math.pow( tAir_C + 273.15, 4 );
        return ra;
    }

    //    public static void main( String[] args )
    //    {
    //        double tAirC = 25;
    //        double tAirK = tAirC + 273.15;
    //        double rh = 80;
    //        double eAir = Air.waterSaturationVapourPressure( tAirC ) * (rh / 100);
    //        double ra;
    //
    //        double rldc;
    //
    //        rldc = downwellingRadiationClear( tAirC, eAir );
    //        System.out.println( downwellingRadiationClear( tAirC, eAir ) );
    //
    //        //        double fcc= 0;  // fractional cloud cover= 1 - Rs/Rs0 with Rs0 the theoretical clear sky downward solar radiation
    //        for ( double fcc = 0; fcc <= 1; fcc += 0.1 )
    //            System.out.println( fcc + "\t" + skyRadiation( tAirC, eAir, fcc ) );
    //    }
}
