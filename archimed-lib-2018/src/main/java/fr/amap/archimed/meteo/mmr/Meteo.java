/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.meteo.mmr;

import org.joda.time.DateTime;

import fr.amap.archimed.application.process.lightinterception.mir.fluxes.RadiationComponents;
import fr.amap.archimed.meteo.micrometeo.Sun;
import fr.amap.archimed.model.diffusefraction.DiffuseFractionModel;
import fr.amap.archimed.time.Time;
import fr.amap.archimed.time.TimeConverter;
import fr.amap.archimed.time.TimeElement;
import fr.amap.archimed.time.TimeUnit;
import fr.amap.archimed.util.unit.Metric;
import fr.amap.archimed.util.unit.Unit;
import fr.amap.archimed.util.unit.UnitConverter;
import fr.amap.commons.animation.DoubleProperty;
import fr.amap.commons.animation.KeyValue;
import fr.amap.commons.animation.Timeline;
import javafx.animation.Interpolator;

/**
 *
 * @author Julien Heurtebize
 */
public class Meteo
{
    /**
     * Energy from the sun, per unit time, received on a unit area of surface
     * perpendicular to the direction of propagation of the radiation, at mean
     * earth-sun distance outside of the atmosphere
     */
    public final static float SOLAR_CONSTANT = 0.0820f; //MJ m^-2 min^-1 (1.366 kW m^-2)

    public final static String TEMPERATURE = "TEMPERATURE";

    public final static String CLEARNESS = "CLEARNESS";

    public final static String GLOBAL_MJ_M2 = "GLOBAL_MJ_M2";

    public final static String DIRECT_MJ_M2 = "DIRECT_MJ_M2";

    public final static String DIFFUSE_MJ_M2 = "DIFFUSE_MJ_M2";

    public enum Variable
    {
        TEMPERATURE( "TEMPERATURE" ),

        CLEARNESS( "CLEARNESS" );

        private String variable;

        private Variable( String variable )
        {
            this.variable = variable;
        }
    }

    private final Timeline timeline;

    public Meteo( DateTime startRecordTime, DateTime endRecordTime, double timestep )
    {
        timeline = new Timeline( startRecordTime, endRecordTime, timestep );
    }

    public float getDiffuse_W_M2( DateTime time )
    {
        throw new UnsupportedOperationException();
    }

    public float getDiffuseMJ( DateTime time )
    {
        float diffuseMJ;

        try
        {
            diffuseMJ = (float)(double)timeline.getValue( DIFFUSE_MJ_M2, time, Interpolator.DISCRETE );
        }
        catch( Exception ex )
        {
            diffuseMJ = Float.NaN;
        }

        return diffuseMJ;
    }

    public void setDiffuseMJ( DateTime time, float diffuseMJ ) throws Exception
    {
        checkTime( time );
        timeline.insertKeyValue( time, new KeyValue( DIFFUSE_MJ_M2, new DoubleProperty( (double)diffuseMJ ) ) );
    }

    public float getGlobalMJ( DateTime time )
    {
        float globalMJ;

        try
        {
            globalMJ = (float)(double)timeline.getValue( GLOBAL_MJ_M2, time, Interpolator.DISCRETE );
        }
        catch( Exception ex )
        {
            globalMJ = Float.NaN;
        }

        return globalMJ;
    }

    public void setGlobalMJ( DateTime time, float globalMJ ) throws Exception
    {
        checkTime( time );
        timeline.insertKeyValue( time, new KeyValue( GLOBAL_MJ_M2, new DoubleProperty( (double)globalMJ ) ) );
    }

    public float getDirectMJ( DateTime time )
    {

        float directMJ;

        try
        {
            directMJ = (float)(double)timeline.getValue( DIRECT_MJ_M2, time, Interpolator.DISCRETE );
        }
        catch( Exception ex )
        {
            directMJ = Float.NaN;
        }

        return directMJ;
    }

    public void setDirectMJ( DateTime time, float directMJ ) throws Exception
    {
        checkTime( time );
        timeline.insertKeyValue( time, new KeyValue( DIRECT_MJ_M2, new DoubleProperty( (double)directMJ ) ) );
    }

    public float getClearness( DateTime time )
    {
        float clearness;

        try
        {
            clearness = (float)(double)timeline.getValue( CLEARNESS, time, Interpolator.DISCRETE );
        }
        catch( Exception ex )
        {
            clearness = Float.NaN;
        }

        return clearness;
    }

    public void setClearness( DateTime time, float clearness ) throws Exception
    {
        checkTime( time );
        timeline.insertKeyValue( time, new KeyValue( CLEARNESS, new DoubleProperty( (double)clearness ) ) );
    }

    /**
     * Set temperature in degrees celsius (°c).
     *
     * @param time
     * @param temperature Temperature (°c)
     * @throws java.lang.Exception
     */
    public void setTemperature( DateTime time, float temperature ) throws Exception
    {
        checkTime( time );
        timeline.insertKeyValue( time, new KeyValue( TEMPERATURE, new DoubleProperty( (double)temperature ) ) );
    }

    /**
     * Get temperature in degrees celsius (°c).
     *
     * @param time
     * @return temperature (°c), NaN if no temperature can be found at the given
     * time.
     */
    public float getTemperature( DateTime time )
    {
        float temperature;

        try
        {
            temperature = (float)(double)timeline.getValue( TEMPERATURE, time, Interpolator.DISCRETE );
        }
        catch( Exception ex )
        {
            temperature = Float.NaN;
        }

        return temperature;
    }

    private void checkTime( DateTime time ) throws Exception
    {
        if ( time.compareTo( timeline.getStartTime() ) < 0 || time.compareTo( timeline.getEndTime() ) > 0 )
        {
            throw new Exception( "The given time is outside the meteo recording time range !" );
        }
    }

    /**
     * Incident solar radiation above the atmosphere on a horizontal surface.
     * John A. Duffie & William A. Beckman.
     *
     * @param latitude (radians)
     * @param doy Day of year.
     * @param time1 begin of period in decimal hour
     * @param time2 end of period in decimal hour
     * @return The extraterrestrial radiation (MJ.m^-2) on a period, where period = time2-time1
     * 0 if the sun is not set or is rise.
     */
    public static float computeExtraTerrestrialHourly( double latitude, int doy, double time1, double time2 )
    {
        double sc = Sun.getEquationOfTime( doy );

        //add seasonal correction
        time1 += sc;
        time2 += sc;

        // hour angles of the sun (in radians)
        double sTA1 = getHourAngle( time1 );
        double sTA2 = getHourAngle( time2 );

        Sun.RiseSetHours angles = fitHourAnglesToSunset( sTA1, sTA2, new Sun( latitude, doy ) );
        double solarTimeAngle = angles.set - angles.rise;

        if ( solarTimeAngle <= 0 )
            return 0;

        double declination = Sun.getDeclinationFromDoy( doy );

        double corrFactor = Sun.getCorrFactorFromDoy( doy );

        // SOLAR_CONSTANT = 0.0820f; // in MJ m^-2 min^-1 (from 1.366 kW m^-2)
        double extraTRad = ((12 * 60) / Math.PI) * SOLAR_CONSTANT * corrFactor * (Math.cos( latitude ) * Math.cos( declination ) * (Math.sin( angles.set ) - Math.sin( angles.rise )) + solarTimeAngle * (Math.sin( latitude ) * Math.sin( declination )));

        return (float)extraTRad;
    }

    /**
     * Incident solar radiation above the atmosphere on a horizontal surface.
     * John A. Duffie & William A. Beckman.
     *
     * @param latitude (radians)
     * @param doy : Day of Year
     * @return The extraterrestrial daily radiation(MJ m^-2 day ^-1)
     */
    public static float computeExtraTerrestrialDaily( double latitude, int doy )
    {
        double declination = Sun.getDeclinationFromDoy( doy );

        double corrFactor = Sun.getCorrFactorFromDoy( doy );

        double sunsetHourAngle = Sun.computeSunsetHourAngle( latitude, declination );
        double extraTRad = ((24 * 60) / Math.PI) * SOLAR_CONSTANT * corrFactor * (Math.cos( latitude ) * Math.cos( declination ) * (Math.sin( sunsetHourAngle )) + sunsetHourAngle * (Math.sin( latitude ) * Math.sin( declination )));

        return (float)extraTRad;

    }

    private static double getHourAngle( double time )
    {
        return (Math.PI / 12.0) * (time - 12);
    }

    private static Sun.RiseSetHours fitHourAnglesToSunset( double hourAngle1, double hourAngle2, Sun sun )
    {
        double sunSetHAngle = sun.getSunsetHourAngle();

        double sta1 = hourAngle1;
        double sta2 = hourAngle2;

        sta1 = Math.max( sta1, -sunSetHAngle );
        sta2 = Math.min( sta2, sunSetHAngle );

        return new Sun.RiseSetHours( sta1, sta2 );
    }

    /**
     * Get the clearness index.
     * @param latitude Latitude in radians.
     * @param doy
     * @param globalRadiation Solar insolation in MJ m^-2 per day.
     * @return 
     */
    public static float getKtDaily( double latitude, int doy, double globalRadiation )
    {
        //compute extraterrestrial radiation (MJ m^-2 day ^-1)
        float extraRad = computeExtraTerrestrialDaily( latitude, doy );

        float kt = (float)(globalRadiation / extraRad);

        return kt;
    }

    /**
     * Get the clearness index.
     * @param latitude Latitude in radians.
     * @param time Time
     * @param duration
     * @param rGh Global radiation, irradiance in W m^-2 duration.
     * @return 
     */
    public static float getKt( Unit rGh, double latitude, Time time, float duration )
    {
        //convert given irradiance in W
        double irradiance = UnitConverter.convert( rGh, Metric.NONE );

        double time1 = time.getDecimalHour() - duration * 0.5f;
        double time2 = time.getDecimalHour() + duration * 0.5f;

        //compute extraterrestrial radiation (MJ m^-2 duration ^-1)
        float extraRad = computeExtraTerrestrialHourly( latitude, time.getDoy(), time1, time2 );

        //extraterrestrial radiation in MJ m^-2 second ^-1
        extraRad = (float)TimeConverter.convertFromPerDurationToAnother( new TimeElement( extraRad, TimeUnit.HOUR ), duration, TimeUnit.SECOND );

        //extraterrestrial radiation in J m^-2 second ^-1
        extraRad = (float)UnitConverter.convert( new Unit( extraRad, Metric.MEGA ), Metric.NONE );

        if ( extraRad == 0 )
            return 0;

        float kt = (float)(irradiance / extraRad);

        return kt;
    }

    /**
     * <p>Partitioning of global into direct and diffuse components with different methods (see {@link PartitioningMethod}).
     * Can be used for periods of one hour or shorter.
     * 
     * @param model model computing diffuse fraction Kd
     * @param inRadiation partioned radiation
     * @param sunElevation in radians
     */
    public static RadiationComponents partitionGlobalHourly( DiffuseFractionModel model, RadiationComponents inRadiation, double sunElevation )
    {
        final float global = inRadiation.getGlobal();
        float diffuse = inRadiation.getDiffuse();
        float direct = inRadiation.getDirect();

        if ( global <= 0 )
            direct = diffuse = 0;
        else if ( sunElevation <= 0.0 )
        {
            diffuse = global;
            direct = 0;
        }
        else
        {
            diffuse = model.getKd() * global;
            direct = global - diffuse;
        }

        return new RadiationComponents( global, diffuse, direct );
    }
}
