package fr.amap.archimed.meteo.micrometeo;

/**
 * Sun position representation.
 * Provides sun related functionalities.
 *
 * @author J. Dauzat - May 2012, Julien Heurtebize (refactoring)
 */
public class Sun
{
    private final SunPosition position;

    private double equationOfTime;

    private double declination;

    private double corrFactor;

    private double latitude;

    private RiseSetHours riseAndSetHours;

    private double sunsetHourAngle;

    private int doy;

    private double decimalHour;

    private final static double[] DOY_ANGLES;

    private final static double[] DECLINATIONS;

    private final static double[] EQUATIONS_OF_TIME;

    private final static double[] CORR_FACTORS;

    private final static double CENTER_OF_SOLAR_DISK = Math.toRadians( -0.83 );

    static
    {
        DOY_ANGLES = new double[ 366 ];
        DECLINATIONS = new double[ 366 ];
        EQUATIONS_OF_TIME = new double[ 366 ];
        CORR_FACTORS = new double[ 366 ];

        for ( int doy = 1; doy <= 366; doy++ )
        {
            DOY_ANGLES[ doy - 1 ] = Sun.computeDoyAngle( doy );
            DECLINATIONS[ doy - 1 ] = Sun.computeDeclinationFromDoy( doy );
            EQUATIONS_OF_TIME[ doy - 1 ] = Sun.computeEquationOfTime( doy );
            CORR_FACTORS[ doy - 1 ] = Sun.computeCorrFactorFromDoy( doy, false );
        }
    }

    public static class RiseSetHours
    {
        public double rise;

        public double set;

        public RiseSetHours( double rise, double set )
        {
            this.rise = rise;
            this.set = set;
        }
    }

    public Sun( double zenith, double azimut )
    {
        position = new SunPosition();
        position.setZenith( zenith );
        position.setAzimut( azimut );
    }

    /**
     * 
     * @param latitude in radians
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     */
    public Sun( double latitude, int doy )
    {
        position = new SunPosition();
        this.latitude = latitude;
        setDoy( doy );
    }

    public final void setDoy( int doy )
    {
        this.declination = getDeclinationFromDoy( doy );
        this.equationOfTime = getEquationOfTime( doy );
        this.corrFactor = getCorrFactorFromDoy( doy );
        this.doy = doy;

        riseAndSetHours = computeSunriseAndSunsetHours( this.latitude, doy );
        sunsetHourAngle = computeSunsetHourAngle( this.latitude, declination );
    }

    /**
     * <p>Computes sun elevation and azimut</p>
     * <a href="http://www.pveducation.org/pvcdrom/properties-of-sunlight/suns-position">
     * http://www.pveducation.org/pvcdrom/properties-of-sunlight/suns-position</a>
     *
     
     * @param decimalHour in solar time
     */
    public void setPosition( double decimalHour )
    {
        /*The Hour Angle converts the local solar time (LST) into the number of degrees 
        which the sun moves across the sky. 
        By definition, the Hour Angle is 0° at solar noon. 
        Since the Earth rotates 15° per hour, each hour away from solar noon corresponds 
        to an angular motion of the sun in the sky of 15°.
        In the morning the hour angle is negative, in the afternoon the hour angle is positive.*/

        //If timeSolar doesn't have Seasonal Correction! :
        double hourAngle = Math.toRadians( (decimalHour + equationOfTime - 12) * 15 );

        double cosHourAngle = Math.cos( hourAngle );

        double cosLatitude = Math.cos( this.latitude );
        double sinLatitude = Math.sin( this.latitude );
        double cosDeclin = Math.cos( this.declination );
        double sinDeclin = Math.sin( this.declination );

        position.setElevation( Math.asin( (sinLatitude * sinDeclin) + (cosLatitude * cosDeclin * cosHourAngle) ) );
        position.setZenith( (float)((Math.PI / 2.0) - position.getElevation()) );

        double dY = -Math.sin( hourAngle );
        double dX = Math.tan( this.declination ) * cosLatitude - sinLatitude * cosHourAngle;
        double az = Math.atan2( dY, dX );

        if ( az < 0 )
            az += Math.PI * 2;

        position.setAzimut( az ); //compass convention
        this.decimalHour = decimalHour;

        //firePositionChanged();
    }

    //    private static double computeDeclinationFromDoyAngle( double doyAngle )
    //    {
    //        return ((23.45 * Math.PI) / 180.0) * Math.sin( doyAngle - 1.39 );
    //    }

    /**
     * Sun declination from Cooper (1969) as a function of day.
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return sun declination in radians.
     */
    private static double computeDeclinationFromDoy( int doy )
    {
        //double doyAngle = getDoyAngle(doy);
        //return computeDeclinationFromDoyAngle(doyAngle);
        //more accurate equation
        return Math.asin( Math.sin( Math.toRadians( -23.44 ) ) * Math.cos( (Math.toRadians( 360 ) / 365.24) * (doy + 10) + (Math.toRadians( 360 ) / Math.PI) * 0.0167 * Math.sin( (Math.toRadians( 360 ) / 365.24) * (doy - 2) ) ) );
    }

    private static double computeDoyAngle( int doy )
    {
        return 2 * Math.PI * doy / 365.0;
    }

    /**
     * Get the set hour angle.
     * @param latitude in radians
     * @param declination in radians
     * @return The angle in radians.
     */
    public static double computeSunsetHourAngle( double latitude, double declination )
    {
        //double sunsetHourAngle = -Math.tan(latitude) * Math.tan(declination);

        //more accurate equation (https://en.wikipedia.org/wiki/Sunrise_equation)
        double sunsetHourAngle = (Math.sin( CENTER_OF_SOLAR_DISK ) - Math.sin( latitude ) * Math.sin( declination )) / Math.cos( latitude ) * Math.cos( declination );

        sunsetHourAngle = Math.max( -1, sunsetHourAngle );
        sunsetHourAngle = Math.min( 1, sunsetHourAngle );
        sunsetHourAngle = Math.acos( sunsetHourAngle );

        return sunsetHourAngle;
    }

    /**
     * <p>Sun declination from Cooper (1969) as a function of day.</p>
     * If the value is lower than 1 or higher than 366 the value is respectively set to 1 and 366.
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return Sun declination angle in radians.
     */
    public static double getDeclinationFromDoy( int doy )
    {
        doy = Math.max( 1, doy );
        doy = Math.min( 366, doy );

        doy--;

        return DECLINATIONS[ doy ];
    }

    /**
     * <p>Get the day of the year angle (2pi * doy / 365).</p>
     * If the value is lower than 1 or higher than 366 the value is respectively set to 1 and 366.
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return 
     */
    public static double getDoyAngle( int doy )
    {
        doy = Math.max( 1, doy );
        doy = Math.min( 366, doy );

        doy--;

        return DOY_ANGLES[ doy ];
    }

    /**
     * Calculates the True Solar Time
     *
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return True Solar Time [decimal hour]
     */
    private static double computeEquationOfTime( int doy )
    {
        // distance
        double b = 2 * Math.PI * (doy - 81) / 365.;

        // Sc: seasonal correction for solar time [hour]
        double sc = (0.1645 * Math.sin( 2 * b )) - (0.1255 * Math.cos( b )) - (0.025 * Math.sin( b ));

        return sc;
    }

    private static double computeCorrFactorFromDoyAngle( double doyAngle, boolean fast )
    {
        if ( fast )
            return 1 + (0.033 * Math.cos( doyAngle ));

        return 1.000110 + 0.034221 * Math.cos( doyAngle ) + 0.001280 * Math.sin( doyAngle ) + 0.000719 * Math.cos( 2 * doyAngle ) + 0.000077 * Math.sin( 2 * doyAngle );
    }

    private static double computeCorrFactorFromDoy( int doy, boolean fast )
    {
        double doyAngle = Sun.getDoyAngle( doy );
        return Sun.computeCorrFactorFromDoyAngle( doyAngle, fast );
    }

    /**
     * Get the eccentricity correction factor of the earth's orbit (inverse relative distance Earth-Sun).
     * @param doy The day of year.
     * @return 
     */
    public static double getCorrFactorFromDoy( int doy )
    {
        doy = Math.max( 1, doy );
        doy = Math.min( 366, doy );

        doy--;

        return CORR_FACTORS[ doy ];
    }

    /**
     * @deprecated 
     * 
     * Calculates the True Solar Time
     *
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return True Solar Time [decimal hour]
     */
    public static float getEquationOfTimeOld( int doy )
    {
        double a1 = Math.toRadians( (1.00554 * doy) - 6.28306 );
        double a2 = Math.toRadians( (1.93946 * doy) + 23.35089 );

        double trueSolarTime = -7.67825 * Math.sin( a1 ) - 10.09176 * Math.sin( a2 );

        return (float)(trueSolarTime / 60);
    }

    /**
     * <p>Get the True Solar Time from the day of the year.</p>
     * If the value is lower than 1 or higher than 366 the value is respectively set to 1 and 366.
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return True Solar Time [decimal hour]
     */
    public static double getEquationOfTime( int doy )
    {
        doy = Math.max( 1, doy );
        doy = Math.min( 366, doy );

        doy--;

        return EQUATIONS_OF_TIME[ doy ];

        //return getSolarTimeOld(doy);
    }

    /**
     * 
     * @return True if it is the day, false if it is the night.
     */
    public boolean isTheDay()
    {
        return position.getZenith() <= Math.PI / 2.0;
    }

    public static double computeNumberOfDayLight( double latitude, double declination )
    {
        double sunsetHourAngle = computeSunsetHourAngle( latitude, declination );

        //earth rotates at 15 degrees an hour, 2/radians(15) * sunsetHourAngle
        return 7.639437268410977 * sunsetHourAngle;
    }

    /**
     * Calculates the hours of rise and set for the given day and latitude.
     *
     * @param latitude in radians
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @return Rise and set hours in solar time.
     */
    public static RiseSetHours computeSunriseAndSunsetHours( double latitude, int doy )
    {
        double declin = getDeclinationFromDoy( doy );

        // hour angle
        double ha = computeSunsetHourAngle( latitude, declin );

        // Universal time
        double ut = (ha / Math.toRadians( 15 ));
        double tst = getEquationOfTime( doy );

        double sunRise = 12 - tst - ut;
        double sunSet = 12 - tst + ut;

        return new RiseSetHours( sunRise, sunSet );
    }

    /**
     * <p>Get the True Solar Time computed from the given day of the year.</p>
     * {@link #getEquationOfTime(int)}
     * @return 
     */
    public double getEquationOfTime()
    {
        return equationOfTime;
    }

    /**
     * <p>Get sun declination computed from the given day of the year.</p>
     * {@link #getDeclinationFromDoy(int) }
     * @return 
     */
    public double getDeclination()
    {
        return declination;
    }

    public double getCorrFactor()
    {
        return corrFactor;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public RiseSetHours getRiseAndSetHours()
    {
        return riseAndSetHours;
    }

    public int getDoy()
    {
        return doy;
    }

    public double getSunsetHourAngle()
    {
        return sunsetHourAngle;
    }

    /*public static void sunPathImage(int doy, double latitude, float decimalHour1, float decimalHour2) {
    
        int nbPixels = 800;
        int border = 30;
        float center = nbPixels / 2;
    
        float radius = center - border;
    
        HemisphereImage image = new HemisphereImage.Builder()
                .nbPixels(nbPixels)
                .border(30)
                .nbParallels(10)
                .nbMeridians(36)
                .drawCardinalPoints(true)
                .build();
    
        BufferedImage bimg = image.compute();
        Graphics2D g = (Graphics2D) bimg.getGraphics();
    
        // sun position
        Sun sun = new Sun(latitude, doy);
        int sunRadius = nbPixels / 100;
    
        for (float h = decimalHour1; h < decimalHour2; h += 0.5) {
            
            sun.setPosition(h);
            
            double z = radius * sun.zenith / (Math.PI / 2);
            double x = Math.sin(sun.getAzimut()) * z;
            double y = Math.cos(sun.getAzimut()) * z;
            
            x += center;
            y += center;
    
            if (sun.isTheDay()) {
                g.setColor(new Color(255, 225, 100));
                g.fillOval((int) (x - sunRadius), (int) (nbPixels - (y - sunRadius) - 1), 2 * sunRadius, 2 * sunRadius);
    
                if (h % 1 == 0f) {
                    g.setColor(new Color(0, 0, 0));
                    g.drawString(String.valueOf((int) h) + "h", (int) (x - sunRadius), (int) (nbPixels - (y - sunRadius) - 1));
                }
            }
        }
    
        ImageView view = new ImageView(bimg);
        view.view(new JDialog());
    
    }*/

    public SunPosition getPosition()
    {
        return position;
    }

    public double getDecimalHour()
    {
        return decimalHour;
    }
}
