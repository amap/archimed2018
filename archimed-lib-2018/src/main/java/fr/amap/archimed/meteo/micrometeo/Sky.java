package fr.amap.archimed.meteo.micrometeo;

import fr.amap.archimed.application.process.lightinterception.mir.fluxes.Direction;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.RadiativeFluxes;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.Turtle;
import fr.amap.archimed.model.diffusefraction.DeJong;

/**
 * Methods for calculating the sky brightness.
 *
 * @author J. Dauzat - May 2012, J. Heurtebize (refactoring)
 */
public class Sky
{
    private final static double[] NORM_FACTORS = { 0.900819726, 0.94256221, 0.986249192, 1.031958334, 1.079767103, 1.12975041, 1.181980935, 1.236527958, 1.293453708, 1.352810764, 1.414637812, 1.478955311, 1.545765898, 1.61506015, 1.686840618, 1.761137908, 1.838005705, 1.917484415, 1.99941645, 2.081869256, 2.165381027, 2.251210433, 2.339448431, 2.430067558, 2.522966404, 2.617975865, 2.714949921, 2.813856511, 2.914740922, 3.017597829, 3.122319601, 3.228742766, 3.336752204, 3.446278234, 3.55716475, 3.669022399, 3.78131709, 3.893827485, 4.006779385, 4.120472023, 4.235073956, 4.350609413, 4.466963374, 4.583876661, 4.700928765, 4.817555386, 4.933232167, 5.04781837, 5.161527729, 5.274508583, 5.386580723, 5.497260319, 5.606093578, 5.712987125, 5.818030826, 5.921108308, 6.021761321, 6.119379553,
            6.213613987, 6.304535202, 6.392400158, 6.477443352, 6.559784561, 6.639331965, 6.715515766, 6.786803032, 6.851619293, 6.910712779, 6.965237856, 7.015541585, 7.061590293, 7.103231336, 7.140275825, 7.172520988, 7.199709328, 7.221409045, 7.237050821, 7.246487395, 7.250285146, 7.249124042, 7.243406694, 7.233320756, 7.218969113, 7.200411663, 7.177638905, 7.150650411, 7.119697941, 7.085301701, 7.047991974, 7.008178218, 6.96616831 };

    private final RadiativeFluxes radiativeFluxes;

    public Sky( Turtle turtle )
    {
        radiativeFluxes = new RadiativeFluxes( turtle );
    }

    public RadiativeFluxes getRadiativeFluxes()
    {
        return radiativeFluxes;
    }

    /**
     * Brightness of Standard OverCast sky (SOC) at a given elevation
     * (normalized over brightness at zenith) (ref. Anderson...)
     *
     * @param elevation over the horizon (radians)
     * @return brightness at elevation / brightness at zenith
     */
    public static float brightnessNormSOC( double elevation )
    {
        return (float)((1f + 2f * Math.sin( elevation )) / 3f);
    }

    /**
     * Brightness per solid angle of Standard OverCast sky (SOC) at a given
     * elevation The sum for the entire sky vault is diffuse / 2PI (ref.
     * Anderson...)
     *
     * @param diffuse diffuse radiation in W/m²
     * @param elevation over the horizon (radians)
     * @return brightness at elevation / brightness at zenith
     */
    public static float brightnessSOC( float diffuse, float elevation )
    {
        if ( elevation < 0.01 )
            return 0;

        float brightness = (float)((1f + 2f * Math.sin( elevation )) / 3f);

        // brightness *= diffuse / 2.3687458;
        // brightness *= diffuse / 2.284617;
        brightness *= diffuse / 2.366345925976101;
        return brightness;
    }

    /**
     * Brightness of Clear Sky (normalized over brightness at zenith)
     *
     * @param directionInSky
     * @param sun
     * @return brightness at sky position / brightness at zenith
     */
    public static float brightnessNormClear( Direction directionInSky, Direction sun )
    {
        double cosSunZen = Math.cos( sun.getZenith() );
        double sinElevation = Math.sin( directionInSky.getElevation() );

        float angle = sun.getVector().angle( directionInSky.getVector() );
        double cosAngle = Math.cos( angle );

        /*double gamma = cosZenith * cosSunZen;
        gamma += Math.sin(directionInSky.getZenith()) * Math.sin(sun.getZenith()) * Math.cos(directionInSky.getAzimut() -  (sun.getAzimut()));*/

        double brightness = 0.91 + 10 * Math.exp( -3.0 * angle );
        brightness += 0.45 * (cosAngle * cosAngle);
        brightness *= 1.0 - Math.exp( -0.32 / sinElevation );
        brightness /= (0.91 + (10 * Math.exp( -3.0 * sun.getZenith() )) + (0.45 * cosSunZen * cosSunZen));
        brightness /= 0.27385; // 1 - exp (-0.32/sin (pi/2))

        return (float)brightness;
    }

    /**
     * @deprecated 
     * Brightness of Clear Sky (normalized over brightness at zenith)
     *
     * @param directionInSky
     * @param sun
     * @return brightness at sky position / brightness at zenith
     */
    public static float brightnessNormClearOld( Direction directionInSky, Sun sun )
    {
        double cosSunZen = Math.cos( sun.getPosition().getZenith() );
        double cosZenith = Math.cos( directionInSky.getZenith() );

        double gamma = cosZenith * cosSunZen;
        gamma += Math.sin( directionInSky.getZenith() ) * Math.sin( sun.getPosition().getZenith() ) * Math.cos( directionInSky.getAzimut() - (sun.getPosition().getAzimut()) );

        double brightness = 0.91 + 10 * Math.exp( -3.0 * Math.acos( gamma ) );
        brightness += 0.45 * (gamma * gamma);
        brightness *= 1.0 - Math.exp( -0.32 / cosZenith );
        brightness /= (0.91 + (10 * Math.exp( -3.0 * sun.getPosition().getZenith() )) + (0.45 * cosSunZen * cosSunZen));
        brightness /= 0.27385; // 1 - exp (-0.32/sin (pi/2))

        return (float)brightness;
    }

    /**
     * Brightness of Clear Sky (normalized over brightness at zenith)
     *
     * @param diffuse
     * @param directionInSky
     * @param sun
     * @return brightness at sky position / brightness at zenith
     */
    public static float brightnessClear( float diffuse, Direction directionInSky, Sun sun )
    {
        if ( diffuse == 0 )
            return 0;

        float brightness = brightnessNormClear( directionInSky, new Direction( sun.getPosition().getDirection() ) );

        int zenDeg = (int)(Math.toDegrees( sun.getPosition().getElevation() ) + 0.1);
        if ( zenDeg > 90 )
            return 0;

        if ( zenDeg < 0 )
            return 0;

        brightness *= diffuse / NORM_FACTORS[ zenDeg ];

        return brightness;
    }

    /**
     * Interpolation between brightnessNormClear and brightnessNormSOC according
     * to clearness index
     *
     * @param diffuse radiation in W/m²
     * @param global radiation in W/m²
     * @param directionInSky
     * @param sun
     * @return sky brightness in the direction (zenith, azimut)
     */
    public static float brightnessNorm( float diffuse, float global, Direction directionInSky, Direction sun )
    {
        float coeffSOC = getSOCInDiffuseHourly( diffuse, global, directionInSky.getElevation() );
        float coeffClear = 1f - coeffSOC;
        float brightness = coeffSOC * brightnessNormSOC( directionInSky.getElevation() );
        brightness += (coeffClear * brightnessNormClear( directionInSky, sun ));
        brightness *= diffuse;
        return brightness;
    }

    /**
     * Brightness per solid angle (interpolated between brightnessNormClear and
     * brightnessNormSOC according to clearness index)
     *
     * @param diffuse radiation in W/m²
     * @param global radiation in W/m²
     * @param directionInSky
     * @param sun
     * @return sky brightness in the direction (zenith, azim)
     */
    public static float brightness( float diffuse, float global, Direction directionInSky, Sun sun )
    {
        if ( directionInSky.getZenith() > Math.PI * 0.499 )
            return 0;

        float elevation = (float)((Math.PI / 2f) - directionInSky.getZenith());

        float coeffSOC = getSOCInDiffuseHourly( diffuse, global, elevation );

        float coeffClear = 1f - coeffSOC;

        float brightness = 0;
        brightness += brightnessSOC( coeffSOC * diffuse, elevation );
        brightness += brightnessClear( coeffClear * diffuse, directionInSky, sun );

        return brightness;
    }

    /**
     * Assess proportion of Standard Overcast Sky (SOC) in diffuse radiation by
     * comparing (diffuse/global) to diffuseGlobalHourlyClear (the ratio is
     * between R, for clear sky, and 1, for overcast sky
     *
     * @param diffuse
     * @param global
     * @param sunElevation (radians)
     * @return ratio (diffuse SOC : diffuse total)
     */
    public static float getSOCInDiffuseHourly( float diffuse, float global, double sunElevation )
    {
        if ( sunElevation <= 0 )
            return 0.5f;

        float R = DeJong.getDiffuseInGlobalHourlyClear( sunElevation );
        return getSOCFraction( diffuse, global, R );
    }

    /**
     * Assess proportion of Standard Overcast Sky (SOC) in diffuse radiation by
     * comparing (diffuse/global) to diffuseGlobalDailyClear ((diffuse/global)
     * is 0.23 for clear sky and 1 for overcast sky
     *
     * @param diffuse
     * @param global
     * @return ratio (diffuse SOC : diffuse total)
     */
    public static float getSOCInDiffuseDaily( float diffuse, float global )
    {
        return getSOCFraction( diffuse, global, 0.23f );
    }

    private static float getSOCFraction( float diffuse, float global, float diffuseForClearSky )
    {
        float fractionSoc = (diffuse / global - diffuseForClearSky) / (1 - diffuseForClearSky);
        fractionSoc = Math.max( fractionSoc, 0 );

        return fractionSoc;
    }

}
