/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.time;

/**
 * Defines a Time period.
 * @author Florian Hilaire
 */
public class TimePeriod {
    
    private Time start;
    private Time end;
    
    /**
     * Constructor.
     * @param start period's start
     * @param end period's end
     */
    public TimePeriod(Time start, Time end) {
        this.start = start;
        this.end = end;
    }

    /**
     * Gets the start of the period.
     * @return Time start
     */
    public Time getStart() {
        return start;
    }

    /**
     * Gets the end of the period.
     * @return Time end
     */
    public Time getEnd() {
        return end;
    }
    
    /**
     * Returns the duration of the time period.
     * @return the duration in decimal hour
     */
    public double getDuration() {
        return (double)TimeUtil.timeDifference(start, end)/60;
    }
}
