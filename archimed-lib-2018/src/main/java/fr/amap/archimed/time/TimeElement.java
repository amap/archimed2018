/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.time;

/**
 * Time element.
 * @author Julien Heurtebize
 */
public class TimeElement
{
    public double value;

    public TimeUnit unit;

    /**
     * Constructor.
     * @param value value
     * @param unit unit
     */
    public TimeElement( double value, TimeUnit unit )
    {
        this.value = value;
        this.unit = unit;
    }
}
