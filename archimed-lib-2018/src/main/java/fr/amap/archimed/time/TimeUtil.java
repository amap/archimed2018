package fr.amap.archimed.time;

import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.github.bfsmith.geotimezone.TimeZoneLookup;
import com.github.bfsmith.geotimezone.TimeZoneResult;

import fr.amap.archimed.meteo.micrometeo.Sun;

/**
 * Utility class to make convertions or operations on the Time object.
 * @author J. Heurtebize
 */
public class TimeUtil
{
    /**
     * Convert time from UTC (Universal Time Coordinated) to TST (True Solar Time).
     * @param utc
     * @param longitude
     * @return 
     */
    public static Time convertUTCIntoTST( DateTime utc, double longitude )
    {
        //equation of time
        double eot = Sun.getEquationOfTime( utc.getDayOfYear() );

        //time correction factor
        double tc = ((4.0 * Math.toDegrees( longitude )) + eot);

        //local solar time
        double solarTime = getDecimalHour( utc ) + (tc / 60.0);

        return new Time( utc.getYear(), utc.getDayOfYear(), (float)solarTime );
    }

    /**
     * 
     * @param time
     * @param longitude in radians from -pi to pi (-180°W to 180°E) with 0 as the Greenwich meridian.
     * @return 
     */
    public static Time convertTSTIntoUTC( Time time, double longitude )
    {
        //equation of time
        double eot = Sun.getEquationOfTime( time.getDoy() );

        //time correction factor
        double tc = ((4.0 * Math.toDegrees( longitude )) + eot);

        double solarTime = time.getDecimalHour() - tc / 60.0;
        return new Time( time.getYear(), time.getDoy(), solarTime );
    }

    /**
     * Convert time from Legal Time (local time with daylight saving) to TST (True Solar Time).
     * @param legalTime
     * @param longitude in radians from -pi to pi (-180°W to 180°E) with 0 as the Greenwich meridian.
     * @return 
     */
    public static Time convertLegalTimeIntoTST( DateTime legalTime, double longitude )
    {
        DateTime utcTime = convertLegalTimeIntoUTC( legalTime );

        Time solar = convertUTCIntoTST( utcTime, longitude );

        return solar;
    }

    public static DateTime convertTSTIntoLegalTime( Time time, double longitude, DateTimeZone zone )
    {
        Time utcTime = convertTSTIntoUTC( time, longitude );

        //convert time to DateTime
        DateTime utcDateTime = TimeUtil.createDateTimeFromTime( utcTime, DateTimeZone.UTC );

        DateTime localDateTime = utcDateTime.toDateTime( zone );

        return localDateTime;
    }

    /*public static Time convertTSTintoMST(Time tst) {
        
        //equation of time
        double ee = Sun.getEquationOfTime(tst.getDoy());
        
        float mstTime = (float) (tst.getDecimalHour() - ee);
        return new Time(tst.getYear(), tst.getDoy(), mstTime);
    }*/

    /*public static Time convertMSTintoTST(Time mst) {
        
        //equation of time
        double ee = Sun.getEquationOfTime(mst.getDoy());
        
        float tst = (float) (mst.getDecimalHour() + ee);
        return new Time(mst.getYear(), mst.getDoy(), tst);
    }*/

    /**
     * Convert time from MST (Mean Solar Time) to LT (Local time).
     * @param mst
     * @param longitude
     * @return 
     */
    /*public static Time convertMSTintoLT(Time mst, double longitude) {
        float legalTime = (float) (mst.getDecimalHour() - 4.0 / 60.0 * Math.toDegrees(longitude));
        return new Time(mst.getYear(), mst.getDoy(), legalTime);
    }*/

    /*public static Time convertLTintoUTC(Time legalTime, int timeZone) {
        float utc = (float) (legalTime.getDecimalHour() + timeZone);
        return new Time(legalTime.getYear(), legalTime.getDoy(), utc);
    }*/

    /*public static Time convertMSTIntoUTC(Time mst, double longitude){
        
        Time tstTime = convertMSTintoTST(mst);
        Time utcTime = convertTSTIntoUTC(tstTime, longitude);
        
        return utcTime;
    }*/

    /**
     * Get the decimal hour from hour and minutes.
     * @param hour The hour
     * @param minutes The minutes
     * @return The decimal hour
     */
    public static double getDecimalHour( int hour, int minutes )
    {
        return hour + (minutes / 60.0);
    }

    /**
     * Get the decimal hour from hour, minutes and seconds.
     * @param hour The hour
     * @param minutes The minutes
     * @param seconds The seconds
     * @return The decimal hour.
     */
    public static double getDecimalHour( int hour, int minutes, int seconds )
    {
        return hour + (minutes / 60.0) + (seconds / 3600.0);
    }

    public static Time getHoursMinSecFromMillis( long millis )
    {

        int nbSeconds = (int)(millis / 1000.0);
        int nbMinutes = (int)(nbSeconds / 60.0);
        nbSeconds -= nbMinutes * 60;

        int nbHours = (int)(nbMinutes / 60.0);
        nbMinutes -= nbHours * 60;

        return new Time( 1999, 1, 1, nbHours, nbMinutes, nbSeconds );
    }

    public static double getDecimalHour( DateTime time )
    {
        return getDecimalHour( time.getHourOfDay(), time.getMinuteOfHour(), time.getSecondOfMinute() );
    }

    public static DateTime createDateTimeFromDoy( int year, int dayOfYear, int hourOfDay, int minuteOfHour, int secondOfMinute, DateTimeZone zone )
    {
        DateTime datetime = new DateTime( year, 1, 1, hourOfDay, minuteOfHour, secondOfMinute, zone );
        datetime = datetime.plusDays( dayOfYear - 1 );

        return datetime;
    }

    public static DateTime createDateTimeFromTime( Time time, DateTimeZone zone )
    {
        return createDateTimeFromDoy( time.getYear(), time.getDoy(), time.getHours(), time.getMinutes(), time.getSeconds(), zone );
    }

    public static DateTime convertLegalTimeIntoUTC( DateTime localTime )
    {
        DateTime utcTime = localTime.toDateTime( DateTimeZone.UTC );

        return utcTime;
    }

    /**
     * <p>Get the ID for the time zone computed from a longitude and a latitude.</p>
     * The ID is either an abbreviation such as "PST", a full name such as "America/Los_Angeles",
     * or a custom ID such as "GMT-8:00".
     * @param latitude in radians from -pi/2 to pi/2 (-90° to 90°)
     * @param longitude in radians from -pi to pi (-180°W to 180°E) with 0 as the Greenwich meridian.
     * @return 
     */
    public static String getDateTimeZoneID( double latitude, double longitude )
    {
        TimeZoneLookup tzl = new TimeZoneLookup();
        TimeZoneResult result = tzl.getTimeZone( Math.toDegrees( latitude ), Math.toDegrees( longitude ) );

        return result.getResult();
    }

    /**
     * <p>Get the time zone computed from a longitude and a latitude.</p>
     * @param latitude in radians from -pi/2 to pi/2 (-90° to 90°)
     * @param longitude in radians from -pi to pi (-180°W to 180°E) with 0 as the Greenwich meridian.
     * @return 
     */
    public static DateTimeZone getDateTimeZone( double latitude, double longitude )
    {
        String timeZoneId = getDateTimeZoneID( latitude, longitude );

        return DateTimeZone.forTimeZone( TimeZone.getTimeZone( timeZoneId ) );
    }

    /**
     * Return the difference between two Time in minute
     * @param t1
     * @param t2
     * @return
     */
    public static int timeDifference( Time t1, Time t2 )
    {
        int diff = 0;
        Time td = t1;
        while ( !TimeUtil.isTimeEqualOrSuperior( td, t2 ) )
        {
            td = TimeUtil.incrementedTime( td, 0.0167f );
            diff++;
        }
        return diff;
    }

    /**
     * Time incremented of a timeStep (0.2f = 12 min, 0.5f = 30 min, 1 = 1h, 1.5f = 1h30 etc...).
     * @param t
     * @param timeStep
     * @return
     */
    public static Time incrementedTime( Time t, float timeStep )
    {
        int year, doy, hour, minute;

        year = t.getYear();
        doy = t.getDoy();
        hour = t.getHours() + (int)timeStep;
        minute = (int)(t.getMinutes() + (timeStep - (int)timeStep) * 60);

        int i = 0;
        while ( minute > 60 )
        {
            minute -= 60;
            i++;
        }
        hour += i;
        i = 0;
        while ( hour > 24 )
        {
            hour -= 24;
            i++;
        }
        doy += i;
        i = 0;
        while ( doy > 365 )
        {
            doy -= 365;
            i++;
        }
        year += i;

        if ( minute == 60 )
        {
            minute = 0;
            hour++;
        }
        if ( hour == 24 )
        {
            hour = 0;
            doy++;
        }
        if ( doy == 365 )
        {
            doy = 0;
            year++;
        }

        return new Time( year, doy, hour, minute );
    }

    /**
     * Indicates if two Time are the same date or if t1 is superior to t2.
     * @param t1
     * @param t2
     * @return
     */
    public static boolean isTimeEqualOrSuperior( Time t1, Time t2 )
    {
        return t1.getYear() >= t2.getYear() && t1.getDoy() >= t2.getDoy() && t1.getHours() >= t2.getHours() && t1.getMinutes() >= t2.getMinutes() && t1.getSeconds() >= t2.getSeconds();
    }

    /**
     * Indicates if t1 is superior to t2.
     * @param t1
     * @param t2
     * @return
     */
    public static boolean isTimeSuperior( Time t1, Time t2 )
    {
        return t1.getYear() > t2.getYear() || (t1.getYear() > t2.getYear() && t1.getDoy() > t2.getDoy()) || ((t1.getYear() == t2.getYear() && t1.getDoy() == t2.getDoy()) && t1.getHours() > t2.getHours()) || ((t1.getYear() == t2.getYear() && t1.getDoy() == t2.getDoy() && t1.getHours() == t2.getHours()) && t1.getMinutes() > t2.getMinutes()) || ((t1.getYear() == t2.getYear() && t1.getDoy() == t2.getDoy() && t1.getHours() == t2.getHours() && t1.getMinutes() == t2.getMinutes()) && t1.getSeconds() > t2.getSeconds());
    }

    /**
     * main test.
     * @param args arguments
     * @throws IOException exc
     * @throws Exception exc
     */
    //    public static void main(String[] args) throws IOException, Exception {
    //        String[] availableIDs = TimeZone.getAvailableIDs();
    //        
    //        double longitude = Math.toRadians(2.33);
    //        double latitude = Math.toRadians(48.87);
    //        
    //        final DateTimeZone parisTimeZone = TimeUtil.getDateTimeZone(latitude, longitude);
    //        final DateTimeZone costaRicaTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone("America/Costa_Rica"));
    //        
    //        DateTime dateTime = new DateTime(2016, 8, 23, 12, 0, parisTimeZone);
    //        int dayOfYear = dateTime.getDayOfYear();
    //        Time solarTime = convertLegalTimeIntoTST(dateTime, longitude);
    //        Sun sun = new Sun(latitude, dayOfYear);
    //        Sun.RiseSetHours riseAndSetHours = sun.getRiseAndSetHours();
    //        
    //        
    //        
    //        DateTime rise = convertTSTIntoLegalTime(new Time(2016, dayOfYear, riseAndSetHours.rise), longitude, parisTimeZone);
    //        DateTime set = convertTSTIntoLegalTime(new Time(2016, dayOfYear, riseAndSetHours.set), longitude, parisTimeZone);
    //        
    //        DateTime parisDateTime = convertTSTIntoLegalTime(solarTime, longitude, parisTimeZone);
    //            
    //        ScheduledExecutorService service = Executors.newScheduledThreadPool(1);
    //        service.scheduleAtFixedRate(new Runnable() {
    //            @Override
    //            public void run(){
    //                
    //                DateTime parisDateTime = DateTime.now(parisTimeZone);
    //                DateTime costaRicaDateTime = parisDateTime.toDateTime(costaRicaTimeZone);
    //                
    //                Time solarTimeToParis = TimeUtil.convertLegalTimeIntoTST(parisDateTime, Math.toRadians(2.67));
    //
    //                System.out.println("current hour of day in paris/costa-rica/solar time in paris: "+parisDateTime.toString("HH:mm:ss")
    //                +" / "+costaRicaDateTime.toString("HH:mm:ss")+" / "+solarTimeToParis.getHours()+":"+solarTimeToParis.getMinutes());
    //
    //
    //                
    //            }
    //        }, 0, 1, TimeUnit.SECONDS);
    //    }
}
