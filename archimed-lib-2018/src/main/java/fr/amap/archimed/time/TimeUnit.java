/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.time;

/** Describing the number of seconds in differents time units.
 *
 * @author J. Heurtebize
 */
public enum TimeUnit
{
    DAY( 86400 ),

    HOUR( 3600 ),

    MINUTE( 60 ),

    SECOND( 1 );

    private final long value;

    /**
     * Constructor.
     * @param value value
     */
    private TimeUnit( long value )
    {
        this.value = value;
    }

    /**
     * Gets the value.
     * @return the value
     */
    public long getValue()
    {
        return value;
    }
}
