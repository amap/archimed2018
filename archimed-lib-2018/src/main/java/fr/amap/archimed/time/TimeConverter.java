/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.time;

/**
 *
 * @author J. Heurtebize
 */
public class TimeConverter
{
    public static double convert( TimeElement timeElement, TimeUnit dest )
    {
        double factor = timeElement.unit.getValue() / (double)dest.getValue();
        return timeElement.value * factor;
    }

    /**
     * Convert a value expressed for a duration to another time unit.
     * Example : 1MJ/m²/hour will result to  0.0277 MJ/m²/second
     * @param timeElement
     * @param duration
     * @param dest
     * @return 
     */
    public static double convertFromPerDurationToAnother( TimeElement timeElement, double duration, TimeUnit dest )
    {
        double factor = TimeConverter.convert( new TimeElement( 1, timeElement.unit ), dest );
        double result = timeElement.value / factor;

        return result;
    }

    //    public static void main( String[] args )
    //    {
    //        float extraRad = 50;
    //
    //        extraRad = (float)TimeConverter.convertFromPerDurationToAnother( new TimeElement( extraRad, TimeUnit.DAY ), 1, TimeUnit.SECOND );
    //
    //        System.out.println( extraRad );
    //    }
}
