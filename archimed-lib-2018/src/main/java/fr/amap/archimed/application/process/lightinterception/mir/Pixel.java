/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir;

/**
 * element of {@link MeshProjection}
 *
 * @author Jean Dauzat
 */
public class Pixel
{
    // x index in pixel table
    public int i;

    // y index in pixel table
    public int j;

    // height of projected triangle
    public float val;

    public Pixel( int i, int j, float val )
    {
        this.i = i;
        this.j = j;
        this.val = val;
    }
}
