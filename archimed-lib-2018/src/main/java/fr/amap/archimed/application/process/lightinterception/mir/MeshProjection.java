package fr.amap.archimed.application.process.lightinterception.mir;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.element.shape.Triangle;
import fr.amap.archimed.math.geometry.element.shape.mesh.TriangulatedMesh;
import jeeb.lib.structure.geometry.mesh.Polygon3D;

/**
 * Projections on the pixel tables.
 *
 * @author J. Dauzat - April 2012, J. Heurtebize (refactoring)
 */
public class MeshProjection
{
    private final List<Pixel> projection;

    /**
     * surface projeté du mesh (vrai surface) en m2.
     */
    private float projectedMeshArea;

    /**
     * surface projeté en pixel.
     */
    private float projectedPixelsArea;

    /**
     * Project a mesh 
     * @param mesh
     * @param direction
     * @param sceneOrigin
     * @param xPixelSize
     * @param yPixelSize 
     * @return  
     */
    public static MeshProjection projectMeshOnGround( TriangulatedMesh mesh, Vector3f direction, Point2f sceneOrigin, float xPixelSize, float yPixelSize )
    {
        MeshProjection meshProjection = new MeshProjection( new ArrayList<>(), 0, 0 );

        for ( Triangle triangle : mesh.getTriangles() )
            projectTriangle( triangle, direction, sceneOrigin, xPixelSize, yPixelSize, meshProjection );

        return meshProjection;
    }

    /**
     * Modified 16/05/2012 (previous method: projectPolygonOld)
     *
     * @param triangle
     * @param direction
     * @param sceneOrigin
     * @param xPixelSize pixel size on x axis
     * @param yPixelSize pixel size on y axis
     * @param meshProjection
     */
    public static void projectTriangle( Triangle triangle, Vector3f direction, Point2f sceneOrigin, float xPixelSize, float yPixelSize, MeshProjection meshProjection )
    {
        //used to store the projected area
        Point3f[] projectedPoints = new Point3f[ triangle.getVertices().length ];

        // transform coordinates in pixel
        Point3f[] points = new Point3f[ triangle.getVertices().length ];

        /*projection of all points of the polygon*/

        Point3f[] vertices = triangle.getVertices();

        for ( int n = 0; n < vertices.length; n++ )
        {
            Point3f point = vertices[ n ];

            //project point
            Point3f pt = projectPoint( point, direction );

            //used to store the projected area
            projectedPoints[ n ] = new Point3f( pt.x, pt.y, 0 );

            //transform point to pixel coordinates
            pt.x = (pt.x - sceneOrigin.x) / xPixelSize;
            pt.y = (pt.y - sceneOrigin.y) / yPixelSize;

            points[ n ] = pt;
        }

        /* find min and max (x,y) indices of polygon projection */
        Point3f pt = points[ 0 ];

        int iMin = (int)Math.floor( pt.x );
        int iMax = (int)Math.ceil( pt.x );

        int jMin = (int)Math.floor( pt.y );
        int jMax = (int)Math.ceil( pt.y );

        float kMin = (int)Math.floor( pt.z );
        float kMax = (int)Math.ceil( pt.z );

        for ( int p = 1; p < points.length; p++ )
        {
            iMin = Math.min( iMin, (int)Math.floor( points[ p ].x ) );
            iMax = Math.max( iMax, (int)Math.ceil( points[ p ].x ) );

            jMin = Math.min( jMin, (int)Math.floor( points[ p ].y ) );
            jMax = Math.max( jMax, (int)Math.ceil( points[ p ].y ) );

            kMin = Math.min( kMin, (int)Math.floor( points[ p ].z ) );
            kMax = Math.max( kMax, (int)Math.ceil( points[ p ].z ) );
        }

        /* initialize min and max Y indices from iMin to iMax*/
        int iLength = (iMax - iMin) + 1;

        int[] minY = new int[ iLength ];
        int[] maxY = new int[ iLength ];

        Arrays.fill( minY, jMax );
        Arrays.fill( maxY, jMin );

        /* find min and max Y indices of polygon edges (inner pixels)*/

        for ( int p = 0; p < points.length; p++ )
        {
            //edge = point1 -> point2
            Point3f point1 = points[ p ];

            //if last point, link to the first point
            Point3f point2 = points[ (p + 1) % points.length ];

            getBorderPixels( point1, point2, iMin, minY, maxY );
        }

        /* fill pixels within borders and calculate their height */

        Vector3f normal = computeNormal( points ); //normal of the polygon

        //slope of normale
        float slopeX, slopeY;
        if ( Math.abs( normal.z ) > 0.00001f )
        {
            slopeX = normal.x / normal.z;
            slopeY = normal.y / normal.z;
        }
        else
        {
            slopeX = direction.z * normal.x;
            slopeY = direction.z * normal.y;
        }

        // calculate pixel height
        float zPoint0 = points[ 0 ].getZ();
        zPoint0 += slopeX * (points[ 0 ].getX() - iMin);
        zPoint0 += slopeY * (points[ 0 ].getY() - jMin);

        int nbHits = 0;

        for ( int i = iMin; i < iMax; i++ )
        {

            int ni = i - iMin; //from 0 to iMax-iMin

            float zi_pix = zPoint0 - (slopeX * ni);

            for ( int j = minY[ ni ]; j < maxY[ ni ]; j++ )
            {

                int nj = j - jMin;

                float zpix = zi_pix - (slopeY * nj); //interpolate (y = slope.(x-x1) + y1)

                zpix = Float.max( kMin, zpix );
                zpix = Float.min( kMax, zpix );

                meshProjection.projection.add( new Pixel( i, j, zpix ) );

                nbHits++;
            }
        }

        //used to store the projected area
        float pixelArea = xPixelSize * yPixelSize;
        Polygon3D polProjected = new Polygon3D( projectedPoints );

        meshProjection.projectedMeshArea += polProjected.getArea();
        meshProjection.projectedPixelsArea += nbHits * pixelArea;
    }

    private static Point3f projectPoint( Point3f pt, Vector3f direction )
    {
        if ( direction.getZ() == 0 )
            return null;

        float dz = -pt.z / direction.getZ();

        float x = pt.x + (direction.getX() * dz);
        float y = pt.y + (direction.getY() * dz);
        float z = pt.z;

        return new Point3f( x, y, z );
    }

    /**
     * 
     * @param point1 First point of the edge.
     * @param point2 Second point of the edge.
     * @param iOrigin
     * @param minY
     * @param maxY 
     */
    private static void getBorderPixels( Point3f point1, Point3f point2, int iOrigin, int[] minY, int[] maxY )
    {
        Point3f pMin, pMax;
        if ( point1.x < point2.x )
        {
            pMin = new Point3f( point1 );
            pMax = new Point3f( point2 );
        }
        else
        {
            pMin = new Point3f( point2 );
            pMax = new Point3f( point1 );
        }

        if ( (pMax.x - pMin.x) < 0.000001 )
            return;

        // slope of polygon edge equation
        float slope = (pMax.y - pMin.y) / (pMax.x - pMin.x);

        int i_min = (int)Math.ceil( pMin.x );
        int i_max = (int)Math.floor( pMax.x );

        for ( int i = i_min; i <= i_max; i++ )
        {

            int i0 = i - iOrigin;
            float yi = (slope * (i - pMin.x)) + pMin.y; // y = ax+b 
            int j = (int)(Math.floor( yi ) + 0.5);

            if ( (i0 >= 0) && (i0 < minY.length) )
            {
                minY[ i0 ] = Math.min( minY[ i0 ], j );
                maxY[ i0 ] = Math.max( maxY[ i0 ], j );
            }
        }
    }

    /**
     * <p>Method to get the polygon's normal</p>
     * Cross product of polygon edges (not normalized) / 
     * Note: - polygon must be plane (flat) / - edges must not be colinear
     *
     * @param pointList
     * @return The polygon's normal as a 3d vector, null if the normale can't be computed.
     */
    public static Vector3f computeNormal( Point3f[] pointList )
    {
        if ( pointList.length < 2 )
            return null;

        Vector3f normale = new Vector3f();

        Vector3f vect1 = new Vector3f();
        Vector3f vect2 = new Vector3f();

        for ( int n = 0; n + 2 < pointList.length; n++ )
        {
            // first edge:
            vect1.set( pointList[ n + 1 ] );
            vect1.sub( pointList[ n ] );

            if ( vect1.length() > 0 )
            {
                vect1.normalize();
                // next edge: vec2
                vect2.set( pointList[ n + 2 ] );
                vect2.sub( pointList[ n + 1 ] );

                if ( vect2.length() > 0 )
                {
                    vect2.normalize();
                    normale = new Vector3f();

                    normale.cross( vect1, vect2 );

                    float norm = normale.length();
                    if ( norm > 0 )
                    {
                        normale.normalize();
                        return normale;
                    }
                }
            }

        }

        return normale;
    }

    public MeshProjection( List<Pixel> projection, float meshArea, float pixelArea )
    {
        this.projection = projection;
        this.projectedMeshArea = meshArea;
        this.projectedPixelsArea = pixelArea;
    }

    public List<Pixel> getProjection()
    {
        return projection;
    }

    public float getProjectedMeshArea()
    {
        return projectedMeshArea;
    }

    public float getProjectedPixelsArea()
    {
        return projectedPixelsArea;
    }
}
