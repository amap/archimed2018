package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

import java.io.IOException;
import java.util.Arrays;

import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ArchimedScene;

/**
 * MMR pixel table. To each pixel is associated information about node(s) hit by a ray.
 * This implementation uses 3 dimension arrays of nodes/heights.
 * This saves memory space compared to use of lists of {@link PixelInfo}
 */
public class PixelTableInt implements IPixelTable
{
    /**
     * allocation increment
     */
    private static final int CHUNK_SIZE = 10;

    /**
     * tableau 2D de tableau de nodeID.
     * 
     * la 3ème dimension est réallouée de CHUNK_SIZE au besoin
     */

    /**
     * node array
     * dimension 1 : x coordinate
     * dimension 2 : y coordinate
     * dimension 3 : node references
     */
    private ASNode[][][] nodes;

    /**
     * height array
     */
    private float[][][] heights;

    public PixelTableInt( int xSize, int ySize )
    {
        nodes = new ASNode[ xSize ][ ySize ][];
        heights = new float[ xSize ][ ySize ][];
    }

    @Override
    public void addPixelInfo( int i, int j, ASNode node, float height )
    {
        ASNode[] nds = nodes[ i ][ j ];
        float[] hts = heights[ i ][ j ];

        if ( nds == null )
        {
            nds = nodes[ i ][ j ] = new ASNode[ CHUNK_SIZE ];
            hts = heights[ i ][ j ] = new float[ CHUNK_SIZE ];
            nds[ 0 ] = node;
            hts[ 0 ] = height;
        }
        else
        {
            final int n = nds.length;

            // index of first free array entry on 3rd dimension 
            int ffn = n - 1;

            if ( nds[ ffn ] != null )
            {
                nds = nodes[ i ][ j ] = Arrays.copyOf( nds, n + CHUNK_SIZE );
                hts = heights[ i ][ j ] = Arrays.copyOf( hts, n + CHUNK_SIZE );
                ffn++;
            }
            else
                for ( ; ffn >= 0; ffn-- )
                    if ( nds[ ffn ] != null )
                        break;

            nds[ ffn ] = node;
        }
    }

    @Override
    public int getXSize()
    {
        // heights[0][0][0]; // float
        //		int b = heights[0][0].length; // len float[] 3° dim
        //		int a = heights[0].length; // len float[][] 2° dim Y
        //		int c = nodeIds.length; // len float[][][] 1ere dim X

        return nodes.length;
    }

    @Override
    public int getYSize()
    {
        return heights[ 0 ].length;
    }

    @Override
    public int getHitCount( int x, int y )
    {
        return nodes[ x ][ y ].length;
    }

    @Override
    public ASNode getNode( int x, int y, int n )
    {
        return nodes[ x ][ y ][ n ];
    }

    @Override
    public float getHeight( int x, int y, int n )
    {
        return heights[ x ][ y ][ n ];
    }

    private void sortPixel( int x, int y )
    {
        // cf. https://stackoverflow.com/questions/112234/sorting-matched-arrays-in-java
        throw new Error( "PixelTableInt.sortPixel() not implemented" );
    }

    @Override
    public void sortPixels()
    {
        for ( int y = getYSize() - 1; y >= 0; y-- )
            for ( int x = getXSize() - 1; x >= 0; x-- )
                sortPixel( x, y );
    }

    public static PixelTable readProjectionFile( String filePath, ArchimedScene scene ) throws IOException
    {
        throw new Error( "PixelTableInt.readProjectionFile() not implemented" );
    }

    @Override
    public void writeProjectionFile( String filePath ) throws IOException
    {
        throw new Error( "PixelTableInt.writeProjectionFile() not implemented" );
    }
}
