package fr.amap.archimed.application.process.lightinterception.mir;

import java.io.IOException;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.Application;
import fr.amap.archimed.application.ModelManager;
import fr.amap.archimed.application.process.lightinterception.LightInterceptionProcess;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.DirectionCount;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.RadiativeFlux;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.RadiativeFluxes;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.Turtle;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.TurtleDirection;
import fr.amap.archimed.application.process.lightinterception.mir.fluxes.TurtleFluxes;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.IPixelTable;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.PixelTableCache;
import fr.amap.archimed.meteo.MeteoData;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.lightinterception.InterceptionModel;
import fr.amap.archimed.parameters.ModelKey;
import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.scene.ASItem;
import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ASTreeIterator;
import fr.amap.archimed.scene.ArchimedScene;
import fr.amap.archimed.scene.PlotBox;
import fr.amap.archimed.time.Time;
import fr.amap.archimed.time.TimePeriod;
import fr.amap.archimed.util.NamedValues;

public class MirProcess extends LightInterceptionProcess
{
    private final static Logger logger = LogManager.getLogger();

    private MainParameters parameters;

    private MeteoData meteo;

    private ArchimedScene scene;

    private ModelManager modelManager;

    /**
     * current time period
     */
    private TimePeriod timePeriod;

    /**
     * pixel table list with file/mode info
     */
    private PixelTableCache pixelTableCache;

    /**
     * sky division in sectors
     */
    private Turtle turtle;

    /**
     * false if sun direction is accounted for separately
     */
    private boolean allInTurtle;

    /**
     * true if execute() is called for the first time
     */
    private boolean firstPeriod;

    public MirProcess( Application application ) throws Exception
    {
        scene = application.getScene();
        parameters = application.getMainParameters();

        if ( parameters.getOutputParameters().isAtEachStep() )
            throw new Exception( "atEachStep parameter not yet supported" );

        meteo = application.getMeteo();
        modelManager = application.getModelManager();
        allInTurtle = parameters.getLightInterceptionParameters().isAllInTurtle();
        pixelTableCache = new PixelTableCache( parameters, scene );

        // number of directions
        int skySectorCount = parameters.getLightInterceptionParameters().getSkySectors();

        turtle = new Turtle( DirectionCount.getEnumByInt( skySectorCount ), true, allInTurtle );

        firstPeriod = true;
    }

    public void setTimePeriod( TimePeriod period )
    {
        timePeriod = period;
    }

    @Override
    public void execute() throws Exception
    {
        RadiativeFluxes radiativeFluxes = createRadiativeFluxes();

        runMir( radiativeFluxes, 0 );

        if ( firstPeriod )
            for ( ASNode node : scene )
                skyFraction( node );
        firstPeriod = false;

        // copy intermediate results as node attributes

        for ( ASNode node : scene )
            if ( node instanceof ASItem )
            {
                ASItem item = (ASItem)node;
                NamedValues attrs = item.getAttributes();
                MirNodeInfo mni = (MirNodeInfo)attrs.get( MirNodeInfo.MIR_INFO );

                //                String type = item.getNodeType().toString();
                //                if ( type != "Cobblestone" )
                //                    //                    System.out.println( "tmpIntercepted type " + type + " id " + node.getFileId() + " " + mni.tmpIntercepted );
                //                    System.out.println( "tmpIrradiation id " + node.getFileId() + " " + mni.tmpIrradiation );

                // node PAR+NIR intercepted irradiance 
                attrs.put( PIO_LIGHT_INTERCEPT_IRRADIANCE, mni.stepIrradiance );

                // node visible area in all directions
                //                attrs.put( PIO_LIGHT_INTERCEPT_VISIBLE_AREA, mni.visibleAreaByDirection );

                // node mesh area
                attrs.put( PIO_LIGHT_INTERCEPT_MESH_AREA, mni.getMeshArea() );

                // remove MirNodeInfo
                attrs.remove( MirNodeInfo.MIR_INFO );
            }
    }

    /**
     * create radiative fluxes for all turtle directions
     * @param turtle sky sector division
     */
    private RadiativeFluxes createRadiativeFluxes()
    {
        // latitude in radians
        float latitude = (float)Math.toRadians( parameters.getMeteoParameters().getLatitude() );

        // clearness
        float clearness = (float)meteo.getClearness_fromPeriod( timePeriod );

        // scene rotation in radians
        double sceneRotation = Math.toRadians( parameters.getSceneRotation() );

        // radiation time step
        float radiationTimestep = (float)(parameters.getLightInterceptionParameters().getRadiationTimestep() / 60.0);

        // start/end time
        Time startTime = timePeriod.getStart();
        Time endTime = timePeriod.getEnd();

        // sun position
        double startDecimalHour = startTime.getDecimalHour();
        double endDecimalHour = endTime.getDecimalHour();
        turtle.setSunPosition( latitude, startTime.getDoy(), (startDecimalHour + endDecimalHour) * 0.5f );

        return new TurtleFluxes( turtle ).createFromDirections( latitude, clearness, sceneRotation, startTime, endTime, radiationTimestep );
    }

    /**
     * Computes a pixel table for a direction, store it in the save file if the
     * option saveOnDisk is enabled.
     * 
     * @throws IOException 
     */
    private IPixelTable computePixelTable( Vector3f direction, boolean storePixelTable ) throws IOException
    {
        return new MirDirectional( direction, scene.getPlotBox(), scene.getRoot(), pixelTableCache, parameters, storePixelTable ).computePixelTable();
    }

    /**
     * @return the interception {@link Model} linked to a {@link ASNode}
     * @throws Exception
     */
    private InterceptionModel getInterceptionModel( ASNode node ) throws Exception
    {
        if ( node instanceof ASItem )
        {
            ASItem item = (ASItem)node;
            ModelKey mk = new ModelKey( item.getGroupName(), item.getNodeType(), getCategory() );
            return (InterceptionModel)modelManager.getConfiguredModel( mk );
        }

        throw new Exception( "no functional group name found for node id=" + node.getFileId() );
    }

    /**
     * Executes a simulation and computes the global intercepted light and global
     * irradiation of each MirNode.
     * @param turtle 
     * 
     * @param ir          the radiative fluxes
     * @param currentStep indicates the nth timestep we are in
     * @throws Exception 
     */
    private void runMir( RadiativeFluxes ir, int currentStep ) throws Exception
    {
        logger.info( "MIR Simulation..." );
        //        Timer.mark();
        //        this.currentStep = currentStep;

        for ( ASNode node : scene )
        {
            if ( node instanceof ASItem )
            {
                ASItem item = (ASItem)node;
                MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );
                if ( mni == null )
                {
                    mni = new MirNodeInfo( turtle.getDirectionCount(), item );
                    node.getAttributes().put( MirNodeInfo.MIR_INFO, mni );
                }
                mni.stepIntercepted = 0;
                mni.tmpPARinterceptedLight = 0;
                mni.tmpNIRinterceptedLight = 0;
                // if(!mirParams.isAllInTurtle()) node.nbHits -= node.tmpNbHits;
            }
        }

        // Calculation of visible areas and global intercepted light
        computeVisibleAreas();

        computeStepInterLight( ir );

        computeGlobalIrradiation();

        // Enlève le nombre d'interraction de la lumière du soleil avec le noeud pour le
        // prochain step
        if ( !allInTurtle )
            //            for ( MirNode node : mir.getNodes() )
            for ( ASTreeIterator it = scene.iterator(); it.hasNext(); )
            {
                ASNode node = it.next();
                if ( node instanceof ASItem )
                {
                    MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );
                    mni.nbHits -= mni.tmpNbHits;
                }
            }

        //TODO a déplacer dans musc
        //        // Compute intercepted light with mirParams.isScattering()
        //        if ( mirParams.isScattering() )
        //        {
        //            //            Timer.mark();
        //            computeScattering();
        //            //            System.out.println( "Scattering computation time :" + time.Timer.getElapsedTimeInSeconds() + "s" );
        //            computePARirradiation();
        //            if ( mirParams.isComputeNIR() )
        //                computeNIRirradiation();
        //        }
    }

    /**
     * Computes the visible areas of the nodes for all directions
     * @throws Exception 
     */
    private void computeVisibleAreas() throws Exception
    {
        //            Timer.mark();

        double pixelArea = scene.getPlotBox().getPixelArea();
        int i = 0;
        for ( Iterator<TurtleDirection> it = turtle.getDirectionIterator(); it.hasNext(); )
        {
            TurtleDirection d = it.next();

            if ( !d.isProjectionDone() || d.isDirect() )
            {
                Vector3f dir = d.getVector();
                IPixelTable pixelTable = computePixelTable( dir, true );
                computeVisibleAreas( pixelArea, pixelTable, i );
                logger.info( "Projection from " + (d.isDirect() ? "direct " : "") + "direction " + (i + 1) + " computed" );
                turtle.setProjectionDone( i );
            }
            i++;
        }
        //            System.out.println( "Projection computation time :" + time.Timer.getElapsedTimeInSeconds() + "s" );
    }

    /**
     * Calculates visible area by scene elements for one direction. 
     * It uses interception stacks from a pixel table.
     * 
     * @param incident initial incident light
     * @param pixelTable pixel table used to retrieve nodes by direction and by pixel
     * @param directionCounter direction index
     * @throws Exception 
     * @see archimed Florian/Mir.private Map<MirNode, double[]> computeVisibleAreas( Vector3f direction, double pixelArea, PixelTable pixelTable, int directionCounter, Map<MirNode, double[]> visibleAreas )
     */
    private void computeVisibleAreas( double incident, IPixelTable pixelTable, int directionCounter ) throws Exception
    {
        PlotBox plotBox = scene.getPlotBox();

        for ( int i = plotBox.getTableSize().x - 1; i >= 0; i-- )
            for ( int j = plotBox.getTableSize().y - 1; j >= 0; j-- )
            {
                // Calculates light interception and transmission along a ray

                int nHits = pixelTable.getHitCount( i, j );
                for ( int s = 0; s < nHits; s++ )
                {
                    // pour la génération d'image, cf. version Florian pour le cas upperhit

                    // nth node of the stack (first is the highest)
                    ASNode node = pixelTable.getNode( i, j, s );

                    // compute light intercepted/passing through by node
                    InterceptionModel model = getInterceptionModel( node );
                    InterceptionResult interceptResult = model.interception( incident );

                    // for this direction, add (corrected) pixel area to this node visible area
                    MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );
                    mni.visibleAreaByDirection[ directionCounter ] += incident * mni.stepRatioArea;

                    // stop if no light passing through
                    if ( interceptResult.getPassingThroughArea() == 0 )
                        break;
                }

                //TODO à remettre
                //                        if ( transmitted > 0 && computeGroundImg )
                //                        {
                //                            groundImg[ i ][ j ] += transmitted;
                //                            groundStatistic.addValue( groundImg[ i ][ j ] );
                //                        }
            }
    }

    /**
     * Computes the global intercepted light for all directions.
     * 
     * @param ir the radiative fluxes
     */
    public void computeStepInterLight( RadiativeFluxes ir )
    {
        int j = 1;
        for ( RadiativeFlux flux : ir.getDirectionalRadiations() )
        {
            // if sun direction is added (and thus direct flux will be added later) we take
            // the diffuse, else the global
            double radiationValue = (!allInTurtle) ? flux.getRadiation().getDiffuse() : flux.getRadiation().getGlobal();

            updateGlobalIntercepted( radiationValue, j - 1 );

            if ( j <= turtle.getSectorCount() )
                logger.info( "Direction " + (j) + " treated" );
            j++;
        }
        // Sun direction
        if ( !allInTurtle )
        {
            updateGlobalIntercepted( ir.getTotalRadiation().getDirect(), j - 2 );
            logger.info( "Sun direction treated" );
        }
    }

    // USED BY getNodeIrradiation() in case of volumic interception models 
    //    /**
    //     * Compute the irradiance.
    //     * 
    //     * @param interLight
    //     * @param interceptionModel
    //     * @param node
    //     * @return
    //     */
    //    public static double computeIrradiation( double interLight, InterceptionModel interceptionModel, double area )
    //    {
    //
    //        // set irradiation MJ per m2
    //        double irradiation = 0;
    //
    //        if ( interceptionModel instanceof SurfacicInterceptionModel )
    //        {
    //
    //            if ( area != 0 )
    //            {
    //                irradiation = interLight / area;
    //            }
    //            else
    //            {
    //                irradiation = 0;
    //            }
    //
    //        }
    //        //        else if ( interceptionModel instanceof VolumicInterceptionModel )
    //        //        {
    //        // ABANDON DE VOLUMIC POUR LE MOMENT
    //        // VolumicInterceptionModel model = (VolumicInterceptionModel)
    //        // interceptionModel;
    //        // double lad = model.getLeafAreaDensity(); // m2/m3
    //        // double volume = node.getMesh().getVolume(); // m3
    //        // double area = lad * volume; // m2
    //        //
    //        // // If computed area is zero, set to 0
    //        // if (area != 0) {
    //        // irradiation = interLight / area;
    //        // } else {
    //        // irradiation = 0;
    //        // }
    //        //        }
    //
    //        return irradiation;
    //    }
    //
    // USED in case of volumic interception models 
    //
    //    /**
    //     * Get node irradiation in MJ/m2
    //     * 
    //     * @param node
    //     * @return
    //     */
    //    private double getNodeIrradiation( double interLight, int interceptionModelID, double area )
    //    {
    //        return computeIrradiation( interLight, getInterceptionModel( interceptionModelID ), area );
    //    }

    /**
     * Computes the total and step globalIrradiation for each nodes.
     */
    private void computeGlobalIrradiation()
    {
        for ( ASNode node : scene )
            if ( node instanceof ASItem )
            {
                ASItem item = (ASItem)node;
                MirNodeInfo mni = (MirNodeInfo)item.getAttributes().get( MirNodeInfo.MIR_INFO );

                // node irradiation
                //TODO so far, volumic interception models are not used any more. In case of reuse, use getNodeIrradiation() method
                // double irr = getNodeIrradiation( node.getTmpIntercepted(), node.getInterceptionModelID(), node.getArea() );

                double area = mni.getMeshArea();
                double irr;
                if ( area != 0 )
                    irr = mni.stepIntercepted / area; // irradiation = intercepted light(for this step) / mesh area
                else
                    irr = 0;

                mni.stepIrradiance += irr;

                //TODO à remettre
                //                if ( node.getItem() != null )
                //                    mirStatPlants.get( currentStep ).get( node.getItem().getFather() ).globalIrradiation.addValue( irr );
            }
    }

    /**
     * Computes the fraction of sky a node "sees".
     * 
     * @param node the node
     */
    private void skyFraction( ASNode node )
    {
        MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );

        if ( mni != null ) // no MirNodeInfo in case of root, group, ... nodes
        {
            double skyFraction = 0;
            for ( int i = turtle.getSectorCount() - 1; i >= 0; i-- )
                skyFraction += mni.visibleAreaByDirection[ i ];

            skyFraction /= turtle.getSectorCount();
            skyFraction /= mni.getMeshArea();

            mni.skyFraction = skyFraction;
            //            String type = node instanceof ASItem ? ((ASItem)node).getNodeType().toString() : "null";
            //            if ( type != "Cobblestone" )
            //                System.out.println( "skyfraction type " + type + " id " + node.getFileId() + " " + skyFraction );
        }
    }

    /**
     * Updates the intercepted light of each node. Should be called at each timestep.
     * 
     * @param directionalGlobal flux value
     * @param directionCounter  direction counter
     */
    private void updateGlobalIntercepted( double directionalGlobal, int directionCounter )
    {
        for ( ASNode node : scene )
            if ( node instanceof ASItem )
            {
                ASItem item = (ASItem)node;
                MirNodeInfo mni = (MirNodeInfo)item.getAttributes().get( MirNodeInfo.MIR_INFO );

                double value = mni.visibleAreaByDirection[ directionCounter ] * directionalGlobal;
                mni.stepIntercepted += value;

                //TODO à remettre
                //                if ( node.getItem() != null )
                //                    mirStatPlants.get( currentStep ).get( node.getItem().getFather() ).globalIntercepted.addValue( value );
                //                }
            }
    }
}
