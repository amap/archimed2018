package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import javax.vecmath.Tuple3f;

/**
 * Direction class derived from {@link Direction} holding informations used by {@link Turtle}.
 * 
 * @author François Grand 2018-10-30
 */
public class TurtleDirection extends Direction
{
    /**
     * false if direction is one of the "turtle" directions.
     * true if it is a direct direction (eg. sun direction)
     */
    private boolean direct;

    /**
     * true if scene has been projected in a given direction
     */
    private boolean projectionDone;

    public TurtleDirection( Tuple3f direction, boolean direct )
    {
        super( direction );
        this.direct = direct;
        projectionDone = false;
    }

    public boolean isProjectionDone()
    {
        return projectionDone;
    }

    public void setProjectionDone()
    {
        projectionDone = true;
    }

    /**
     * @return false if direction is one of the "turtle" directions.
     */
    public boolean isDirect()
    {
        return direct;
    }
}
