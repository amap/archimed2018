/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

import org.apache.commons.math3.util.MathUtils;

import fr.amap.archimed.math.geometry.util.CoordinateSystem;
import fr.amap.archimed.math.geometry.util.CoordinatesConversion;
import fr.amap.commons.math.util.SphericalCoordinates;

/**
 * Defines a ground to sky direction.
 * @author Julien Heurtebize
 */
public class Direction
{
    private Vector3f vector;

    private double elevation;

    private double azimut;

    public Direction( double elevation, double azimut, CoordinateSystem coordinateSystem )
    {
        this.elevation = elevation;
        this.azimut = azimut;

        vector = CoordinatesConversion.polarToCartesian( getZenith(), this.azimut, coordinateSystem );
    }

    public Direction( Tuple3f direction )
    {
        Vector3f dir = new Vector3f( direction );
        dir.normalize();

        this.vector = dir;

        SphericalCoordinates sc = new SphericalCoordinates();
        sc.toSpherical( dir.x, dir.y, dir.z );

        elevation = sc.getElevation();
        azimut = CoordinatesConversion.convertAzimut( sc.getAzimut(), CoordinateSystem.TRIGO, CoordinateSystem.COMPASS );

        elevation = MathUtils.normalizeAngle( elevation, Math.PI );
        azimut = MathUtils.normalizeAngle( azimut, Math.PI );
    }

    /**
     * Get the directional vector.
     * @return The vector as a 3d vector
     */
    public Vector3f getVector()
    {
        return vector;
    }

    /**
     * Get the elevation angle.
     * @return The elevation as an angle in radians
     */
    public double getElevation()
    {
        return elevation;
    }

    /**
     * Get the zenithal angle.
     * @return The zenith as an angle in radians.
     */
    public double getZenith()
    {
        return (Math.PI / 2) - elevation;
    }

    /**
     * Get the azimut angle.
     * @return The azimut as an angle in radians.
     */
    public double getAzimut()
    {
        return azimut;
    }
}
