package fr.amap.archimed.application.process;

import fr.amap.archimed.application.ProcessCategory;

/**
 * Process is a step, an aspect of the scene processing.
 * It has inputs and outputs that generally are node attributes identified with standardised strings (see PIO_* below)
 * 
 * @author François Grand
 * 2018-10-11
 */
public abstract class Process
{
    /**
     * process input output attributes names
     * @see ASNode.getAttributes() 
     * LINT : light interception
     * LSCT : light scattering
     * PHSY : photosynthesis
     * ENBL : energy balance
     * SWBL : soil water balance
     * SPLA : soil plant atmosphere interaction
     */

    /**
     * light interception : global (ie. direct + diffuse) irradiance
     */
    public static final String PIO_LIGHT_INTERCEPT_IRRADIANCE = "LINT_IRRADIANCE";

    /**
     * light interception : visible area in all directions
     */
    //    public static final String PIO_LIGHT_INTERCEPT_VISIBLE_AREA = "LINT_VIS_AREA";

    /**
     * light interception : mesh area
     */
    public static final String PIO_LIGHT_INTERCEPT_MESH_AREA = "LINT_MESH_AREA";

    /**
     * process category
     */
    private ProcessCategory category;

    public Process( ProcessCategory cat )
    {
        category = cat;
    }

    public ProcessCategory getCategory()
    {
        return category;
    }

    public abstract void execute() throws Exception;
}
