package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import java.text.NumberFormat;

import javax.vecmath.Point2f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.util.CoordinatesConversion;
import fr.amap.archimed.util.unit.UnitConverter;
import jeeb.lib.util.DefaultNumberFormat;

/**
 * The radiative fluxes.
 * A radiative flux is an amount of power radiated through a given area.
 * It is represented as directional vectors with radiation values.
 *
 * @author J. Dauzat - April 2012, J. Heurtebize (refactoring)
 */
public class RadiativeFluxes
{
    /**
     * components of the total radiation
     */
    private RadiationComponents totalRadiation;

    /**
     * directions
     */
    protected final DirectionContainer directionContainer;

    /**
     * individual fluxes
     */
    private RadiativeFlux[] fluxes;

    private Vector3f directBeam;

    public RadiativeFluxes( DirectionContainer directionContainer )
    {
        this.directionContainer = directionContainer;
        totalRadiation = new RadiationComponents();
        initFluxes();
    }

    public RadiativeFluxes( DirectionContainer directionContainer, RadiationComponents totalRadiation )
    {
        this.directionContainer = directionContainer;
        this.totalRadiation = totalRadiation;
        initFluxes();
    }

    private void initFluxes()
    {
        int size = directionContainer.getNbDirections();

        fluxes = new RadiativeFlux[ size ];
        for ( int i = size - 1; i >= 0; i-- )
            fluxes[ i ] = new RadiativeFlux( directionContainer.get( i ).getVector() );
    }

    /**
     * Add the second incident radiation components to the first incident radiation components.
     *
     * @param irCumulated {@link RadiativeFluxes} object 1 with components in W/m²
     * @param rf {@link RadiativeFluxes} object 2 with components in W/m²
     * @param duration in decimal hour.
     */
    protected void globalCumulateMJ( RadiativeFluxes rf, float duration )
    {
        float factor = UnitConverter.wattToMJ( 1, duration );

        totalRadiation.addScale( factor, rf.totalRadiation );

        if ( rf.totalRadiation.getGlobal() > 0 )
            addScaleFluxes( factor, rf );
    }

    /**
     * add scaled fluxes
     * @param scale factor
     * @param rf added fluxes
     */
    private void addScaleFluxes( float factor, RadiativeFluxes rf )
    {
        for ( int dir = fluxes.length - 1; dir >= 0; dir-- )
            fluxes[ dir ].getRadiation().addScale( factor, rf.getDirectionalRadiations()[ dir ].getRadiation() );
    }

    /**
     * Scale fluxes and total radiation components by a factor.
     * @param factor factor applied to all components
     */
    protected void scale( float factor )
    {
        totalRadiation.scale( factor );
        for ( int dir = fluxes.length - 1; dir >= 0; dir-- )
            fluxes[ dir ].getRadiation().scale( factor );
    }

    /**
     * set global, diffuse, direct components of a flux
     * @param f flux index
     * @param global global component
     * @param diffuse diffuse component
     * @param direct direct component
     */
    protected void setFluxRadiation( int f, float global, float diffuse, float direct )
    {
        fluxes[ f ].setRadiation( global, diffuse, direct );
    }

    public RadiationComponents getTotalRadiation()
    {
        return totalRadiation;
    }

    public RadiativeFlux[] getDirectionalRadiations()
    {
        return fluxes;
    }

    public Vector3f getDirectBeam()
    {
        return directBeam;
    }

    public void setDirectBeam( Vector3f directBeam )
    {
        this.directBeam = directBeam;
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();

        NumberFormat nf = DefaultNumberFormat.getInstance();

        str.append( "\nGlobal radiation integrated over the period (MJ m-2)\n" );
        str.append( "('hemispherical' fluxes, i.e. as measured with horizontal sensor)\n" );
        str.append( "Global: " ).append( totalRadiation.getGlobal() );
        str.append( "\t(diffuse= " ).append( totalRadiation.getDiffuse() ).append( "; direct= " ).append( totalRadiation.getDirect() ).append( ")\n" );
        str.append( "global in sectors\n" );

        int s = 0;
        for ( Direction direction : directionContainer )
        {
            Vector3f dir = direction.getVector();
            Point2f angles = CoordinatesConversion.cartesianToPolar( dir );

            float zenith = (float)Math.toDegrees( angles.x );
            float azimuth = (float)Math.toDegrees( angles.y );

            str.append( "Sector: " ).append( s ).append( " (zenith: " ).append( nf.format( zenith ) ).append( " azimuth:" ).append( nf.format( azimuth ) ).append( ") directionalGlobal: " ).append( fluxes[ s ].getRadiation().getGlobal() ).append( "\n" );
            s++;
        }

        return str.toString();
    }
}
