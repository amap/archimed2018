package fr.amap.archimed.application;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.scheduler.DefaultScheduler;
import fr.amap.archimed.application.scheduler.Scheduler;
import fr.amap.archimed.meteo.MeteoData;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.custom.ElaeisModel;
import fr.amap.archimed.model.lightinterception.TranslucentModel;
import fr.amap.archimed.model.lightinterception.VirtualSensorModel;
import fr.amap.archimed.model.opticalproperties.OpticalPropertiesModel;
import fr.amap.archimed.parameters.ModelConfigurationFile;
import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.scene.ArchimedScene;

/**
 * sequence of {@link Model} organised by a {@link Scheduler}
 * 
 * @author François Grand
 * 2018-09-25
 */
public class Application
{
    private final static Logger logger = LogManager.getLogger();

    private MainParameters mainParameters;

    private ModelManager modelManager;

    private Scheduler scheduler;

    private ArchimedScene scene;

    private MeteoData meteo;

    public Application( MainParameters prms )
    {
        mainParameters = prms;
        scheduler = new DefaultScheduler( this );
    }

    public void execute() throws Exception
    {
        logger.info( "executing Application" );

        scene = new ArchimedScene();
        scene.loadOPS( mainParameters );
        scene.generatePaving( mainParameters );

        // create default parameter provider (from CSV "model manager" file)

        ModelConfigurationFile mcf = new ModelConfigurationFile( getModelManager() );
        mcf.loadCSVConfiguration( mainParameters );
        //            modelManager.registerParameterProvider( mcf );

        // initialise meteo data/load meteo informations file

        initMeteo();

        scheduler.parseProcesses();
        scheduler.execute();
    }

    private void initMeteo() throws IOException
    {
        String meteoPath = mainParameters.getMeteoFilePath();
        logger.info( "loading meteo data from '" + meteoPath + "'" );

        double altitude = mainParameters.getMeteoParameters().getAltitude();
        double latitude = mainParameters.getMeteoParameters().getLatitude();

        meteo = new MeteoData( altitude, latitude );
        meteo.readMeteoFile( meteoPath );
    }

    public ModelManager getModelManager()
    {
        if ( modelManager == null )
        {
            // create model manager

            modelManager = new ModelManager();

            // register default models

            modelManager.registerModel( new OpticalPropertiesModel() ); // optical properties (PAR/NIR scattering factor)
            modelManager.registerModel( new TranslucentModel() ); // transparency parameterised light interception
            modelManager.registerModel( new VirtualSensorModel() ); // virtual sensor

            // create example Elais parameter provider
            //TODO remove !

            ElaeisModel elaeis = new ElaeisModel();
            //            modelManager.registerParameterProvider( elaeis );
        }

        return modelManager;
    }

    public MainParameters getMainParameters()
    {
        return mainParameters;
    }

    public ArchimedScene getScene()
    {
        return scene;
    }

    public MeteoData getMeteo()
    {
        return meteo;
    }
}
