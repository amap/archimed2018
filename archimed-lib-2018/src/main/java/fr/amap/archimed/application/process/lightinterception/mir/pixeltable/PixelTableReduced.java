package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ArchimedScene;

/**
 * MMR pixel table. To each pixel is associated information about node(s) hit by
 * a ray. In this implementation, a hit list is associated to each pixel, hit
 * information is reduced to node id.
 * 
 * @author F. Grand 2018-10-26
 */
public class PixelTableReduced implements IPixelTable
{
    /**
     * number of pixel in X direction
     */
    private int xSize;

    /**
     * number of pixel in X direction
     */
    private int ySize;

    private List<ASNode>[][] pixels;

    /**
     * Constructor
     *
     * @param xSize
     * @param ySize
     * @param upperHit if true, keep only first intercepted node by rays in pixel
     *                 info
     */
    public PixelTableReduced( int xSize, int ySize )
    {
        this.xSize = xSize;
        this.ySize = ySize;
        pixels = new ArrayList[ xSize ][ ySize ];
    }

    @Override
    public int getXSize()
    {
        return xSize;
    }

    @Override
    public int getYSize()
    {
        return ySize;
    }

    @Override
    public void addPixelInfo( int i, int j, ASNode node, float value )
    {
        if ( pixels[ i ][ j ] == null )
            pixels[ i ][ j ] = new ArrayList<>();
        pixels[ i ][ j ].add( node );
    }

    /**
     * An image of the PixelTable
     * 
     * @return
     */
    // @Override
    // public String toString() {
    // NumberFormat nf = DefaultNumberFormat.getInstance();
    // StringBuilder b = new StringBuilder();
    // for (int i = 0; i < xSize; i++) {
    // StringBuffer line = new StringBuffer();
    // for (int j = 0; j < ySize; j++) {
    // List<PixelInfo> list = pixels[i][j];
    // //TFloatArrayList list = pixelsValues[i][j];
    // String token = "-";
    // if (list != null && !list.isEmpty()) {
    // token = nf.format(list.get(0).getValue());
    // //token = nf.format(list.get(0));
    // }
    // line.append(token);
    // line.append(' ');
    // }
    // b.append(line);
    // b.append("\n");
    // }
    // return b.toString();
    // }

    // public boolean isEqualTo(PixelTable pixTable) {
    //// System.out.println("xSize : " + this.xSize + " " + pixTable.xSize);
    //// System.out.println("ySize : " + this.ySize + " " + pixTable.ySize);
    //// System.out.println("pixels lenght : " + this.pixels.length + " " +
    // pixTable.pixels.length);
    //// System.out.println("pixelsUpperHit length: " + this.pixelsUpperHit.length +
    // " " + pixTable.pixelsUpperHit.length);
    //// System.out.println("isUpperHit() : " + this.isUpperHit()+ " " +
    // pixTable.isUpperHit());
    //
    // if(pixTable.isUpperHit() != this.isUpperHit()) return false;
    // if(pixTable.xSize != this.xSize && pixTable.ySize != this.ySize) return
    // false;
    //
    // if(upperHit){
    // if(pixTable.pixelsUpperHit.length != this.pixelsUpperHit.length) return
    // false;
    // for(int i = 0; i < this.pixelsUpperHit.length; i++) {
    // for(int j = 0; j < this.pixelsUpperHit[i].length; j++) {
    // if(pixTable.pixelsUpperHit[i][j].compareTo(this.pixelsUpperHit[i][j]) != 0)
    // return false;
    // }
    // }
    // } else {
    // if(pixTable.pixels.length != this.pixels.length) return false;
    // for(int i = 0; i < this.pixels.length; i++) {
    // for(int j = 0; j < this.pixels[i].length; j++) {
    // if(pixTable.getPixels()[i][j].size() != this.pixels[i][j].size()) return
    // false;
    // for(PixelInfo pixInfo : this.pixels[i][j]) {
    // if(pixTable.getPixels()[i][j].get(this.pixels[i][j].indexOf(pixInfo)).compareTo(pixInfo)
    // != 0) return false;
    // }
    // }
    // }
    // }
    //
    // return true;
    // }

    //    public static byte[] compress( PixelTableReduced pixelTable ) throws IOException
    //    {
    //
    //        ByteArrayOutputStream baos = new ByteArrayOutputStream();
    //        GZIPOutputStream gzipOut = new GZIPOutputStream( baos );
    //
    //        try (ObjectOutputStream objectOut = new ObjectOutputStream( gzipOut ))
    //        {
    //            objectOut.writeObject( pixelTable );
    //        }
    //
    //        byte[] bytes = baos.toByteArray();
    //        return bytes;
    //    }

    //    public static PixelTableReduced uncompress( byte[] bytes ) throws IOException, ClassNotFoundException
    //    {
    //
    //        ByteArrayInputStream bais = new ByteArrayInputStream( bytes );
    //        GZIPInputStream gzipIn = new GZIPInputStream( bais );
    //        PixelTableReduced pixelTable;
    //
    //        try (ObjectInputStream objectIn = new ObjectInputStream( gzipIn ))
    //        {
    //            pixelTable = (PixelTableReduced)objectIn.readObject();
    //        }
    //
    //        return pixelTable;
    //    }

    /**
     * sort all pixels by height
     */
    @Override
    public void sortPixels()
    {
        // there's no point sorting, height is not stored
    }

    /**
     * Reads the saved pixel tables.
     * 
     * @param filePath
     * @param scene
     * @return a PixelTable
     * @throws IOException
     */
    public static PixelTableReduced readProjectionFile( String filePath, ArchimedScene scene ) throws IOException
    {
        try (DataInputStream dis = new DataInputStream( new BufferedInputStream( new FileInputStream( filePath ) ) ))
        {
            int tableXSize = dis.readInt();
            int tableYSize = dis.readInt();

            PixelTableReduced pixTable = new PixelTableReduced( tableXSize, tableYSize );

            int nbSuccessiveNullPixels = 0;

            for ( int i = 0; i < pixTable.getXSize(); i++ )
                for ( int j = 0; j < pixTable.getYSize(); j++ )
                {

                    if ( nbSuccessiveNullPixels > 0 )
                    {
                        //                        pixTable.setValues( i, j, null );
                        nbSuccessiveNullPixels--;
                    }
                    else
                    {
                        int nbValues = dis.readInt();

                        if ( nbValues < 0 )
                        {
                            nbSuccessiveNullPixels = Math.abs( nbValues );
                            //                            pixTable.setValues( i, j, null );
                            nbSuccessiveNullPixels--;

                        }
                        else if ( nbValues == 0 )
                        {
                            //                            pixTable.setValues( i, j, null );
                        }
                        else
                        {
                            List<ASNode> values = new ArrayList<>( nbValues );

                            for ( int p = 0; p < nbValues; p++ )
                            {
                                int nodeID = dis.readInt();
                                values.add( scene.getNodeFromId( nodeID ) );
                            }

                            //                            pixTable.setValues( i, j, values );
                            pixTable.pixels[ i ][ j ] = values;
                        }
                    }
                }

            return pixTable;
        }
    }

    @Override
    public void writeProjectionFile( String filePath ) throws IOException
    {
        try (DataOutputStream dos = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( new File( filePath ) ) ) ))
        {
            dos.writeInt( getXSize() );
            dos.writeInt( getYSize() );

            int nbSuccessiveNullPixels = 0;

            for ( int i = 0; i < getXSize(); i++ )
                for ( int j = 0; j < getYSize(); j++ )
                {
                    //                    List<Integer> pxls = getPixels()[ i ][ j ];
                    List<ASNode> pxls = pixels[ i ][ j ];

                    if ( pxls == null )
                        nbSuccessiveNullPixels++;
                    else
                    {
                        if ( nbSuccessiveNullPixels > 0 )
                        {
                            dos.writeInt( -nbSuccessiveNullPixels );
                            nbSuccessiveNullPixels = 0;
                        }

                        dos.writeInt( pxls.size() );
                        for ( int p = 0; p < pxls.size(); p++ )
                            dos.writeInt( pxls.get( p ).getFileId() );
                    }
                }

            if ( nbSuccessiveNullPixels > 0 )
                dos.writeInt( -nbSuccessiveNullPixels );
        }
    }

    @Override
    public int getHitCount( int x, int y )
    {
        List<ASNode> pxls = pixels[ x ][ y ];
        if ( pxls == null )
            return 0;
        return pxls.size();
    }

    @Override
    public ASNode getNode( int x, int y, int n )
    {
        return pixels[ x ][ y ].get( n );
    }

    @Override
    public float getHeight( int x, int y, int n )
    {
        return 0;
    }
}
