/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

/**
 * @author J. Heurtebize, F. Grand (refactoring, 2018-10-25)
 */
public class RadiationComponents
{
    /**
     * Describes the sunlight that has been scattered by molecules and
     * particules in the atmosphere but that has still made it down to the
     * surface of the earth. In watt per square meter (W/m²).
     */
    private float diffuse;

    /**
     * The sum of diffuse and direct radiation. In watt per square meter (W/m²).
     */
    private float global;

    /**
     * Describe solar radiation traveling on a straight line from the sun down
     * to the surface of the earth. In watt per square meter (W/m²).
     */
    private float direct;

    public RadiationComponents()
    {
        global = diffuse = direct = 0;
    }

    public RadiationComponents( float global, float diffuse, float direct )
    {
        this.global = global;
        this.diffuse = diffuse;
        this.direct = direct;
    }

    public float getDiffuse()
    {
        return diffuse;
    }

    public float getGlobal()
    {
        return global;
    }

    public float getDirect()
    {
        return direct;
    }

    /**
     * add all components of a {@link RadiationComponents} 
     */
    public void add( RadiationComponents rc )
    {
        global += rc.global;
        diffuse += rc.diffuse;
        direct += rc.direct;
    }

    /**
     * multiply all components by a factor
     */
    public void scale( float factor )
    {
        global *= factor;
        diffuse *= factor;
        direct *= factor;
    }

    /**
     * multiply components by factors
     */
    public void scale( float globalFactor, float diffuseFactor, float directFactor )
    {
        global *= globalFactor;
        diffuse *= diffuseFactor;
        direct *= directFactor;
    }

    /**
     * add a scaled component
     * @param scale factor
     * @param rc added component
     */
    public void addScale( float factor, RadiationComponents rc )
    {
        global += factor * rc.global;
        diffuse += factor * rc.diffuse;
        direct += factor * rc.direct;
    }
}
