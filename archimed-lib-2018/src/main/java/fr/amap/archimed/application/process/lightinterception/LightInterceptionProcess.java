package fr.amap.archimed.application.process.lightinterception;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

public abstract class LightInterceptionProcess extends Process
{
    public LightInterceptionProcess()
    {
        super( ProcessCategory.LightInterception );
    }
}
