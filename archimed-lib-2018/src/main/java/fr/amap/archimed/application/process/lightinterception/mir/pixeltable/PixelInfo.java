/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

import fr.amap.archimed.scene.ASNode;

/**
 * Pixel's informations in a PixelTable.
 * @author julien
 */
public class PixelInfo implements Comparable<PixelInfo>
{
    /**
     * intercepted node
     */
    private ASNode node;

    /**
     * height of intercepted node
     */
    private final float value;

    /**
     * Constructor
     *
     * @param value
     * @param nodeID
     */
    public PixelInfo( float value, ASNode node )
    {
        this.value = value;
        this.node = node;
    }

    public float getValue()
    {
        return value;
    }

    public ASNode getNode()
    {
        return node;
    }

    @Override
    public int compareTo( PixelInfo pix )
    {
        if ( pix.value > this.value )
            return 1;

        if ( pix.value < this.value )
            return -1;

        return 0;
    }
}
