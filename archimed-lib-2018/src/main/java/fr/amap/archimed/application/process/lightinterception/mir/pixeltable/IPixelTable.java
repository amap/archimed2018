package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

import java.io.IOException;

import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ArchimedScene;

/**
 * interface for pixel tables
 * 
 * @author François Grand
 */
public interface IPixelTable
{
    /**
     * get number of pixel in x dimension
     */
    public int getXSize();

    /**
     * get number of pixel in y dimension
     */
    public int getYSize();

    /**
     * add information for (x,y) position
     * @param x pixel x position
     * @param y pixel y position
     * @param node hit node
     * @param height height of hit node
     */
    public void addPixelInfo( int x, int y, ASNode node, float height );

    /**
     * get number of hits at (x,y) position 
     * @param x pixel x position
     * @param y pixel y position
     */
    public int getHitCount( int x, int y );

    /**
     * get nth hit node at (x,y) position
     * @param x pixel x position
     * @param y pixel y position
     * @param n nth node in hit list at (x,y) position
     */
    public ASNode getNode( int x, int y, int n );

    /**
     * get height of nth hit node at (x,y) position
     * @param x pixel x position
     * @param y pixel y position
     * @param n nth node in hit list at (x,y) position
     */
    public float getHeight( int x, int y, int n );

    /**
     * for all pixels, sort pixel hit list by decreasing height (first element is the highest) 
     */
    public void sortPixels();

    /**
     * read a pixel table from a file
     * @param filePath path to the file to be read
     * @param scene scene containing node whose ids are in the read file
     * @throws IOException
     */
    @SuppressWarnings("unused")
    public static PixelTable readProjectionFile( String filePath, ArchimedScene scene ) throws IOException
    {
        throw new IOException( "not implemented" );
    }

    /**
     * write pixel table to a file
     * @param filePath path to the file to be written
     * @throws IOException
     */
    public void writeProjectionFile( String filePath ) throws IOException;
}
