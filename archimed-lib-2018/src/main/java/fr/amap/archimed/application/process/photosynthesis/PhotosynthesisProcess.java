package fr.amap.archimed.application.process.photosynthesis;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

public class PhotosynthesisProcess extends Process
{
    public PhotosynthesisProcess()
    {
        super( ProcessCategory.Photosynthesis );
    }

    @Override
    public void execute()
    {
    }
}
