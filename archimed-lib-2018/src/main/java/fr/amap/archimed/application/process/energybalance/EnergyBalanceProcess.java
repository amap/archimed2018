package fr.amap.archimed.application.process.energybalance;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

public class EnergyBalanceProcess extends Process
{
    public EnergyBalanceProcess()
    {
        super( ProcessCategory.EnergyBalance );
    }

    @Override
    public void execute()
    {
    }
}
