/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import javax.vecmath.Vector3f;

/**
 * @author Julien Heurtebize, F. Grand (refactoring)
 */
public class RadiativeFlux
{
    private Vector3f direction;

    private RadiationComponents radiation = new RadiationComponents();

    public RadiativeFlux( Vector3f dir )
    {
        direction = dir;
    }

    public Vector3f getDirection()
    {
        return direction;
    }

    public RadiationComponents getRadiation()
    {
        return radiation;
    }

    public void setRadiation( float global, float diffuse, float direct )
    {
        radiation = new RadiationComponents( global, diffuse, direct );
    }
}
