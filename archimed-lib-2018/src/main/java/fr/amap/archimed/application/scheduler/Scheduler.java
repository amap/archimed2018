package fr.amap.archimed.application.scheduler;

import java.util.ArrayList;
import java.util.List;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

/**
 * process scheduler
 * determines the model sequence and executes it
 * 
 * @author François Grand
 * 2018-10-02
 */
public abstract class Scheduler
{
    protected List<Process> processes;

    public Scheduler()
    {
        processes = new ArrayList<>();
    }

    public abstract void parseProcesses() throws Exception;

    protected Process getProcess( ProcessCategory cat )
    {
        for ( Process p : processes )
            if ( p.getCategory().equals( cat ) )
                return p;

        return null;
    }

    protected void addProcess( Process proc ) throws Exception
    {
        if ( getProcess( proc.getCategory() ) != null )
            throw new Exception( "scheduler : a " + proc.getCategory() + " process has already been added" );

        processes.add( proc );
    }

    public void execute() throws Exception
    {
        for ( Process model : processes )
            model.execute();
    }
}
