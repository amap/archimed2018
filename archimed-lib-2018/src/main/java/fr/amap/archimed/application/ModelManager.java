package fr.amap.archimed.application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import fr.amap.archimed.model.Model;
import fr.amap.archimed.parameters.ModelKey;

/**
 * factory for {@link Model} and {@link ParameterProvider} classes
 * 
 * @author François Grand
 * 2018-10-02
 */
public class ModelManager
{
    /**
     * registred models for all functional group/type/process categories
     * ie. all possible models
     */
    private List<Model> registredModels;

    /**
     * configured models (found in configuration file) : only one per key
     */
    private Map<ModelKey, Model> configuredModels;

    /**
     * registred parameter providers
     */
    //    private List<ParameterProvider> registredParameterProviders;

    /**
     * @param mainParameters main Archimed configuration (input property file)
     */
    public ModelManager()
    {
        //        registredParameterProviders = new ArrayList<>();
        configuredModels = new HashMap<>();
        registredModels = new ArrayList<>();
    }

    /**
     * add a model to list of all possible ones
     */
    public void registerModel( Model model )
    {
        registredModels.add( model );
    }

    //    /**
    //     * add a parameter provider (at the beginning of the list) 
    //     */
    //    public void registerParameterProvider( ParameterProvider provider )
    //    {
    //        registredParameterProviders.add( 0, provider );
    //    }

    /**
     * add a model to list of configured models
     */
    public void addConfiguredModel( ModelKey mk, Model model )
    {
        configuredModels.put( mk, model );
    }

    public Model getConfiguredModel( ModelKey modelKey ) throws Exception
    {
        for ( Entry<ModelKey, Model> entry : configuredModels.entrySet() )
            if ( entry.getKey().matches( modelKey ) )
                return entry.getValue();

        throw new Exception( "no model found for key " + modelKey );
    }

    /**
     * get models matching a key
     * @param modelKey filter key
     * @throws Exception
     */
    public List<Model> getRegistredModels( ModelKey modelKey )
    {
        return registredModels.stream().filter( model -> model.matchesKey( modelKey ) ).collect( Collectors.toList() );
    }

    //    public ParameterProvider getParameterProvider( ModelKey key ) throws Exception
    //    {
    //        throw new Error( "not implemented" );
    //    }
}
