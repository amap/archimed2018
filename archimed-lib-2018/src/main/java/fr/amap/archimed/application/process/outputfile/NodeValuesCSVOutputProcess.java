package fr.amap.archimed.application.process.outputfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.vecmath.Point3f;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;
import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.scene.ASItem;
import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ArchimedScene;
import fr.amap.archimed.util.NamedValues;

public class NodeValuesCSVOutputProcess extends Process
{
    /**
     * time step column title
     */
    private final static String timestepHeader = "step";

    /**
     * plant id column title
     */
    private final static String plantidHeader = "plantId";

    /**
     * node id column title
     */
    private final static String nodeidHeader = "nodeId";

    /**
     * node mesh area column title
     */
    private final static String meshareaHeader = "meshArea";

    /**
     * global (direct + diffuse) node irradiance column title
     */
    private final static String globalirradianceHeader = "globalIrradiance";

    /**
     * node weighted barycentre column title
     */
    private final static String zbarycentreHeader = "barycentreZ";

    /**
     * possible column titles
     */
    public final static String[] availableHeaders = { timestepHeader, plantidHeader, nodeidHeader, meshareaHeader, globalirradianceHeader, zbarycentreHeader };

    /**
     * default value delimiter
     */
    private String valueDelimiter = ";";

    /**
     * default record (line) separator
     */
    private String recordSeparator = "\n";

    private MainParameters parameters;

    /**
     * current time step
     */
    private int timeStep;

    /**
     * current time period
     */
    //    private TimePeriod timePeriod;

    /**
     * path to ouput file
     */
    private String outputPath;

    /**
     * scene holding nodes written to CSV
     */
    private ArchimedScene scene;

    public NodeValuesCSVOutputProcess( MainParameters prms, ArchimedScene scene )
    {
        super( ProcessCategory.FileOutput );
        parameters = prms;
        this.scene = scene;
    }

    /**
     * set current time period and step number
     * @param period current time period
     * @param step step number of current time period
     */
    //    public void setTimePeriod( TimePeriod period, int step )
    public void setTimePeriod( int step )
    {
        //        timePeriod = period;
        timeStep = step;
    }

    private String createCurrentOutputDirectory() throws IOException
    {
        String path = parameters.getOutputDirectory( scene.getPlotBox() );

        File f = new File( path );
        if ( !f.exists() )
            if ( !f.mkdirs() )
                throw new IOException( "cannot create output directory " + System.getProperty( "user.dir" ) + "/" + path );

        return path;
    }

    @Override
    public void execute() throws Exception
    {
        if ( outputPath == null )
            outputPath = createCurrentOutputDirectory();

        //        String dirPath = createCurrentOutputDirectory();
        exportNodesValuesToCSV( outputPath + File.separatorChar + "nodes_values.csv", availableHeaders );
    }

    /**
     * compute header line from header list
     */
    private String computeHeader( String[] headers )
    {
        StringBuilder sb = new StringBuilder();
        for ( String h : headers )
        {
            if ( sb.length() > 0 )
                sb.append( valueDelimiter );
            sb.append( h );
        }
        return sb.toString();
    }

    private void addValue( StringBuilder sb, double value )
    {
        if ( sb.length() > 0 )
            sb.append( valueDelimiter );
        sb.append( value );
    }

    private void addValue( StringBuilder sb, int value )
    {
        if ( sb.length() > 0 )
            sb.append( valueDelimiter );
        sb.append( value );
    }

    private void addValue( StringBuilder sb, String value )
    {
        if ( sb.length() > 0 )
            sb.append( valueDelimiter );
        sb.append( value );
    }

    /**
     * Export the values of the nodes in a CSV file. The file is created if needed.
     * 
     * @param path CSV file path
     * @param output column names
     * @throws IOException 
     */
    private void exportNodesValuesToCSV( String path, String[] columns ) throws IOException
    {
        String header = computeHeader( columns );
        File fileOut = new File( path );
        try (FileWriter fileWriter = new FileWriter( fileOut, true ))
        {
            try (BufferedReader br = new BufferedReader( new FileReader( path ) ))
            {
                // file creation : write header
                if ( br.readLine() == null )
                {
                    fileWriter.append( header );
                    fileWriter.append( recordSeparator );
                }

                StringBuilder sb = new StringBuilder();
                for ( ASNode node : scene )
                    if ( node instanceof ASItem )
                    {
                        ASItem item = (ASItem)node;
                        NamedValues attrs = item.getAttributes();

                        if ( attrs.hasValue( PIO_LIGHT_INTERCEPT_IRRADIANCE ) )
                        {
                            double irradiance = attrs.getDouble( PIO_LIGHT_INTERCEPT_IRRADIANCE );
                            double meshArea = attrs.getDouble( PIO_LIGHT_INTERCEPT_MESH_AREA );

                            sb.setLength( 0 );

                            for ( String h : columns )
                                switch ( h )
                                {
                                    case timestepHeader: // time step
                                        addValue( sb, timeStep );
                                        break;

                                    case plantidHeader: // plant id
                                        addValue( sb, item.getObjectId() );
                                        break;

                                    case nodeidHeader: // node id
                                        addValue( sb, node.getFileId() );
                                        break;

                                    case meshareaHeader: // mesh area
                                        addValue( sb, meshArea );
                                        break;

                                    case globalirradianceHeader: // global (direct+diffuse) irradiance
                                        addValue( sb, irradiance );
                                        break;

                                    case zbarycentreHeader: // mesh barycentre Z coordinate
                                        Point3f bary = item.getMesh().computeAreaWeigthedBarycentre();
                                        if ( bary == null )
                                            addValue( sb, "NaN" );
                                        else
                                            addValue( sb, bary.z );
                                        break;

                                    default:
                                        throw new IOException( "unsupported '" + h + "' CSV column header" );
                                }

                            fileWriter.append( sb.toString() );
                            fileWriter.append( recordSeparator );
                        }
                    }
            }
        }
    }
}
