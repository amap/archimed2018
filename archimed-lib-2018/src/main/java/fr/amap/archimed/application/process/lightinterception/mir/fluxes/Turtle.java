package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.math.geometry.element.shape.Circle2D;
import fr.amap.archimed.math.geometry.element.shape.mesh.HullMesh;
import fr.amap.archimed.math.geometry.util.FisheyeProjection;
import fr.amap.archimed.meteo.micrometeo.Sun;
import jeeb.lib.structure.geometry.mesh.Mesh;

/**
 * 2018-10-25 : refactoring (F. Grand) 
 */
public class Turtle extends DirectionContainer
{
    private final static Logger logger = LogManager.getLogger();

    private Mesh mesh;

    /**
     * false if sun direction is accounted for separately
     */
    private final boolean allInTurtle;

    /**
     * sun information
     */
    private Sun sun;

    /**
     * number of sector in turtle
     */
    private int sectorCount;

    /**
     * total number of directions (may include independent sun direction)
     */
    private int directionCount;

    /**
     * @param nbDirections code for number of directions
     * @param onlyUpward true if only directions with z > 0 must be computed (ie. hemispherical turtle) or false for spherical turtle
     * @param allInTurtle false if sun direction is accounted for separately
     */
    public Turtle( DirectionCount nbDirections, boolean onlyUpward, boolean allInTurtle )
    {
        this.allInTurtle = allInTurtle;

        directionCount = nbDirections.getDirectionCount();
        if ( !allInTurtle )
            directionCount++;

        switch ( nbDirections )
        {
            case _1:
                initSingleDirection();
                break;

            default:
                init( nbDirections.getDirectionOrder(), onlyUpward );
        }
    }

    private void initSingleDirection()
    {
        final int dirCount = allInTurtle ? 1 : 2;
        directions = new TurtleDirection[ dirCount ];
        directions[ 0 ] = new TurtleDirection( new Vector3f( 0, 0, 1 ), false );
    }

    /**
     * @param scale from 1 to 5 (respectively 6 to 406 directions)
     * @param onlyUpward true if init
     */
    private void init( int scale, boolean onlyUpward )
    {
        mesh = buildTurtle( scale );
        Point3f[] points = mesh.getPoints();

        sectorCount = points.length;
        if ( onlyUpward )
            sectorCount /= 2;

        directions = new TurtleDirection[ directionCount ];

        int sector = 0;
        for ( Point3f point : points )
            if ( !(onlyUpward && (point.z < 0.0)) )
            {
                directions[ sector ] = new TurtleDirection( point, false );
                sector++;
            }
    }

    /**
     * <p>Build a turtle based on 6 upward directions.</p>
     * These directions are the seeds for building turtles with more directions.
     * @param scale from 1 to 5 (respectively 6 to 406 directions)
     * @return 
     */
    private static Mesh buildTurtle( int scale )
    {
        double[] elevation6 = { 90.00, 26.57, 26.57, 26.57, 26.57, 26.57 };
        double[] azimuth6 = { 0, 180, 252, 324, 36, 108 };

        ArrayList<Point3f> points = new ArrayList<>();

        double x, y, z, elevation, azimuth;
        for ( int d = 0; d < 6; d++ )
        {
            elevation = Math.toRadians( elevation6[ d ] );
            azimuth = Math.toRadians( azimuth6[ d ] + 180 );
            x = y = Math.cos( elevation );
            x *= Math.sin( azimuth );
            y *= Math.cos( azimuth );
            z = Math.sin( elevation );

            points.add( new Point3f( (float)x, (float)y, (float)z ) );
            points.add( new Point3f( (float)-x, (float)-y, (float)-z ) );
        }

        Mesh hullMesh = HullMesh.getMesh( points );

        // add new directions (normalised barycentre of hull faces) if direction count >= 16

        for ( int s = 1; s < scale; s++ )
        {
            int[][] paths = hullMesh.getPaths(); // faces (1st dim : face index, 2nd dim : point index)
            Point3f[] pts = hullMesh.getPoints();

            for ( int f = 0; f < paths.length; f++ ) // for each face
            {
                int[] path = paths[ f ]; // points of the face

                // compute normalised barycentre

                Vector3f barycentre = new Vector3f();
                for ( int p = 0; p < path.length; p++ ) // for each point
                    barycentre.add( pts[ path[ p ] ] );
                barycentre.normalize();

                // add barycentre of the face to the point list of the turtle
                points.add( new Point3f( barycentre ) );
            }

            hullMesh = HullMesh.getMesh( points );
        }

        return hullMesh;
    }

    public Sun getSun()
    {
        return sun;
    }

    private void setSunDirection( Vector3f direction )
    {
        directions[ directions.length - 1 ] = new TurtleDirection( direction, true );
    }

    //    /**
    //     * set sun position
    //     * @param zenith zenith angle of the sun 
    //     * @param azimut azimut angle of the sun
    //     * @param decimalHour in solar time
    //     */
    //    public void setSunPosition( double zenith, double azimut, double decimalHour )
    //    {
    //        sun = new Sun( zenith, azimut );
    //        sun.setPosition( decimalHour );
    //        
    //        if ( !allInTurtle )
    //            setSunDirection( sun.getPosition().getDirection() );
    //    }

    /**
     * set sun position
     * @param latitude in radians
     * @param doy Day of the year, number of the day in the year between 1 (1 January) and 365 or 366 (31 December). 
     * @param decimalHour in solar time
     */
    public void setSunPosition( float latitude, int doy, double decimalHour )
    {
        sun = new Sun( latitude, doy );
        sun.setPosition( decimalHour );

        if ( !allInTurtle )
            setSunDirection( sun.getPosition().getDirection() );
    }

    /**
     * 
     * @param turtle
     * @param colors An array representing a color for each direction
     * @return 
     */
    public static BufferedImage mapTurtleV2( DirectionContainer turtle, Color[] colors )
    {
        int scale = 2;
        int nbPixels = 600;
        int border = 30;

        Point2f center = new Point2f( nbPixels / 2.0f, nbPixels / 2.0f );
        float radius = center.x - border;

        BufferedImage bimg = new BufferedImage( nbPixels, nbPixels, BufferedImage.TYPE_INT_RGB );
        Graphics2D gr = bimg.createGraphics();

        gr.setColor( Color.white );
        gr.fillRect( 0, 0, nbPixels, nbPixels );

        int nbDirections = turtle.getNbDirections();

        // estimation de la distance moyenne entre les centres des secteurs voisins
        double deltaMax = 1.2113 * Math.exp( -0.608 * scale );
        logger.info( "scale= " + scale + "\t" + "deltaMax: " + Math.toDegrees( deltaMax ) );
        deltaMax *= 1.1; // for safety

        for ( int i = 0; i < nbPixels; i++ )
        {
            for ( int j = 0; j < nbPixels; j++ )
            {
                Vector3f pointDirection = FisheyeProjection.fromProjToDir( new Point2f( i, j ), center, radius );

                if ( pointDirection.z > 0 )
                {
                    // find nearest turtle sector
                    Vector3f pixelDirection = new Vector3f( pointDirection );

                    double pixelElevation = Math.asin( pixelDirection.z );

                    float angleMin = (float)(Math.PI / 2.0);
                    int sector = 0;

                    for ( int s = 0; s < nbDirections; s++ )
                        if ( Math.abs( pixelElevation - turtle.get( s ).getElevation() ) < deltaMax )
                        {
                            float dotPro = pixelDirection.dot( turtle.get( s ).getVector() );

                            //angle between the sector and the pixel direction
                            float angle = (float)Math.acos( dotPro );

                            if ( angle < angleMin )
                            { //get the closest sector
                                angleMin = angle;
                                sector = s;
                            }
                        }

                    gr.setColor( colors[ sector ] );
                    gr.fillRect( i, nbPixels - j - 1, 1, 1 ); // NB: Y coordinate inverted for image
                }
            }
        }

        return bimg;
    }

    /**
     * Distributes direct radiation in directions sectors (based on calculation
     * of intersection between sun halo and directions sectors)
     *
     * @param direct
     * @param sunDirection
     * @param directions
     * @return direct radiation in directions sectors
     */
    public static float[] directInTurtle( float direct, Vector3f sunDirection, DirectionContainer directions )
    {
        float[] weights = new float[ directions.getNbDirections() ];

        if ( sunDirection.z < 0 )
            return weights;

        double weightSum = 0;

        // plane angle corresponding to sector solid angle of 2PI/nb sectors
        // Note: solid angle W = 2PI (1-cos(angle/2)) => cos(angle/2)= (46-1) / 46
        float directionsSectorRadius = (float)Math.acos( (directions.getNbDirections() - 1) / (double)directions.getNbDirections() );
        float sunHaloRadius = directionsSectorRadius / 2.0f; // TODO create method setsunHaloRadius

        //sunHaloRadius = Float.min(sunHaloRadius, 0.25f);

        for ( int dir = 0; dir < directions.getNbDirections(); dir++ )
        {
            float angle = (float)Math.acos( sunDirection.dot( directions.get( dir ).getVector() ) );

            weights[ dir ] = Circle2D.lumenArea( angle, directionsSectorRadius, sunHaloRadius );

            weightSum += weights[ dir ];
        }

        for ( int dir = 0; dir < directions.getNbDirections(); dir++ )
            if ( weightSum == 0 )
                weights[ dir ] = 0;
            else
                weights[ dir ] *= direct / weightSum;

        return weights;
    }

    private TurtleDirection[] getDirections()
    {
        return (TurtleDirection[])directions;
    }

    public Iterator<TurtleDirection> getDirectionIterator()
    {
        return Arrays.stream( getDirections() ).iterator();
    }

    public int getSectorCount()
    {
        return sectorCount;
    }

    public int getDirectionCount()
    {
        return directionCount;
    }

    /**
     * set flag "projection done" for a given direction
     * @param direction direction index
     */
    public void setProjectionDone( int direction )
    {
        getDirections()[ direction ].setProjectionDone();
    }

    public Mesh getMesh()
    {
        return mesh;
    }
}
