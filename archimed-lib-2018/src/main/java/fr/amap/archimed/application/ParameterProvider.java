package fr.amap.archimed.application;

/**
 * interface implemented by objects that provide parameter configurations
 * 
 * @author François Grand
 * 2018-09-25
 */
//public interface ParameterProvider
//{
//    /**
//     * get model parameters for a given functional group/functional type/process category
//     * @param groupId id of the functional group
//     * @param groupName name of the functional group
//     * @param type functional type
//     * @param category process category
//     */
//    //    public ModelParameters getParameters( int groupId, NodeType type, ProcessCategory category );
//    // public ModelParameters getParameters( String groupName, NodeType type, ProcessCategory category );
//
//    /**
//     * get model parameters for a given node for a functional type/process category
//     * @param type functional type
//     * @param category process category
//     * @param node
//     */
//    //  public ModelParameters getParameters( NodeType type, ProcessCategory category, ASNode node );
//
//    //    public ModelParameters getParameters( ModelKey modelKey );
//
//    //    public ModelParameters getParameters( ModelKey modelKey, ASNode node );
//
//    /**
//     * determine if this provider can handle a model key  
//     */
//    //    public boolean canHandleModelKey( ModelKey key );
//}