package fr.amap.archimed.application.process.lightinterception.mir;

import java.io.IOException;

import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Vector3f;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.IPixelTable;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.PixelTable;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.PixelTableCache;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.PixelTableReduced;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.PixelTableUpperhit;
import fr.amap.archimed.math.geometry.element.shape.mesh.TriangulatedMesh;
import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.scene.ASItem;
import fr.amap.archimed.scene.ASNode;
import fr.amap.archimed.scene.ASTreeIterator;
import fr.amap.archimed.scene.PlotBox;

/**
 * nodes projection, pixel table creation, "areaVisible" computation
 * 
 * @author François Grand
 * 
 *         2018-09-14
 */
public class MirDirectional
{
    private final static Logger LOGGER = LogManager.getLogger();

    /**
     * projection direction
     */
    private final Vector3f direction;

    /**
     * scene nodes
     */
    //    private List<MirNode> nodes;

    /**
     * pixel table cache
     */
    private final PixelTableCache pixelTableCache;

    /**
     * Mir parameters
     */
    private MainParameters mainParameters;

    /**
     * scene toricity
     */
    private boolean toricity;

    /**
     * false if computed pixel table should not be stored in cache/file
     */
    private boolean storePixelTable;

    /**
     * plot coordinates    
     */
    private PlotBox plotBox;

    /**
     * root node of the object to project
     */
    private ASNode rootNode;

    public MirDirectional( Vector3f direction, PlotBox plotBox, ASNode rootNode, PixelTableCache tableCache, MainParameters mainPrms, boolean storeTable )
    {
        this.direction = direction;
        this.plotBox = plotBox;
        //        this.nodes = mirNodes;
        this.rootNode = rootNode;
        pixelTableCache = tableCache;
        mainParameters = mainPrms;
        storePixelTable = storeTable;

        switch ( mainParameters.getLightInterceptionParameters().getToricity() )
        {
            case NON_TORIC_FINITE_BOX_TOPOLOGY:
                toricity = false;
                break;

            case TORIC_INFINITE_BOX_TOPOLOGY:
                toricity = true;
                break;

            default:
                throw new IllegalArgumentException( "Toricity mode not supported !" );
        }
    }

    /**
     * create a pixel table and stores it in the cache
     * @return created pixel table
     * @throws IOException
     */
    private IPixelTable createAndStorePixelTable() throws IOException
    {
        IPixelTable pixTable = projectNodes();

        if ( storePixelTable )
            pixelTableCache.putPixelTable( direction, pixTable );

        return pixTable;
    }

    /**
     * Computes a pixel table for a direction, store it in the save file if the
     * option saveOnDisk is enabled.
     * 
     * @throws IOException 
     */
    public IPixelTable computePixelTable() throws IOException
    {
        switch ( mainParameters.getLightInterceptionParameters().getProjectionPolicy() )
        {
            case COMPUTE_EVERY_TIME:
                return projectNodes();

            case KEEP_IN_MEMORY:
                // lookup in cache
                IPixelTable pixTable = pixelTableCache.getPixelTable( direction );
                if ( pixTable == null )
                    pixTable = createAndStorePixelTable();
                return pixTable;

            case SAVE_TO_DISK:
                try
                {
                    boolean wasInCache = pixelTableCache.hasPixelTable( direction );

                    // lookup in cache
                    pixTable = pixelTableCache.getPixelTable( direction );

                    // recompute hit count since it's not stored in projection file
                    if ( !wasInCache )
                        for ( ASTreeIterator it = rootNode.treeIterator(); it.hasNext(); )
                        {
                            ASNode node = it.next();
                            if ( node instanceof ASItem )
                            {
                                MeshProjection proj = computeRatioArea( direction, (ASItem)node );
                                computeNbHits( proj, node );
                            }
                        }
                }
                catch( IOException ex )
                {
                    LOGGER.warn( "Cannot read projection file ! Program will compute it. Try to restart the program." );
                    pixTable = createAndStorePixelTable();
                }
                return pixTable;

            default:
                throw new Error( "MirDirectional.computePixelTable : unhandled 'Projection' value " );
        }
    }

    /**
     * Computes pixel table of archiNodes projection.
     * @return filled pixel table
     * @throws IOException 
     */
    private IPixelTable projectNodes() throws IOException
    {
        Point2i tableSize = plotBox.getTableSize();
        boolean upperHit = mainParameters.isUpperHit();
        boolean reducedTable = mainParameters.getLightInterceptionParameters().isReducedTable();

        IPixelTable pixTable;
        if ( reducedTable )
        {
            if ( upperHit )
                throw new IOException( "invalid parameter combination reducedTable=true, upperHit=true" );

            pixTable = new PixelTableReduced( tableSize.x, tableSize.y );
        }
        else
        {
            if ( upperHit )
                pixTable = new PixelTableUpperhit( tableSize.x, tableSize.y );
            else
                pixTable = new PixelTable( tableSize.x, tableSize.y );
        }

        /* Project scene archiNodes */

        for ( ASTreeIterator it = rootNode.treeIterator(); it.hasNext(); )
        {
            ASNode node = it.next();

            if ( node instanceof ASItem )
            {
                MeshProjection meshProjection = computeRatioArea( direction, (ASItem)node );

                MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );
                mni.tmpNbHits = 0;

                for ( Pixel pix : meshProjection.getProjection() )
                {
                    int i = pix.i;
                    int j = pix.j;
                    if ( toricity )
                    {
                        while ( i < 0 )
                            i += tableSize.x;

                        while ( j < 0 )
                            j += tableSize.y;

                        while ( i >= tableSize.x )
                            i -= tableSize.x;

                        while ( j >= tableSize.y )
                            j -= tableSize.y;

                        if ( !reducedTable )
                            pixTable.addPixelInfo( i, j, node, pix.val );

                        mni.nbHits++;
                        mni.tmpNbHits++;
                    }
                    else if ( (i >= 0) && (i < tableSize.x) && (j >= 0) && (j < tableSize.y) )
                    {
                        if ( !reducedTable )
                            pixTable.addPixelInfo( i, j, node, pix.val );

                        mni.nbHits++;
                        mni.tmpNbHits++;
                    }
                }
            }
        }

        pixTable.sortPixels();

        return pixTable;
    }

    /**
     * Computes the ratio area for a node
     * 
     * @param dir
     * @param node
     * @return
     */
    private MeshProjection computeRatioArea( Vector3f dir, ASItem node )
    {
        TriangulatedMesh mesh = node.getMesh();

        MeshProjection meshProjection = MeshProjection.projectMeshOnGround( mesh, dir, new Point2f( plotBox.getInfCorner().x, plotBox.getInfCorner().y ), // scene origin
                plotBox.getPixelSize().x, plotBox.getPixelSize().y );

        float ratioArea = 1;
        if ( meshProjection.getProjectedPixelsArea() > 0 )
            ratioArea = meshProjection.getProjectedMeshArea() / meshProjection.getProjectedPixelsArea();

        // test
        // ratioArea = 1;
        //        node.setTmpRatioArea( ratioArea );
        MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );
        mni.stepRatioArea = ratioArea;

        return meshProjection;
    }

    /**
     * Compute the number of time a node is touched by rays.
     * 
     * @param meshProjection
     * @param node
     * @param nodeID
     */
    private void computeNbHits( MeshProjection meshProjection, ASNode node )
    {
        int tableXSize = plotBox.getTableSize().x;
        int tableYSize = plotBox.getTableSize().y;

        MirNodeInfo mni = (MirNodeInfo)node.getAttributes().get( MirNodeInfo.MIR_INFO );

        for ( Pixel pix : meshProjection.getProjection() )
        {
            int i = pix.i;
            int j = pix.j;
            if ( toricity )
            {
                while ( i < 0 )
                {
                    i += tableXSize;
                }
                while ( j < 0 )
                {
                    j += tableYSize;
                }
                while ( i >= tableXSize )
                {
                    i -= tableXSize;
                }
                while ( j >= tableYSize )
                {
                    j -= tableYSize;
                }
                mni.nbHits++;

            }
            else if ( (i >= 0) && (i < tableXSize) && (j >= 0) && (j < tableYSize) )
                mni.nbHits++;
        }
    }
}
