/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import java.util.Arrays;
import java.util.Iterator;

import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Transformations;

/**
 *
 * @author Julien Heurtebize
 */
public class DirectionContainer implements Iterable<Direction>
{
    protected Direction[] directions;

    public DirectionContainer()
    {
    }

    public DirectionContainer( int directionCount )
    {
        directions = new Direction[ directionCount ];
    }

    public void add( Direction direction, int d )
    {
        directions[ d ] = direction;
    }

    /**
     * Get the direction at the specified index.
     * @param directionID The direction id.
     * @return The direction or null if the index is out of bounds.
     */
    public Direction get( int directionID )
    {
        if ( directionID < 0 || directionID > directions.length - 1 )
        {
            return null;
        }

        return directions[ directionID ];
    }

    /**
     * Get the number of directions.
     * @return 
     */
    public int getNbDirections()
    {
        return directions.length;
    }

    public Vector3f[] getDirectionVectors()
    {
        Vector3f[] vectors = new Vector3f[ directions.length ];

        for ( int i = 0; i < directions.length; i++ )
        {
            vectors[ i ] = directions[ i ].getVector();
        }

        return vectors;
    }

    /**
     * create a Z rotated {@link DirectionContainer}
     * @param rotAngle rotation angle around Z axis
     * @return Z rotated {@link DirectionContainer} copy
     */
    public DirectionContainer getZRotated( float rotAngle )
    {
        final int nDir = getNbDirections();
        DirectionContainer res = new DirectionContainer( nDir );

        for ( int d = nDir - 1; d >= 0; d-- )
        {
            Vector3f dir = getDirectionVectors()[ d ];
            Transformations.rotateAroundZ( dir, -rotAngle );

            Direction direction = new Direction( dir );
            res.add( direction, d );
        }

        return res;
    }

    // Iterable<Direction> interface

    @Override
    public Iterator<Direction> iterator()
    {
        return Arrays.stream( directions ).iterator();
    }
}
