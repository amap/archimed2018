package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.vecmath.Vector3f;

import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.scene.ArchimedScene;

/**
 * pixel table list with projection management/file information
 * 
 * @author François Grand
 * 
 *         2018-09-14
 */
public class PixelTableCache implements Iterable<IPixelTable>
{
    /**
     * Map<Direction,chemin vers Pixel Table>, utilisé avec option SAVE_TO_DISK
     */
    private Map<Vector3f, String> pixelTableFiles;

    /**
     * Map<Direction,pixelTable>, utilisé avec option KEEP_IN_MEMORY
     */
    private Map<Vector3f, IPixelTable> pixelTables;

    /**
     * handle only upper hits
     * 
     * @see Mir.Mode
     */
    private final boolean upperHit;

    /**
     * si true cf Option 2 de PixelTable.
     */
    private boolean reducedTable;

    /**
     * true if saveOnDisk
     */
    private boolean saveToDisk;

    /**
     * if saveOnDisk, path to directory where pixel tables are saved
     */
    private String pixelTablesSaveFilePath;

    private MainParameters mainParameters;

    private ArchimedScene scene;

    public PixelTableCache( MainParameters prms, ArchimedScene scene )
    {
        mainParameters = prms;
        this.scene = scene;

        reducedTable = mainParameters.getLightInterceptionParameters().isReducedTable();

        upperHit = mainParameters.isUpperHit();

        pixelTables = new LinkedHashMap<>();
        pixelTableFiles = new HashMap<>();

        saveToDisk = mainParameters.getLightInterceptionParameters().getProjectionPolicy() == ProjectionPolicy.SAVE_TO_DISK;
        String outputDir = mainParameters.getOutputDirectory( scene.getPlotBox() );
        if ( saveToDisk && outputDir != null )
        {
            File repo = new File( (new File( outputDir )).getParent() + "/pixelTables" );
            if ( !repo.exists() )
                repo.mkdirs();
            pixelTablesSaveFilePath = repo.getAbsolutePath();
        }
    }

    /**
     * determine if a pixel table for a given direction is stored in map
     * @param direction pixel table direction
     * @return true if present in map
     */
    public boolean hasPixelTable( Vector3f direction )
    {
        return pixelTables.containsKey( direction );
    }

    /**
    * search for a pixel table in Map
    * 
    * @param direction search key
    * @param inputPath path to file to read in case pixel table is not in the cache
    * @return found pixel table
    * @throws IOException
    */
    public IPixelTable getPixelTable( Vector3f direction ) throws IOException
    {
        // search in map
        IPixelTable res = pixelTables.get( direction );
        if ( !saveToDisk )
            return res;

        // read with file path cache
        if ( pixelTableFiles.containsKey( direction ) )
            return readAndStoreProjectionFile( direction, pixelTableFiles.get( direction ) );

        // compute projection file path
        String filePath = makeFilePath( direction );

        // read with given path
        return readAndStoreProjectionFile( direction, filePath );
    }

    public void putPixelTable( Vector3f direction, IPixelTable pixelTable ) throws IOException
    {
        if ( saveToDisk )
        {
            String filePath = makeFilePath( direction );
            pixelTableFiles.put( direction, filePath );
            pixelTable.writeProjectionFile( filePath );
        }
        else
            pixelTables.put( direction, pixelTable );
    }

    /**
     * read a projection file and store pixel table to map as well as file path
     * 
     * @param direction
     * @param filePath
     * @return
     * @throws IOException
     */
    private IPixelTable readAndStoreProjectionFile( Vector3f direction, String filePath ) throws IOException
    {
        IPixelTable res;
        if ( reducedTable )
        {
            if ( upperHit )
                throw new IOException( "invalid parameter combination reducedTable=true, upperHit=true" );

            res = PixelTableReduced.readProjectionFile( filePath, scene );
        }
        else
        {
            if ( upperHit )
                res = PixelTableUpperhit.readProjectionFile( filePath, scene );
            else
                res = PixelTable.readProjectionFile( filePath, scene );
        }

        if ( saveToDisk )
            pixelTableFiles.put( direction, filePath );
        else
            pixelTables.put( direction, res );
        return res;
    }

    /**
     * compute path to pixel table file
     * 
     * @param direction direction for the pixel table
     * @throws IOException 
     */
    private String makeFilePath( Vector3f direction ) throws IOException
    {
        String dirPath;
        if ( pixelTablesSaveFilePath == null )
            dirPath = File.createTempFile( "archimed_mir_", "" ).getAbsolutePath();
        else
            dirPath = pixelTablesSaveFilePath;

        String str = direction.toString().replaceAll( "\\.", "," );
        return reducedTable ? dirPath + "/r_direction_" + str : dirPath + "/direction_" + str;
    }

    // Iterable interface

    @Override
    public Iterator<IPixelTable> iterator()
    {
        return pixelTables.values().iterator();
    }
}
