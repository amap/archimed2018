package fr.amap.archimed.application.process.lightscattering;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

public class LightScatteringProcess extends Process
{
    public LightScatteringProcess()
    {
        super( ProcessCategory.LightScattering );
    }

    @Override
    public void execute()
    {
    }
}
