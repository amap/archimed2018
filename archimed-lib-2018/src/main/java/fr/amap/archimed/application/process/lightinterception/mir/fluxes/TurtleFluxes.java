package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Transformations;
import fr.amap.archimed.meteo.micrometeo.Sky;
import fr.amap.archimed.meteo.micrometeo.Sun;
import fr.amap.archimed.meteo.mmr.Meteo;
import fr.amap.archimed.model.diffusefraction.DeJong;
import fr.amap.archimed.time.Time;
import fr.amap.archimed.util.unit.UnitConverter;

/**
 * radiation fluxes associated to a "turtle" sky sector division
 * 
 * @author François Grand 2018-10-25
 */
public class TurtleFluxes extends RadiativeFluxes
{
    public TurtleFluxes( Turtle turtle )
    {
        super( turtle );
    }

    /**
     * <p>
     * Creates a RadiativeFluxes object based on a set of directions.</p>
     * If the direct radiation is null, incident radiation is on n directions.
     * RadiationComponents is integrated over the small period startTime to
     * endTime.
     *
     * This method corresponds with globalIntegrateFromDirections() but time
     * integration is outside
     *
     * @param latitude in radians
     * @param clearness
     * @param sceneRotation
     * @param startTime
     * @param endTime
     * @param timeStep
     * @param listener
     * @param allInTurtle
     * @param skySectors
     * @return
     */
    public RadiativeFluxes createFromDirections( double latitude, float clearness, double sceneRotation, Time startTime, Time endTime, float timeStep /*, Listener listener, boolean allInTurtle, int skySectors*/ )
    {
        Turtle turtle = (Turtle)directionContainer;

        double startDecimalHour = startTime.getDecimalHour();
        double endDecimalHour = endTime.getDecimalHour();

        Sun sun = turtle.getSun();
        RadiativeFluxes irCumul = new RadiativeFluxes( directionContainer );
        irCumul.setDirectBeam( sun.getPosition().getDirection() );

        int startDoy, endDoy;
        float totalDuration = 0; // total duration of received energy (in decimal hour)

        for ( int year = startTime.getYear(); year <= endTime.getYear(); year++ )
        {
            if ( year == startTime.getYear() )
                startDoy = startTime.getDoy();
            else
                startDoy = 0;

            if ( year == endTime.getYear() )
                endDoy = endTime.getDoy();
            else
                endDoy = 365;

            for ( int doy = startDoy; doy <= endDoy; doy++ )
            {
                sun = new Sun( latitude, doy );

                Sun.RiseSetHours sunriseSunset = sun.getRiseAndSetHours();

                if ( doy == startDoy && startTime.getDecimalHour() > sunriseSunset.rise )
                    startDecimalHour = startTime.getDecimalHour();
                else
                    startDecimalHour = sunriseSunset.rise;

                if ( doy == endDoy && endTime.getDecimalHour() < sunriseSunset.set )
                    endDecimalHour = endTime.getDecimalHour();
                else
                    endDecimalHour = sunriseSunset.set;

                for ( double h = startDecimalHour; h < endDecimalHour; h += timeStep )
                {
                    float duration = (float)Math.min( timeStep, endDecimalHour - h );

                    sun.setPosition( h );
                    Time time = new Time( year, doy, h );

                    float globalMJ = clearness * Meteo.computeExtraTerrestrialHourly( latitude, time.getDoy(), h, h + duration );

                    if ( globalMJ > 0 && duration > 0 )
                    {
                        // compute global radiation

                        float global = UnitConverter.megaJoulesToWatts( globalMJ, duration );
                        RadiationComponents globalRadiation = new RadiationComponents( global, 0.0f, 0.0f );

                        // compute direct and diffuse radiation from global

                        RadiationComponents totalRadiation = Meteo.partitionGlobalHourly( new DeJong( clearness, sun.getPosition().getElevation() ), globalRadiation, sun.getPosition().getElevation() );

                        // step radiation in all directions (W/m2)
                        RadiativeFluxes irStep = new RadiativeFluxes( directionContainer, totalRadiation );

                        // compute global radiation for each direction

                        if ( sceneRotation == 0 )
                            setRadiationComponentsToSectors( irStep, sun );
                        else
                            setRadiationComponentsToSectorsRotated( irStep, sun, sceneRotation );

                        // cumulate this step radiative fluxes components over the duration

                        irCumul.globalCumulateMJ( irStep, duration );
                        totalDuration += duration;
                    }
                }
            }
        }

        irCumul.scale( 1000000f / (totalDuration * 3600f) );
        return irCumul;
    }

    /**
     * Computes directional global fluxes in turtle sectors
     *
     * @param ir with components in Watts per second
     * @param sun
     * @param directions
     */
    private void setRadiationComponentsToSectors( RadiativeFluxes ir, Sun sun )
    {
        float[] turtleDirect;

        final int dirCount = directionContainer.getNbDirections();
        float[] turtleDiffuse = new float[ dirCount ];

        float irGlobal = ir.getTotalRadiation().getGlobal();
        if ( irGlobal > 0 )
        {
            float irDiffuse = ir.getTotalRadiation().getDiffuse();
            float irDirect = ir.getTotalRadiation().getDirect();

            float directDir = (float)(irDirect / Math.sin( sun.getPosition().getZenith() ));

            turtleDirect = Turtle.directInTurtle( directDir, sun.getPosition().getDirection(), directionContainer );

            float totalDirect = 0;
            float totalDiffuse = 0;

            for ( int d = dirCount - 1; d >= 0; d-- )
            {
                turtleDiffuse[ d ] = Sky.brightnessNorm( irDiffuse, irGlobal, directionContainer.get( d ), new Direction( sun.getPosition().getDirection() ) );

                double coeff = Math.cos( directionContainer.get( d ).getZenith() );

                // convert to flux as measured on horizontal plane
                turtleDirect[ d ] *= coeff;
                turtleDiffuse[ d ] *= coeff;
                totalDirect += turtleDirect[ d ];
                totalDiffuse += turtleDiffuse[ d ];
            }

            for ( int d = dirCount - 1; d >= 0; d-- )
            {
                float turtleDiff = turtleDiffuse[ d ] * irDiffuse / totalDiffuse;
                float turtleDir = turtleDirect[ d ];
                if ( totalDirect > 0 )
                    turtleDir *= irDirect / totalDirect;

                ir.setFluxRadiation( d, turtleDiff + turtleDir, turtleDiff, turtleDir );
            }
        }
    }

    /**
     * Computes directional global fluxes in turtle sectors with rotation in order to get directions of radiative fluxes in plot local coordinates
     *
     * @param ir with components in Watts per second
     * @param sun
     * @param directions
     * @param rotAngle
     */
    private void setRadiationComponentsToSectorsRotated( RadiativeFluxes ir, Sun sun, double rotAngle )
    {
        float[] turtleDirect;

        final int dirCount = directionContainer.getNbDirections();
        float[] turtleDiffuse = new float[ dirCount ];

        float irGlobal = ir.getTotalRadiation().getGlobal();
        if ( irGlobal > 0 )
        {
            float irDiffuse = ir.getTotalRadiation().getDiffuse();
            float irDirect = ir.getTotalRadiation().getDirect();

            Vector3f sunVectorRot = sun.getPosition().getDirection();
            Transformations.rotateAroundZ( sunVectorRot, -1 * (float)rotAngle );

            Direction sunDirectionRot = new Direction( sunVectorRot );

            DirectionContainer rotatedDirections = directionContainer.getZRotated( (float)rotAngle );

            float directDir = (float)(irDirect / Math.sin( sun.getPosition().getZenith() )); //!!!! A REVOIR SI TERRAIN EN PENTE

            turtleDirect = Turtle.directInTurtle( directDir, sunDirectionRot.getVector(), rotatedDirections );

            float totalDirect = 0;
            float totalDiffuse = 0;

            for ( int d = dirCount - 1; d >= 0; d-- )
            {
                turtleDiffuse[ d ] = Sky.brightnessNorm( irDiffuse, irGlobal, rotatedDirections.get( d ), sunDirectionRot );

                double coeff = Math.cos( rotatedDirections.get( d ).getZenith() );

                // convert to flux as measured on horizontal plane
                turtleDirect[ d ] *= coeff;
                turtleDiffuse[ d ] *= coeff;
                totalDirect += turtleDirect[ d ];
                totalDiffuse += turtleDiffuse[ d ];
            }

            for ( int d = dirCount - 1; d >= 0; d-- )
            {
                float turtleDiff = turtleDiffuse[ d ] * irDiffuse / totalDiffuse;
                float turtleDir = turtleDirect[ d ];
                if ( totalDirect > 0 )
                    turtleDir *= irDirect / totalDirect;

                ir.setFluxRadiation( d, turtleDiff + turtleDir, turtleDiff, turtleDir );
            }
        }
    }
}
