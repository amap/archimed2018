package fr.amap.archimed.application.process.lightinterception.mir;

import fr.amap.archimed.scene.ASItem;

/**
 * information linked to nodes by Mir model
 * 
 * @author grand 2018-10-23
 */
public class MirNodeInfo
{
    /**
     * name given to node MirNodeInfo attribute
     * @see ASNode.getAttributes()
     */
    public static final String MIR_INFO = "MIRinfo";

    public int tmpNbHits;

    public int nbHits;

    /**
     * Projected mesh (real) area to projected area in pixel ratio, for one direction 
     */
    public float stepRatioArea;

    /**
     * intercepted radiation (W) for all spectrum, for one simulation step, for all directions
     * (was tmpIntercepted)
     */
    public double stepIntercepted;

    /**
     * dans le visible pour un pas de temps
     */
    public double tmpPARinterceptedLight;

    /**
     * dans le proche-infrarouge pour un pas de temps
     */
    public double tmpNIRinterceptedLight;

    /**
     * for each direction, real visible area of the node including occlusion by other nodes
     * unit = area of a pixel
     */
    public double[] visibleAreaByDirection;

    /**
     * surface en 3D du mesh.
     */
    private double meshArea;

    /**
     * portion de ciel vu par le mesh (angle solide).
     */
    public double skyFraction;

    /**
     * irradiance (W/m²) = stepIntercepted/node area, for all spectrum, for one step, for all directions
     * (was tmpIrradiation)
     */
    public double stepIrradiance;

    /**
     * @param directionCount number of sky sectors (ie. not including sun)
     */
    public MirNodeInfo( int directionCount, ASItem item )
    {
        visibleAreaByDirection = new double[ directionCount ];
        meshArea = item.getMesh().getArea();
    }

    public double getMeshArea()
    {
        return meshArea;
    }
}
