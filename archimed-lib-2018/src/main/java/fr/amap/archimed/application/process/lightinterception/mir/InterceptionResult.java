/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.application.process.lightinterception.mir;

/**
 * Contains the intercepted light results, intercepted and passed through components.
 * The "passed through" is not transmission; rather not intercepted light due to geometry (eg. porous or turbid object)
 * 
 * @author Julien Heurtebize, F.Grand (refactoring, 2018-10-24)
 */
public class InterceptionResult
{
    /**
     * The intercepted light, unit=area of a pixel. 
     * Was "interceptedLight" before, unit=MJ.
     */
    private double interceptionArea;

    /**
     * The passing through light, unit=area of a pixel. 
     * Was "passingThroughLight" before, unit=MJ.
     */
    private double passingThroughArea;

    public InterceptionResult( double interceptedLight, double passingThroughArea )
    {
        this.interceptionArea = interceptedLight;
        this.passingThroughArea = passingThroughArea;
    }

    public double getInterceptionArea()
    {
        return interceptionArea;
    }

    public double getPassingThroughArea()
    {
        return passingThroughArea;
    }
}
