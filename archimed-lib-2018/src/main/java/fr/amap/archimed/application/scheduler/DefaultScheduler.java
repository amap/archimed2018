package fr.amap.archimed.application.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.Application;
import fr.amap.archimed.application.process.Process;
import fr.amap.archimed.application.process.energybalance.EnergyBalanceProcess;
import fr.amap.archimed.application.process.lightinterception.mir.MirProcess;
import fr.amap.archimed.application.process.lightscattering.LightScatteringProcess;
import fr.amap.archimed.application.process.outputfile.NodeValuesCSVOutputProcess;
import fr.amap.archimed.application.process.photosynthesis.PhotosynthesisProcess;
import fr.amap.archimed.application.process.stomatalconductance.StomatalConductanceProcess;
import fr.amap.archimed.meteo.MeteoData;
import fr.amap.archimed.parameters.input.MainParameters;
import fr.amap.archimed.time.TimePeriod;

/**
 * default process scheduler
 * sequence of optical properties, light interception, light scattering (optional), photosynthesis (optional), energy balance (optional)
 * 
 * @author François Grand
 * 2018-10-02
 */
public class DefaultScheduler extends Scheduler
{
    private final static Logger logger = LogManager.getLogger();

    private Application application;

    public DefaultScheduler( Application app )
    {
        application = app;
    }

    @Override
    public void parseProcesses() throws Exception
    {
        MainParameters prms = application.getMainParameters();

        // light interception

        addProcess( new MirProcess( application ) );

        // light scattering

        if ( prms.getLightInterceptionParameters().isScattering() )
            addProcess( new LightScatteringProcess() );

        // photosynthesis

        if ( prms.isComputePhotosynthesis() )
        {
            addProcess( new StomatalConductanceProcess() );
            addProcess( new PhotosynthesisProcess() );
        }

        // energy balance

        if ( prms.isComputeEnergyBalance() )
            addProcess( new EnergyBalanceProcess() );

        // CSV file output

        if ( prms.getOutputParameters().isExportNodesValues() )
            addProcess( new NodeValuesCSVOutputProcess( prms, application.getScene() ) );
    }

    @Override
    public void execute() throws Exception
    {
        MeteoData meteo = application.getMeteo();
        int currentStep = 0;
        for ( TimePeriod period : meteo.getMeteoData().keySet() )
        {
            logger.info( "Timestep : " + period.getStart() + " -> " + period.getEnd().toString() + "\t" + (currentStep + 1) );

            for ( Process proc : processes )
            {
                if ( proc instanceof MirProcess )
                    ((MirProcess)proc).setTimePeriod( period );
                else if ( proc instanceof NodeValuesCSVOutputProcess )
                    ((NodeValuesCSVOutputProcess)proc).setTimePeriod( currentStep );
                proc.execute();
            }
        }
    }
}
