package fr.amap.archimed.application.process.lightinterception.mir.pixeltable;

public enum ProjectionPolicy
{
    /**
     * Compute the projection for a given direction for each call.
     */
    COMPUTE_EVERY_TIME,

    /**
     * Save the projections in memory. It is useful when a call to
     * {@link #execute(javax.vecmath.Vector3f, double) } is performed with the same
     * direction multiple times. This should be use if the environment execution
     * system can handle big data in memory.
     */
    KEEP_IN_MEMORY,

    /**
     * Save the projections inside the temp system folder. It is useful when a call
     * to {@link #execute(javax.vecmath.Vector3f, double) } is performed with the
     * same direction multiple times.
     */
    SAVE_TO_DISK;
}