package fr.amap.archimed.application.process.stomatalconductance;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.Process;

public class StomatalConductanceProcess extends Process
{
    public StomatalConductanceProcess()
    {
        super( ProcessCategory.StomatalConductance );
    }

    @Override
    public void execute()
    {
    }
}
