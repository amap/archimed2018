package fr.amap.archimed.application.process.lightinterception.mir.fluxes;

/**
 * number of directions for sky division in sectors (turtle)
 * 
 * @author François Grand 2018-10-25
 */
public enum DirectionCount
{
    _1( 1, 0 ),

    _6( 6, 1 ),

    _16( 16, 2 ),

    _46( 46, 3 ),

    _136( 136, 4 ),

    _406( 406, 5 );

    /**
     * number of directions
     */
    private int count;

    /**
     * order of the direction number (0 for 1, 1 for 6, 2 for 16, ...)
     */
    private int order;

    private DirectionCount( int count, int order )
    {
        this.count = count;
        this.order = order;
    }

    @Override
    public String toString()
    {
        return String.valueOf( count );
    }

    public int getDirectionCount()
    {
        return count;
    }

    public int getDirectionOrder()
    {
        return order;
    }

    public static DirectionCount getEnumByInt( int count ) throws Exception
    {
        for ( DirectionCount e : DirectionCount.values() )
            if ( count == e.getDirectionCount() )
                return e;

        throw new Exception( "invalid count " + count + " for DirectionCount" );
    }
}
