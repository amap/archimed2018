package fr.amap.archimed.application;

/**
 * list of process (model abstraction) IDs
 * 
 * @author François Grand
 * 2018-09-25
 */
public enum ProcessCategory
{
    /**
     * optical properties (reflectance, transmittance, ...) model
     */
    OpticalProperties,

    /**
     * model of light interaction with objects having a 3D representation (intercepted irradiance)
     */
    LightInterception,

    /**
     * model computing light redistribution
     */
    LightScattering,

    /**
     * photosynthesis model
     */
    Photosynthesis,

    /**
     * stomatal conductance model
     */
    StomatalConductance,

    /**
     * energy balance (temperature) model
     */
    EnergyBalance,

    /**
     * output to file 
     */
    FileOutput
}
