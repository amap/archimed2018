package fr.amap.archimed.model.lightinterception;

import fr.amap.archimed.application.process.lightinterception.mir.InterceptionResult;
import fr.amap.archimed.model.AbstractModel;

/**
 * An interface for the interception models in MMR.
 * This model category is a "local" model, ie. applied to a smaller scale than scene, eg. on a leaf
 *
 * @author J. Dauzat - May 2012, Julien Heurtebize (refactoring)
 */
public interface InterceptionModel extends AbstractModel
{
    public InterceptionResult interception( double light_MJ );
}
