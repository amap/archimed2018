package fr.amap.archimed.model;

import fr.amap.archimed.parameters.ModelKey;

/**
 * implementation of a Process.
 * Ex : NRH, Farquhar, BWBL, MIR, MUSC 
 * 
 * @author François Grand
 * 2018-09-28
 */
public abstract class Model implements AbstractModel
{
    /**
     * determine if model can handle nodes with given key (functional group name, process category, functional type) 
     */
    public abstract boolean matchesKey( ModelKey key );

    /**
     * parse parameters in configuration file (CSV "model manager")
     * @param key context in which configuration is to be parsed (models can be used for multiple contexts)
     * @param conf configuration string read from file 
     * @return true if configuration string was parsed successfully
     * @throws Exception 
     */
    public abstract boolean parseConfig( ModelKey key, String conf );
}
