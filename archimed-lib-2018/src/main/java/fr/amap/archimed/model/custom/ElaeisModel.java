package fr.amap.archimed.model.custom;

import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.photosynthesis.PhotosynthesisModel;
import fr.amap.archimed.model.stomatalconductance.StomatalConductanceModel;
import fr.amap.archimed.parameters.ModelKey;

/**
 * example of a "modeler model", ie. custom model(s) implementation
 * 
 * @author François Grand
 * 2018-09-25
 */
public class ElaeisModel extends Model implements StomatalConductanceModel, PhotosynthesisModel
{
    // Model abstract methods

    @Override
    public boolean matchesKey( ModelKey key )
    {
        switch ( key.getProcessCategory() )
        {
            case Photosynthesis:
            case StomatalConductance:
                return true;

            default:
                return false;
        }
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        throw new Error( "not implemented" );
    }

    // StomatalConductance interface

    @Override
    public double computeStomatalConductance()
    {
        return 0;
    }

    // Photosynthesis interface

    @Override
    public double computeAssimilationRate() throws Exception
    {
        //        Farquhar f = new Farquhar_C3Iterative();
        //        f.setParameters( this );
        //        f.execute();
        //        return f.computeAssimilationRate();
        throw new Error( "not implemented" );
    }
}
