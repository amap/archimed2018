package fr.amap.archimed.model.photosynthesis.farquhar;

import fr.amap.archimed.parameters.ModelKey;

public class Farquhar_C3Iterative extends FarquharModel
{
    @Override
    public double computeAssimilationRate()
    {
        throw new Error( "not implemented" );
    }

    @Override
    public boolean matchesKey( ModelKey key )
    {
        throw new Error( "not implemented" );
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        throw new Error( "not implemented" );
    }
}
