/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.model.diffusefraction;

import fr.amap.archimed.model.AbstractModel;

/**
 *
 * @author Jean Dauzat, J. Heurtebize (refactoring)
 */
public interface DiffuseFractionModel extends AbstractModel
{
    /**
     * @return the diffuse fraction kd (the portion of diffuse radiation)
     */
    public float getKd();
}
