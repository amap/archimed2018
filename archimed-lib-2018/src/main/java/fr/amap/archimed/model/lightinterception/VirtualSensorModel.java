package fr.amap.archimed.model.lightinterception;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.lightinterception.mir.InterceptionResult;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.parameters.ModelKey;

/**
 * A virtual sensor radiative model in MMR.
 *
 * @author J. Dauzat - May 2012, Julien Heurtebize (refactoring), F. Grand (refactoring)
 */
public class VirtualSensorModel extends Model implements SurfacicInterceptionModel
{
    @Override
    public InterceptionResult interception( double light_MJ )
    {
        return new InterceptionResult( light_MJ, light_MJ );
    }

    @Override
    public double getTransparency()
    {
        return 1;
    }

    @Override
    public boolean matchesKey( ModelKey key )
    {
        return key.getProcessCategory() == ProcessCategory.LightInterception;
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        // check that the configuration string (in the 'Interception model' column of the CSV model manager file) matches 
        return conf.equals( "VirtualSensor" );
    }
}
