package fr.amap.archimed.model.stomatalconductance;

/**
 * stomatal conductance models interface
 * 
 * @author François Grand
 * 2018-09-25
 */
public interface StomatalConductanceModel
{
    /**
     * compute stomatal conductance (mmol.m-2.s-1)
     */
    public double computeStomatalConductance();

    /*
     * comparaison multiplicate-BWB :
     * https://www.fs.fed.us/rm/pubs_other/rmrs_2007_buker_p001.pdf
     * 
     * http://cdn.intechweb.org/pdfs/26114.pdf
     * 
     * BWB 1987 :
     * https://www.researchgate.net/publication/201995960_A_Model_Predicting_Stomatal_Conductance_and_Its_Contribution_to_the_Control_of_Photosynthesis_Under_Different_Environmental_Conditions
     * 
     * gs   stomatal conductance (mmol.m-2.s-1)
     * VPD  vapor pressure deficit
     * PPFD photosynthetic photon flux density
     * SWP  soil water potential
     * An   net photosynthetic rate, CO2 assimilation rate g.m-2.s-1  g = masse de CO2 transformé
     * 
     * BWB : gs = f( An, CO2, relative humidity )
     * CO2, relative humidity : at leaf surface
     */
}
