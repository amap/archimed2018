package fr.amap.archimed.model.photosynthesis.nrh;

import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.photosynthesis.PhotosynthesisModel;
import fr.amap.archimed.parameters.ModelKey;

/**
 * phtosynthesis model
 * 
 * @author François Grand
 * 2018-09-25
 */
public class NRHModel extends Model implements PhotosynthesisModel
{
    @Override
    public double computeAssimilationRate()
    {
        throw new Error( "not implemented" );
    }

    @Override
    public boolean matchesKey( ModelKey key )
    {
        throw new Error( "not implemented" );
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        throw new Error( "not implemented" );
    }
}
