package fr.amap.archimed.model.lightinterception;

/**
 * An interface for the Surfacic interception models in MMR.
 *
 * @author J. Dauzat - May 2012, Julien Heurtebize (refactoring)
 */
public interface SurfacicInterceptionModel extends InterceptionModel
{
    public double getTransparency();
}
