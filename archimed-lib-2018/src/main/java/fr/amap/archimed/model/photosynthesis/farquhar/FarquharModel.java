package fr.amap.archimed.model.photosynthesis.farquhar;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.photosynthesis.PhotosynthesisModel;
import fr.amap.archimed.parameters.ModelKey;

/**
 * Farquhar template
 * 
 * @author grand
 * 2018-09-25
 */
public abstract class FarquharModel extends Model implements PhotosynthesisModel
{
    private double vcMax;

    @Override
    public boolean matchesKey( ModelKey key )
    {
        // default implementation, uses "Leaf"/"Leaflet" types
        return key.getProcessCategory() == ProcessCategory.Photosynthesis && (key.getNodeType().getName().equals( "Leaf" ) || key.getNodeType().getName().equals( "Leaflet" ));
    }

    //    @Override
    //    public void setParameters( ParameterProvider pp )
    //    {
    //        super.setParameters( pp );
    //
    //        ModelParameters params = parameterProvider.getParameters( modelKey );
    //        vcMax = params.getEntry( "vcMax" ).getValue();
    //    }
}
