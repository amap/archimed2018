/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.model.opticalproperties;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.parameters.ModelKey;

/**
 * optical properties (scattering factor, ...) computation model
 * @author François Grand
 * 2018-10-17
 */
public class OpticalPropertiesModel extends Model
{
    private final static Logger logger = LogManager.getLogger();

    /**
     * scattering factor in visible spectrum
     */
    private double PARscatteringFactor;

    /**
     * scattering factor in near infrared spectrum
     */
    private double NIRscatteringFactor;

    @Override
    public boolean matchesKey( ModelKey key )
    {
        return key.getProcessCategory() == ProcessCategory.OpticalProperties;
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        // parameter format :  <scattering factor in visible spectrum>, <scattering factor in near infrared spectrum>

        Pattern p = Pattern.compile( "([^\\,]+),([^\\,]+)" );
        Matcher m = p.matcher( conf );
        if ( !m.matches() )
        {
            logger.error( "OpticalProperties model : invalid parameter '" + conf + "'" );
            return false;
        }

        try
        {
            PARscatteringFactor = Double.valueOf( m.group( 1 ) );
            NIRscatteringFactor = Double.valueOf( m.group( 2 ) );
            return true;
        }
        catch( NumberFormatException e )
        {
            logger.error( "OpticalProperties model : invalid parameter '" + conf + "' : " + e.getMessage() );
            return false;
        }
    }

    public double getPARscatteringFactor()
    {
        return PARscatteringFactor;
    }

    public double getNIRscatteringFactor()
    {
        return NIRscatteringFactor;
    }
}
