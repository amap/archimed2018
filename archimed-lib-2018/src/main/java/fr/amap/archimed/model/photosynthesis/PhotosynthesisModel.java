package fr.amap.archimed.model.photosynthesis;

/**
 * photosynthesis models interface
 * 
 * @author François Grand
 * 2018-09-25
 */
public interface PhotosynthesisModel
{
    /**
     * compute assimilation rate
     * @throws Exception 
     */
    public double computeAssimilationRate() throws Exception;
}
