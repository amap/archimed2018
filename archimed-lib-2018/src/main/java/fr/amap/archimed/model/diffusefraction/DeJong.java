/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.model.diffusefraction;

/**
 *
 * @author Julien Heurtebize
 */
public class DeJong implements DiffuseFractionModel
{
    private final boolean hourly;

    private final double sunElevation;

    private final float kt;

    /**
     * For daily estimation.
     * @param kt 
     */
    public DeJong( float kt )
    {
        this.kt = kt;
        sunElevation = -1;
        this.hourly = false;
    }

    /**
     * For hourly estimation
     * @param kt
     * @param sunElevation 
     */
    public DeJong( float kt, double sunElevation )
    {
        this.kt = kt;
        this.sunElevation = sunElevation;
        this.hourly = true;
    }

    /**
     * Get the ratio (diffuse:global) under clear sky conditions (from de Jong
     * 1980, cited by Spitters et al., 1986)
     *
     * @param sunElevation (radians)
     * @return R= diffuse / global
     */
    public static float getDiffuseInGlobalHourlyClear( double sunElevation )
    {
        double sinSunEl = Math.sin( sunElevation );
        float R = (float)(0.847 - (1.61 * sinSunEl) + (1.04 * sinSunEl * sinSunEl));
        return R;
    }

    @Override
    public float getKd()
    {
        if ( hourly )
        {
            if ( kt <= 0.22 )
                return 1.0f;
            else if ( kt <= 0.35 )
                return (float)(1. - (6.4 * (kt - 0.22) * (kt - 0.22)));
            else
            {
                float R = getDiffuseInGlobalHourlyClear( sunElevation );
                float K = (float)((1.47 - R) / 1.66);

                if ( kt <= K )
                    return (float)(1.47 - (1.66 * kt));

                return R;
            }
        }

        if ( kt < 0.07 )
            return 1.0f;
        else if ( kt < 0.35 )
            return (1 - 2.3f * (kt - 0.07f));
        else if ( kt < 0.75 )
            return (1.33f - 1.46f * kt);

        return 0.23f;
    }
}
