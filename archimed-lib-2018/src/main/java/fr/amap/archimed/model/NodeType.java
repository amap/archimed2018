package fr.amap.archimed.model;

/**
 * functional type
 * 
 * @author François Grand
 * 2018-09-28
 */
public class NodeType
{
    private String name;

    public NodeType( String name )
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    // Object methods

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        NodeType other = (NodeType)obj;
        if ( name == null )
        {
            if ( other.name != null )
                return false;
        }
        else if ( !name.equals( other.name ) )
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
