package fr.amap.archimed.model.lightinterception;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.application.process.lightinterception.mir.InterceptionResult;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.parameters.ModelKey;

/**
 * A translucent radiative model in MMR.
 *
 * @author J. Dauzat - May 2012, Julien Heurtebize (refactoring) - Oct 2018, François Grand
 */
public class TranslucentModel extends Model implements SurfacicInterceptionModel
{
    private final static Logger logger = LogManager.getLogger();

    /**
     * surface transparency
     */
    private double transparency;

    @Override
    public boolean matchesKey( ModelKey key )
    {
        return key.getProcessCategory() == ProcessCategory.LightInterception;
    }

    @Override
    public boolean parseConfig( ModelKey key, String conf )
    {
        // parameter format : "Translucent( <transparency> )"

        Pattern p = Pattern.compile( "Translucent\\(([^\\)]+)\\)" );
        Matcher m = p.matcher( conf );
        if ( !m.matches() )
            return false;

        // transparency[0, 1], 0: opaque; 1: completely transparent
        try
        {
            transparency = Double.valueOf( m.group( 1 ) );
            return true;
        }
        catch( NumberFormatException e )
        {
            logger.error( "Translucent model : invalid parameter '" + conf + "' : " + e.getMessage() );
            return false;
        }
    }

    @Override
    public InterceptionResult interception( double light_MJ )
    {
        // radiation amount traversing node
        double passingThrough = light_MJ * transparency;

        // intercepted radiation amount
        double intercepted = light_MJ - passingThrough;

        return new InterceptionResult( intercepted, passingThrough );
    }

    @Override
    public double getTransparency()
    {
        return transparency;
    }
}
