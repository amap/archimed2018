package fr.amap.archimed.parameters;

import fr.amap.archimed.util.NamedDoubles;

/**
 * Parameters linked to a model
 * 
 * @author François Grand
 * 2018-10-10
 */
public class ModelParameters extends NamedDoubles
{
}
