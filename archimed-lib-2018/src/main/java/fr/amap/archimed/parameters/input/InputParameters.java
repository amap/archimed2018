package fr.amap.archimed.parameters.input;

import java.lang.reflect.Method;
import java.util.Properties;

/**
 * super class to parse input parameters from Java properties file
 * 
 * @author François Grand
 * 2018-10-11
 */
public abstract class InputParameters
{
    protected Properties properties;

    public InputParameters( Properties props ) throws Exception
    {
        properties = props;
        parse();
    }

    /**
     * set default values
     */
    //    protected abstract void defaults();

    /**
     * parse and set parameters
     * @throws Exception 
     */
    protected abstract void parse() throws Exception;

    /**
     * parse a boolean parameter
     * @param name parameter name
     * @return parsed value if present in properties
     * @throws Exception in case value is not present
     */
    protected boolean parseBoolean( String name ) throws Exception
    {
        String val = properties.getProperty( name );
        if ( val == null )
            throw new Exception( "missing " + name + " boolean parameter" );
        return Boolean.parseBoolean( val );
    }

    /**
     * parse a boolean parameter with default value
     * @param name parameter name
     * @param defaultValue default value
     * @return parsed value
     */
    protected boolean parseBoolean( String name, boolean defaultValue )
    {
        try
        {
            return parseBoolean( name );
        }
        catch( Exception e )
        {
            return defaultValue;
        }
    }

    /**
     * parse an integer parameter
     * @param name parameter name
     * @return parsed value if present in properties
     * @throws Exception in case value is not present
     */
    protected int parseInt( String name ) throws Exception
    {
        String val = properties.getProperty( name );
        if ( val == null )
            throw new Exception( "missing " + name + " integer parameter" );
        return Integer.parseInt( val );
    }

    /**
     * parse an string parameter
     * @param name parameter name
     * @return parsed value if present in properties
     * @throws Exception in case value is not present
     */
    protected String parseString( String name ) throws Exception
    {
        String val = properties.getProperty( name );
        if ( val == null )
            throw new Exception( "missing " + name + " string parameter" );
        return val;
    }

    /**
     * parse an enum parameter
     * @param name parameter name
     * @param enumClass enum class
     * @return parsed value if present in properties
     * @throws Exception in case value is not present
     */
    protected <T> T parseEnum( String name, Class<T> enumClass ) throws Exception
    {
        String val = properties.getProperty( name );
        if ( val == null )
            throw new Exception( "missing " + name + " enum parameter" );
        Method m = enumClass.getDeclaredMethod( "valueOf", String.class );
        Object v = m.invoke( null, val );
        return (T)v;
    }

    /**
     * parse an enum parameter with default value
     * @param name parameter name
     * @param enumClass enum class
     * @param defaultValue default value
     * @return parsed value
     */
    protected <T> T parseEnum( String name, Class<T> enumClass, T defaultValue )
    {
        try
        {
            return parseEnum( name, enumClass );
        }
        catch( Exception e )
        {
            return defaultValue;
        }
    }

    /**
     * parse a double parameter
     * @param name parameter name
     * @return parsed value if present in properties
     * @throws Exception in case value is not present
     */
    protected double parseDouble( String name ) throws Exception
    {
        String val = properties.getProperty( name );
        if ( val == null )
            throw new Exception( "missing " + name + " double parameter" );
        return Double.parseDouble( val );
    }

    /**
     * parse a double parameter with default value
     * @param name parameter name
     * @param defaultValue default value
     * @return parsed value
     */
    protected double parseDouble( String name, double defaultValue )
    {
        try
        {
            return parseDouble( name );
        }
        catch( Exception e )
        {
            return defaultValue;
        }
    }
}
