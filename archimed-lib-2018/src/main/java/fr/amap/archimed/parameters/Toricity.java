package fr.amap.archimed.parameters;

/**
 * toricity (scene repetition) option
 * 
 * @author François Grand
 * 2018-10-11
 */
public enum Toricity
{
    /**
     * no repetition
     */
    NON_TORIC_FINITE_BOX_TOPOLOGY,

    /**
     * repetition in all directions, to infinity
     */
    TORIC_INFINITE_BOX_TOPOLOGY,

    TORIC_FINITE_BOX_TOPOLOGY
}
