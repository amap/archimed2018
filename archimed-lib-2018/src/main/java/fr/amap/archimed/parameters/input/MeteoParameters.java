package fr.amap.archimed.parameters.input;

import java.util.Properties;

public class MeteoParameters extends InputParameters
{
    /**
     * scene latitude 
     */
    private double latitude;

    /**
     * scene altitude 
     */
    private double altitude;

    public MeteoParameters( Properties props ) throws Exception
    {
        super( props );
    }

    @Override
    protected void parse() throws Exception
    {
        latitude = parseDouble( "latitude" );
        if ( latitude < 0 || latitude > 90 )
            throw new Exception( "invalid latitude=" + latitude + " parameter" );

        altitude = parseDouble( "altitude" );
    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getAltitude()
    {
        return altitude;
    }
}
