package fr.amap.archimed.parameters.input;

import java.util.Properties;

import fr.amap.archimed.application.process.lightinterception.mir.fluxes.DirectionCount;
import fr.amap.archimed.application.process.lightinterception.mir.pixeltable.ProjectionPolicy;
import fr.amap.archimed.parameters.Toricity;

/**
 * parameters for light interception model (Mir/Musc)
 * currently, from Java properties file 
 * 
 * @author François Grand
 * 2018-10-11
 */
public class LightInterceptionParameters extends InputParameters
{
    /**
     * number of turtle sectors
     * must be either 1, 6, 16, 46, 136 or 406
     */
    private int skySectors;

    /**
     * if false, the direct radiation is accounted for separately
     * else the direct radiation is assigned to sky sectors
     */
    private boolean allInTurtle;

    /**
     * determines the frequency of computation of the sun's radiations within a period (in minutes)
     */
    private int radiationTimestep;

    /**
     * if "true", the radiative balance of the scene is supplemented multiple scattering
     */
    private boolean scattering;

    /**
     * determines if near infrared energy will be computed (scattering must be true)
     */
    private boolean computeNIR;

    /**
     * scene virtual repetition    
     */
    private Toricity toricity;

    /**
     * number of rays sent on the plot for each direction (gives a sampling density per m2 of -> plot area / Nb Pixels)
     */
    private int numberOfPixels;

    /**
     * if true, do not store the height of node during light interception process
     * (the program will be less RAM-heavy)
     */
    private boolean reducedTable;

    /**
     * number of cell for paving the ground (no paving if 0); Must have paving if scattering
     */
    private int plotPaving;

    private ProjectionPolicy projectionPolicy;

    public LightInterceptionParameters( Properties props ) throws Exception
    {
        super( props );
    }

    @Override
    protected void parse() throws Exception
    {
        skySectors = parseInt( "skySectors" );
        // check value
        DirectionCount.getEnumByInt( skySectors );

        allInTurtle = parseBoolean( "allInTurtle", true );

        radiationTimestep = parseInt( "radiationTimestep" );

        scattering = parseBoolean( "scattering", true );

        computeNIR = parseBoolean( "computeNIR", true );
        if ( computeNIR && !scattering )
            throw new Exception( "computeNIR parameter requires scattering" );

        toricity = parseEnum( "toricity", Toricity.class, Toricity.NON_TORIC_FINITE_BOX_TOPOLOGY );

        numberOfPixels = parseInt( "numberOfPixels" );
        if ( numberOfPixels < 1 )
            throw new Exception( "invalid numberOfPixels=" + numberOfPixels + " parameter" );

        reducedTable = parseBoolean( "reducedTable", true );

        plotPaving = parseInt( "plotPaving" );
        if ( plotPaving < 0 )
            throw new Exception( "invalid plotPaving=" + plotPaving + " parameter" );
        if ( scattering && plotPaving == 0 )
            throw new Exception( "invalid plotPaving=" + plotPaving + " parameter : must be > 0 with scattering" );

        boolean saveOnDisk = parseBoolean( "saveOnDisk", false );
        projectionPolicy = saveOnDisk ? ProjectionPolicy.SAVE_TO_DISK : ProjectionPolicy.KEEP_IN_MEMORY;
    }

    public int getSkySectors()
    {
        return skySectors;
    }

    public boolean isAllInTurtle()
    {
        return allInTurtle;
    }

    public int getRadiationTimestep()
    {
        return radiationTimestep;
    }

    public boolean isScattering()
    {
        return scattering;
    }

    public boolean isComputeNIR()
    {
        return computeNIR;
    }

    public Toricity getToricity()
    {
        return toricity;
    }

    public int getNumberOfPixels()
    {
        return numberOfPixels;
    }

    public boolean isReducedTable()
    {
        return reducedTable;
    }

    public int getPlotPaving()
    {
        return plotPaving;
    }

    public ProjectionPolicy getProjectionPolicy()
    {
        return projectionPolicy;
    }
}
