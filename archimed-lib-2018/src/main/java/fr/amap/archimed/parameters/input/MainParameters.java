package fr.amap.archimed.parameters.input;

import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.joda.time.LocalDateTime;

import fr.amap.archimed.scene.PlotBox;

/**
 * main application parameters
 * currently, a Java properties file 
 * 
 * @author François Grand
 * 2018-10-05
 */
public class MainParameters extends InputParameters
{
    private String scenePath;

    private String modelsManagerPath;

    private String meteoFilePath;

    /**
     * scene azimut
     */
    private double sceneRotation;

    /**
     * compute photosynthesis step
     */
    private boolean computePhotosynthesis;

    /**
     * compute energy balance
     */
    private boolean computeEnergyBalance;

    /**
     * light interception parameters
     */
    private LightInterceptionParameters lightInterceptionParameters;

    /**
     * meteo parameters
     */
    private MeteoParameters meteoParameters;

    /**
     * output parameters
     */
    private OutputParameters outputParameters;

    public MainParameters( Properties props ) throws Exception
    {
        super( props );
    }

    @Override
    protected void parse() throws Exception
    {
        scenePath = parseString( "file" );

        modelsManagerPath = parseString( "modelsManager" );

        meteoFilePath = parseString( "meteoFile" );

        sceneRotation = parseDouble( "sceneRotation", 0 );

        computePhotosynthesis = parseBoolean( "photosynthesis", false );

        computeEnergyBalance = parseBoolean( "energyBalance", false );
        if ( computeEnergyBalance && !computePhotosynthesis )
            throw new Exception( "invalid photosynthesis parameter : must be true with energyBalance" );

        lightInterceptionParameters = new LightInterceptionParameters( properties );

        meteoParameters = new MeteoParameters( properties );

        outputParameters = new OutputParameters( properties );

        if ( computePhotosynthesis && !outputParameters.isAtEachStep() )
            throw new Exception( "invalid atEachStep parameter : must be true with photosynthesis" );
    }

    public String getScenePath()
    {
        return scenePath;
    }

    public String getModelsManagerPath()
    {
        return modelsManagerPath;
    }

    public String getMeteoFilePath()
    {
        return meteoFilePath;
    }

    public double getSceneRotation()
    {
        return sceneRotation;
    }

    public boolean isComputePhotosynthesis()
    {
        return computePhotosynthesis;
    }

    public boolean isComputeEnergyBalance()
    {
        return computeEnergyBalance;
    }

    /**
     * @return true if keep only first intercepted node in pixel table
     */
    public boolean isUpperHit()
    {
        // essentially, it's the contrary of scattering (scattering requires to keep all node intersections along directions)
        return !lightInterceptionParameters.isScattering();
    }

    public LightInterceptionParameters getLightInterceptionParameters()
    {
        return lightInterceptionParameters;
    }

    public MeteoParameters getMeteoParameters()
    {
        return meteoParameters;
    }

    public OutputParameters getOutputParameters()
    {
        return outputParameters;
    }

    /**
     * compute output directory path from time step
     * format : <base output dir>/<OPS file name> (<plot box Xmin>;<plot box Xmax>) (<plot box Ymin>;<plot box Ymax>)/<year>.<month>.<day> <hour> <minute> <second>
     * @return
     */
    public String getOutputDirectory( PlotBox plotBox )
    {
        // base output directory
        StringBuilder sb = new StringBuilder( outputParameters.getOutputDirectory() );
        sb.append( '/' );

        // scene name from scene path
        String OPSname = FilenameUtils.getBaseName( scenePath );
        sb.append( OPSname );

        // add plotbox coordinates
        sb.append( " (" );
        sb.append( plotBox.getInfCorner().x );
        sb.append( ';' );
        sb.append( plotBox.getSupCorner().x );
        sb.append( ") (" );
        sb.append( plotBox.getInfCorner().y );
        sb.append( ';' );
        sb.append( plotBox.getSupCorner().y );
        sb.append( ")/" );

        // current time stamp
        LocalDateTime start = LocalDateTime.now();
        sb.append( start.toString( "yyyy.MM.dd HH mm ss" ) );

        return sb.toString();
    }
}
