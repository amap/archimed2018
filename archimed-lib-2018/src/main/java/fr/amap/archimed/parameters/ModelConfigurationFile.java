package fr.amap.archimed.parameters;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import fr.amap.archimed.application.ModelManager;
import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.model.Model;
import fr.amap.archimed.model.NodeType;
import fr.amap.archimed.parameters.input.MainParameters;

/**
 * "model manager" configuration file
 * 
 * @author François Grand
 * 2018-10-10
 */
public class ModelConfigurationFile // implements ParameterProvider
{
    /**
     * model manager
     */
    private ModelManager modelManager;

    public ModelConfigurationFile( ModelManager mm )
    {
        modelManager = mm;
    }

    /**
     * try to load a CSV with a Unix/Excel format
     * @param path CSV file path
     */
    private static CSVParser openCSV( String path ) throws IOException
    {
        Reader in = new FileReader( path );
        try
        {
            // delimiter ;
            // quote "
            // record separator \n

            return CSVFormat.INFORMIX_UNLOAD_CSV.withDelimiter( ';' ).withFirstRecordAsHeader().parse( in );
        }
        catch( IOException e )
        {
            // delimiter ,
            // quote "
            // record separator \r\n

            return CSVFormat.EXCEL.withFirstRecordAsHeader().parse( in );
        }
    }

    /**
     * convert model name in configuration file into enum
     * @param cat model name
     */
    private static ProcessCategory getCategoryFromString( String cat ) throws IOException
    {
        switch ( cat )
        {
            case "Interception model":
                return ProcessCategory.LightInterception;

            case "Optical_properties":
                return ProcessCategory.OpticalProperties;

            case "Photosynthesis":
                return ProcessCategory.Photosynthesis;

            default:
                throw new IOException( "unsupported '" + cat + "' process category" );
        }
    }

    /**
     * Load the "model manager" CSV file.
     * Principle :
     * "Group" column gives a name to the functional group,
     * "Type" column gives a name to the functional type,
     * following columns set configuration for processes.
     * "Node_id" column is ignored
     * 
     * @param mainParameters main Archimed parameters
     * @throws Exception 
     */
    public void loadCSVConfiguration( MainParameters mainParameters ) throws Exception
    {
        // open CSV file

        CSVParser parser = openCSV( mainParameters.getModelsManagerPath() );

        String groupName = null;
        int lineNumber = 2;
        NodeType nodeType;

        // get headers of the model columns

        Set<String> modelHeaders = parser.getHeaderMap().keySet();
        modelHeaders.remove( "Node_id" );
        modelHeaders.remove( "Group" );
        modelHeaders.remove( "Type" );
        if ( modelHeaders.size() == 0 )
            throw new IOException( "invalid CSV file " + mainParameters.getModelsManagerPath() + " : no model defined" );

        // iterate lines

        Iterator<CSVRecord> it = parser.iterator();
        while ( it.hasNext() )
        {
            CSVRecord record = it.next();

            // get functional group name

            String tmp = record.get( "Group" );
            if ( !tmp.equals( "" ) )
                groupName = tmp;

            if ( groupName == null )
                throw new IOException( "invalid empty 'Group' value at " + mainParameters.getModelsManagerPath() + ", line " + lineNumber );

            // get functional type

            nodeType = new NodeType( record.get( "Type" ) );

            // iterate models and parameters

            for ( String modelHeader : modelHeaders )
            {
                ProcessCategory cat = getCategoryFromString( modelHeader );

                // create key with current group name, process category, functional type

                ModelKey mk = new ModelKey( groupName, nodeType, cat );

                // parameters

                String params = record.get( modelHeader ).trim();

                if ( params.length() > 0 )
                {
                    // look for a model able to parse these parameters

                    boolean parsed = false;
                    for ( Model model : modelManager.getRegistredModels( mk ) )
                    {
                        // configuration parsing attempt
                        parsed = model.parseConfig( mk, params );
                        if ( parsed )
                        {
                            // found -> add it to model manager's list of configured models
                            modelManager.addConfiguredModel( mk, model );
                            break;
                        }
                    }
                    if ( !parsed )
                        throw new IOException( "invalid CSV file " + mainParameters.getModelsManagerPath() + " : no model found to parse '" + params + "' input (line " + lineNumber + ", '" + modelHeader + "' column)" );
                }
            }

            lineNumber++;
        }
    }

    // ParameterProvider interface

    //    @Override
    //    public ModelParameters getParameters( ModelKey modelKey )
    //    {
    //        // TODO Auto-generated method stub
    //        return null;
    //    }
    //
    //    @Override
    //    public ModelParameters getParameters( ModelKey modelKey, ASNode node )
    //    {
    //        // TODO Auto-generated method stub
    //        return null;
    //    }
}
