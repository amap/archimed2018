package fr.amap.archimed.parameters.input;

import java.util.Properties;

public class OutputParameters extends InputParameters
{
    /**
     * path to the output directory
     */
    private String outputDirectory;

    /**
     * Make outputs for each step if "true"; otherwise only final intergrated results are outputted.
     * Must be "true" if photosynthesis is enabled
     */
    private boolean atEachStep;

    /**
     * export the scene in a ops and opf files
     */
    private boolean exportOPS;

    /**
     * export the data in a xls file
     */
    private boolean exportData;

    /**
     * export the nodes' values in a csv file
     */
    private boolean exportNodesValues;

    /**
     * Save a scene view from each direction
     */
    private boolean printImages;

    /**
     * "printImages" options (height, irradiance, temperature)
     */
    private String printImagesOption;

    /**
     * export the scene in a txt file
     */
    private boolean exportTxt;

    public OutputParameters( Properties props ) throws Exception
    {
        super( props );
    }

    @Override
    protected void parse() throws Exception
    {
        outputDirectory = parseString( "outputDirectory" );

        atEachStep = parseBoolean( "atEachStep" );

        exportOPS = parseBoolean( "exportOPS", false );

        exportData = parseBoolean( "exportData", false );

        exportNodesValues = parseBoolean( "exportNodesValues", false );

        printImages = parseBoolean( "printImages", false );
        if ( printImages )
        {
            printImagesOption = parseString( "printImages.option" );
            switch ( printImagesOption )
            {
                case "height":
                case "irradiance":
                case "temperature":
                    break;

                default:
                    throw new Exception( "invalid printImagesOption=" + printImagesOption + " parameter" );
            }
        }

        exportTxt = parseBoolean( "exportTxt", false );
    }

    public String getOutputDirectory()
    {
        return outputDirectory;
    }

    public boolean isAtEachStep()
    {
        return atEachStep;
    }

    public boolean isExportOPS()
    {
        return exportOPS;
    }

    public boolean isExportData()
    {
        return exportData;
    }

    public boolean isExportNodesValues()
    {
        return exportNodesValues;
    }

    public boolean isPrintImages()
    {
        return printImages;
    }

    public String getPrintImagesOption()
    {
        return printImagesOption;
    }

    public boolean isExportTxt()
    {
        return exportTxt;
    }
}
