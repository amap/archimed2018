package fr.amap.archimed.parameters;

import fr.amap.archimed.application.ProcessCategory;
import fr.amap.archimed.model.NodeType;

/**
 * Model identification key.
 * Identification uses functional group name, process category (optical properties, photosynthesis, ...) and functional type.
 * When comparing 2 keys, a null value matches any value of the same type in the other key.
 * 
 * @author François Grand
 * 2018-10-10
 */
public class ModelKey
{
    /**
     * functional group name
     */
    private String groupName;

    /**
     * functional type
     */
    private NodeType nodeType;

    /**
     * process category (light interception, photosynthesis, ...)
     */
    private ProcessCategory processCategory;

    public ModelKey( String groupName, NodeType type, ProcessCategory cat )
    {
        this.groupName = groupName;
        nodeType = type;
        processCategory = cat;
    }

    /**
     * Tell if 'this' and given key match.
     * Group name, node functional type and process category are used. If a criterion is null, it matches all values  
     * @param key key to compare
     * @return true if keys match
     */
    public boolean matches( ModelKey key )
    {
        if ( groupName != null && key.groupName != null && !groupName.equals( key.groupName ) )
            return false;

        if ( nodeType != null && key.nodeType != null && !nodeType.equals( key.nodeType ) )
            return false;

        if ( processCategory != null && key.processCategory != null && processCategory != key.processCategory )
            return false;

        return true;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public NodeType getNodeType()
    {
        return nodeType;
    }

    public ProcessCategory getProcessCategory()
    {
        return processCategory;
    }

    // Object interface

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
        result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
        result = prime * result + ((processCategory == null) ? 0 : processCategory.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj )
    {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        ModelKey other = (ModelKey)obj;
        if ( groupName == null )
        {
            if ( other.groupName != null )
                return false;
        }
        else if ( !groupName.equals( other.groupName ) )
            return false;
        if ( nodeType == null )
        {
            if ( other.nodeType != null )
                return false;
        }
        else if ( !nodeType.equals( other.nodeType ) )
            return false;
        if ( processCategory != other.processCategory )
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder( "[group: " );
        sb.append( groupName );
        sb.append( ", process: " );
        sb.append( processCategory );
        sb.append( ", type: " );
        sb.append( nodeType );
        sb.append( "]" );
        return sb.toString();
    }
}