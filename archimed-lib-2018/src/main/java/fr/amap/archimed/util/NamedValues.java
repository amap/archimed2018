/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.amap.archimed.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

/** 
 * List of name-value pairs
 * The values can be read from a .properties file
 *
 * @author François Grand
 * 2018-09-25
 */
public class NamedValues extends Properties
{
    /**
     * Add all the entries from the given hashmap
     * @param list entry list to add
     */
    //    public void setEntries( HashMap<String, Object> list )
    //    {
    //        this.entries.putAll( list );
    //    }

    public boolean hasValue( String name )
    {
        return get( name ) != null;
    }

    public String getString( String name )
    {
        return (String)get( name );
    }

    public void setString( String name, String value )
    {
        put( name, value );
    }

    public boolean getBool( String name )
    {
        return ((Boolean)get( name )).booleanValue();
    }

    public void setBool( String name, boolean value )
    {
        put( name, Boolean.valueOf( value ) );
    }

    public int getInt( String name )
    {
        return ((Integer)get( name )).intValue();
    }

    public void setInt( String name, int value )
    {
        put( name, Integer.valueOf( value ) );
    }

    public double getDouble( String name )
    {
        return ((Double)get( name )).doubleValue();
    }

    public void setDouble( String name, double value )
    {
        put( name, Double.valueOf( value ) );
    }

    public void setDouble( NamedDouble d )
    {
        put( d.getName(), Double.valueOf( d.getValue() ) );
    }

    /**
     * Load the entries from a .properties file
     * @param path 
     * @throws IOException 
     */
    public void load( String path ) throws IOException
    {
        clear();
        Reader reader = new FileReader( path );
        try (BufferedReader bfr = new BufferedReader( reader ))
        {
            load( bfr );
        }
    }

    //    /**
    //     * Load the entries from a .properties file
    //     * @param path 
    //     * @throws IOException 
    //     */
    //    public void load( String path ) throws IOException
    //    {
    //        Properties prop = new Properties();
    //
    //        try (InputStream input = preprocessPropertiesFile( path ))
    //        {
    //            // load a properties file
    //            prop.load( input );
    //
    //            // get the property value and print it out
    //            this.entries.clear();
    //            for ( Map.Entry<Object, Object> e : prop.entrySet() )
    //                this.addEntry( (String)e.getKey(), e.getValue() );
    //
    //        }
    //    }
    //
    //    private static InputStream preprocessPropertiesFile( String path ) throws IOException
    //    {
    //        Reader reader = new FileReader( path );
    //        try (BufferedReader bfr = new BufferedReader( reader ))
    //        {
    //            ByteArrayOutputStream out = new ByteArrayOutputStream();
    //
    //            String readLine = null;
    //            while ( (readLine = bfr.readLine()) != null )
    //            {
    //                out.write( readLine.replace( "\\", "\\\\" ).getBytes() );
    //                out.write( "\n".getBytes() );
    //            }
    //
    //            return new ByteArrayInputStream( out.toByteArray() );
    //        }
    //    }
}
