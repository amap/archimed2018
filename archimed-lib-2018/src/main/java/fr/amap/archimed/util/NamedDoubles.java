/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package fr.amap.archimed.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/** 
 * List of name-double pairs
 *
 * @author François Grand
 * 2018-09-28
 */
public class NamedDoubles implements Iterable<NamedDouble>
{
    private List<NamedDouble> entries = new ArrayList<>();

    /**
     * Remove an entry from name
     * @param name entry name
     */
    public void remove( String name )
    {
        NamedDouble old = getEntry( name );
        if ( old != null )
            entries.remove( old );
        else
            System.out.println( "NamedValues: entry '" + name + "' not found" );
    }

    /**
     * Add a single entry
     * @param name new entry name
     * @param object value
     */
    public void addEntry( NamedDouble value )
    {
        entries.add( value );
    }

    /**
     * Get a value from name
     * @param name of the entry
     * @return the value
     */
    public NamedDouble getEntry( String name )
    {
        for ( NamedDouble v : entries )
            if ( v.getName().equals( name ) )
                return v;

        return null;
    }

    /**
     * remove all entries
     */
    public void clear()
    {
        entries.clear();
    }

    /**
     * Load the entries from a .properties file
     * @param path 
     * @throws IOException 
     */
    public void load( String path ) throws IOException
    {
        Properties prop = new Properties();

        try (InputStream input = preprocessPropertiesFile( path ))
        {
            // load a properties file
            prop.load( input );

            // get the property value and print it out
            this.entries.clear();
            for ( Map.Entry<Object, Object> e : prop.entrySet() )
                this.addEntry( new NamedDouble( (String)e.getKey(), Double.valueOf( e.getValue().toString() ) ) );

        }
    }

    private static InputStream preprocessPropertiesFile( String path ) throws IOException
    {
        Reader reader = new FileReader( path );
        try (BufferedReader bfr = new BufferedReader( reader ))
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            String readLine = null;
            while ( (readLine = bfr.readLine()) != null )
            {
                out.write( readLine.replace( "\\", "\\\\" ).getBytes() );
                out.write( "\n".getBytes() );
            }

            return new ByteArrayInputStream( out.toByteArray() );
        }
    }

    // Iterable<NamedDouble> interface

    @Override
    public Iterator<NamedDouble> iterator()
    {
        return entries.iterator();
    }
}
