/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.util.unit;

/**
 * Unit.
 * @author Julien Heurtebize
 */
public class Unit
{
    private double value;

    private Metric metric;

    /**
     * 
     * @param value
     * @param metric 
     */
    public Unit( double value, Metric metric )
    {
        this.value = value;
        this.metric = metric;
    }

    public double getValue()
    {
        return value;
    }

    public Metric getMetric()
    {
        return metric;
    }
}
