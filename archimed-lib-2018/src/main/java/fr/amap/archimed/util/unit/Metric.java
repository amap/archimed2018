/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.util.unit;

/**
 * Metric.
 * @author Julien Heurtebize
 */
public enum Metric
{
    MEGA( 1000000 ),

    KILO( 1000 ),

    HECTO( 100 ),

    DECA( 10 ),

    NONE( 1 ),

    DECI( 0.1 ),

    CENTI( 0.01 ),

    MILLI( 0.001 ),

    MICRO( 0.000001 ),

    NANO( 0.000000001 );

    private final double value;

    /**
     * Constructor.
     * @param value  value to assign
     */
    private Metric( double value )
    {
        this.value = value;
    }

    /**
     * Gets the value.
     * @return the value
     */
    public double getValue()
    {
        return value;
    }
}
