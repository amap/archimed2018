/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.util.unit;

/**
 * Utility class to convert some units.
 * @author Julien Heurtebize
 */
public class UnitConverter
{
    private final static float DAYLIGHT_FACTOR_PAR_TO_PFD = 4.57f;

    private final static float GLOBAL_PAR_TO_SI_RATIO = 0.48f;

    /**
     * Convert watts over a period to megajoules (1Wh = 0.0036MJ and 1W is a joule per second).
     * @param watts power
     * @param duration in decimal hour
     * @return The watt-hour in megajoules.
     */
    public static float wattToMJ( float watts, float duration )
    {
        return watts * duration * 0.0036f;
    }

    /**
     * Get the number of watts that provides the given number of megajoules for a duration in decimal hour.
     * @param mjValue
     * @param duration in decimal hour
     * @return 
     */
    public static float megaJoulesToWatts( float mjValue, float duration )
    {
        return joulesToWatts( mjValue * 1000000, duration );
    }

    /**
     * Get the number of watts that provides the given number of joules for a duration in decimal hour.
     * @param joules
     * @param duration in decimal hour
     * @return 
     */
    public static float joulesToWatts( float joules, float duration/*, Unit unit*/ )
    {
        return joules / (duration * 3600);
    }

    /**
     * Convert watt-hour to J.
     * Can also convert from kwh to MJ, the prefix unit is free.
     * @param sourceUnit
     * @param destMetric
     * @return 
     */
    public static double whToJ( Unit sourceUnit, Metric destMetric )
    {
        //? watt-hour to kwh
        double factor = UnitConverter.convert( new Unit( 1, sourceUnit.getMetric() ), Metric.KILO );
        double sourceInKWh = sourceUnit.getValue() / factor;

        double destInMJ = sourceInKWh * 3.6;
        double dest = UnitConverter.convert( new Unit( destInMJ, Metric.MEGA ), destMetric );

        return dest;
    }

    /**
     * Convert PAR (Photosynthetically Active Radiation) in Watts m-2 
     * into Photon Flux Density in micro mol m-2 s-1
     *
     * @param parWatt
     * @return PFD
     */
    public static double parToPPFD( double parWatt )
    {
        return parWatt * DAYLIGHT_FACTOR_PAR_TO_PFD;
    }

    /**
     * PAR (Photosynthetically Active Radiation) in Watts from solar irradiance in Watts.
     *
     * @param solarIrradiance in watts m-2 
     * @return PAR in watts m-2 
     */
    public static double parInSolarIrradiance( double solarIrradiance )
    {
        return (GLOBAL_PAR_TO_SI_RATIO * solarIrradiance);
    }

    /**
     * Photon Flux Density in micro mol m-2 s-1 from solar irradiance in Watts.
     * @param solarIrradiance
     * @return 
     */
    public static double solarIrradianceToPPFD( double solarIrradiance )
    {
        return parToPPFD( parInSolarIrradiance( solarIrradiance ) );
    }

    /**
     * Generic converter.
     * @param sourceUnit unit to convert
     * @param destMetric which metric to use
     * @return the unit converted
     */
    public static double convert( Unit sourceUnit, Metric destMetric )
    {
        double factor = sourceUnit.getMetric().getValue() / destMetric.getValue();
        return sourceUnit.getValue() * factor;
    }
}
