/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.util.graphic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 * Represents an image of an hemisphere built with an azimuthal equidistant projection.
 * @author Jean Dauzat, Julien Heurtebize (refactoring).
 */
public class HemisphereImage
{
    private final int nbPixels;

    private final int border;

    private final boolean drawMeridians;

    private final int nbMeridians;

    private final Color meridiansDrawColor;

    private final boolean drawParallels;

    private final int nbParallels;

    private final Color parallelsDrawColor;

    private final boolean drawCardinalPoints;

    private final Color cardinalPointsDrawColor;

    private final boolean drawSkyVault;

    private final Color skyVaultColor;

    private final boolean drawBackground;

    private final Color backgroundColor;

    private final boolean drawAzimutAngles;

    private final Color azimutAnglesDrawColor;

    private HemisphereImage( Builder builder )
    {
        this.nbPixels = builder.nbPixels;
        this.border = builder.border;
        this.drawMeridians = builder.drawMeridians;
        this.nbMeridians = builder.nbMeridians;
        this.meridiansDrawColor = builder.meridiansDrawColor;
        this.drawParallels = builder.drawParallels;
        this.nbParallels = builder.nbParallels;
        this.parallelsDrawColor = builder.parallelsDrawColor;
        this.drawSkyVault = builder.drawSkyVault;
        this.skyVaultColor = builder.skyVaultColor;
        this.drawAzimutAngles = builder.drawAzimutAngles;
        this.azimutAnglesDrawColor = builder.azimutAnglesDrawColor;
        this.drawCardinalPoints = builder.drawCardinalPoints;
        this.cardinalPointsDrawColor = builder.cardinalPointsDrawColor;
        this.backgroundColor = builder.backgroundColor;
        this.drawBackground = builder.drawBackground;
    }

    public void computeToImg( BufferedImage image )
    {
        Graphics2D g = (Graphics2D)image.getGraphics();

        float center = nbPixels / 2;
        float radius = center - border;

        if ( drawBackground )
        {
            g.setColor( backgroundColor );
            g.fillRect( 0, 0, nbPixels, nbPixels );
        }

        if ( drawSkyVault )
        {
            g.setColor( skyVaultColor );
            g.fillOval( (int)(center - radius), (int)(center - radius), (int)(2 * radius), (int)(2 * radius) );
        }

        if ( drawParallels )
        {
            g.setColor( parallelsDrawColor );

            float rad = radius / nbParallels;
            for ( int i = 1; i <= nbParallels; i++ )
            {
                g.drawOval( (int)(center - rad * i), (int)(center - rad * i), (int)(2 * rad * i), (int)(2 * rad * i) );
            }
        }

        if ( drawMeridians )
        {
            g.setColor( meridiansDrawColor );

            for ( int i = 1; i <= nbMeridians / 2; i++ )
            {
                double azimuth = i * (Math.PI / (nbMeridians / 2));
                double x = radius * Math.sin( azimuth );
                double y = radius * Math.cos( azimuth );
                g.drawLine( (int)(center - x), (int)(center - y), (int)(center + x), (int)(center + y) );
            }
        }

        g.setColor( Color.black );

        if ( drawAzimutAngles )
        {
            g.setColor( azimutAnglesDrawColor );

            int stepAngle = 360 / nbMeridians;

            for ( int i = 0; i < 360; i += stepAngle )
            {
                double angle = Math.toRadians( i );
                angle -= Math.PI / 2.0;

                String angleStr = i + "°";
                Rectangle2D stringBounds = g.getFontMetrics().getStringBounds( angleStr, g );

                float x = (float)(center + ((radius + 5) * Math.cos( angle )));
                float y = (float)(center + ((radius + 5) * Math.sin( angle )));

                g.drawString( angleStr, (float)(x/*-stringBounds.getWidth()*/), y );
            }
        }

        if ( drawCardinalPoints )
        {
            g.setColor( cardinalPointsDrawColor );

            g.drawString( "N", center, border / 2 );
            g.drawString( "S", center, center + radius + (border / 2) );
            g.drawString( "E", center + radius + (border / 2), center );
            g.drawString( "W", border / 2, center );
        }
    }

    public BufferedImage compute()
    {
        BufferedImage image = new BufferedImage( nbPixels, nbPixels, BufferedImage.TYPE_INT_RGB );
        computeToImg( image );

        return image;
    }

    public static class Builder
    {
        private int nbPixels = 600;

        private int border = 30;

        private boolean drawMeridians = true;

        private int nbMeridians = 36;

        private Color meridiansDrawColor = Color.white;

        private boolean drawParallels = true;

        private int nbParallels = 9;

        private Color parallelsDrawColor = Color.white;

        private boolean drawSkyVault = true;

        private Color skyVaultColor = new Color( 150, 220, 255 );

        private boolean drawBackground = true;

        private Color backgroundColor = Color.white;

        private boolean drawCardinalPoints = true;

        private Color cardinalPointsDrawColor = Color.black;

        private boolean drawAzimutAngles = true;

        private Color azimutAnglesDrawColor = Color.black;

        /**
         * 
         * @param nbPixels The number of pixels in x and y-axis. 
         * @return 
         */
        public Builder nbPixels( int nbPixels )
        {
            this.nbPixels = nbPixels;
            return this;
        }

        /**
         * 
         * @param border An offset in pixel number between the hemisphere border and a side of the image.
         * @return 
         */
        public Builder border( int border )
        {
            this.border = border;
            return this;
        }

        /**
         * 
         * @param drawMeridians True to draw the meridians, false otherwise.
         * @return 
         */
        public Builder drawMeridians( boolean drawMeridians )
        {
            this.drawMeridians = drawMeridians;
            return this;
        }

        /**
         * 
         * @param nbMeridians The number of meridians to draw.
         * @return 
         */
        public Builder nbMeridians( int nbMeridians )
        {
            this.nbMeridians = nbMeridians;
            return this;
        }

        /**
         * 
         * @param meridiansDrawColor The color of the meridians circles.
         * @return 
         */
        public Builder meridiansDrawColor( Color meridiansDrawColor )
        {
            this.meridiansDrawColor = meridiansDrawColor;
            return this;
        }

        /**
         * 
         * @param drawParallels True to draw the parallels, false otherwise.
         * @return 
         */
        public Builder drawParallels( boolean drawParallels )
        {
            this.drawParallels = drawParallels;
            return this;
        }

        /**
         * 
         * @param nbParallels The number of parallels to draw.
         * @return 
         */
        public Builder nbParallels( int nbParallels )
        {
            this.nbParallels = nbParallels;
            return this;
        }

        /**
         * 
         * @param parallelsDrawColor The color of the parallels circles.
         * @return 
         */
        public Builder parallelsDrawColor( Color parallelsDrawColor )
        {
            this.parallelsDrawColor = parallelsDrawColor;
            return this;
        }

        /**
         * 
         * @param drawSkyVault True to draw the sky vault, false otherwise.
         * @return 
         */
        public Builder drawSkyVault( boolean drawSkyVault )
        {
            this.drawSkyVault = drawSkyVault;
            return this;
        }

        /**
         * 
         * @param skyVaultColor The color of the sky vault.
         * @return 
         */
        public Builder skyVaultColor( Color skyVaultColor )
        {
            this.skyVaultColor = skyVaultColor;
            return this;
        }

        /**
         * 
         * @param drawCardinalPoints True to draw the cardinal points 
         * (East as E, North as N, West as W and South as S), false otherwise.
         * @return 
         */
        public Builder drawCardinalPoints( boolean drawCardinalPoints )
        {
            this.drawCardinalPoints = drawCardinalPoints;
            return this;
        }

        /**
         * 
         * @param cardinalPointsDrawColor The color of text of cardinal points.
         * @return 
         */
        public Builder cardinalPointsDrawColor( Color cardinalPointsDrawColor )
        {
            this.cardinalPointsDrawColor = cardinalPointsDrawColor;
            return this;
        }

        /**
         * 
         * @param drawAzimutAngles True to draw azimut angles values (degrees), false otherwise.
         * @return 
         */
        public Builder drawAzimutAngles( boolean drawAzimutAngles )
        {
            this.drawAzimutAngles = drawAzimutAngles;
            return this;
        }

        /**
         * 
         * @param azimutAnglesDrawColor The text color of azimut angles values.
         * @return 
         */
        public Builder azimutAnglesDrawColor( Color azimutAnglesDrawColor )
        {
            this.azimutAnglesDrawColor = azimutAnglesDrawColor;
            return this;
        }

        /**
         * 
         * @param drawBackground True to draw a background, fill the image, false otherwise.
         * @return 
         */
        public Builder drawBackground( boolean drawBackground )
        {
            this.drawBackground = drawBackground;
            return this;
        }

        /**
         * 
         * @param backgroundColor The color of the background surrounding the hemisphere.
         * @return 
         */
        public Builder backgroundColor( Color backgroundColor )
        {
            this.backgroundColor = backgroundColor;
            return this;
        }

        /**
         * Build the image with the given parameters.
         * @return An {@link HemisphereImage}
         */
        public HemisphereImage build()
        {
            return new HemisphereImage( this );
        }
    }
}
