package fr.amap.archimed.util;

/**
 * double value with a name
 * 
 * @author François Grand
 * 2018-09-28
 */
public class NamedDouble
{
    private String name;

    private double value;

    public NamedDouble( String name )
    {
        this.name = name;
    }

    public NamedDouble( String name, double value )
    {
        this.name = name;
        this.value = value;
    }

    public double getValue()
    {
        return value;
    }

    public void setValue( double val )
    {
        value = val;
    }

    public String getName()
    {
        return name;
    }
}
