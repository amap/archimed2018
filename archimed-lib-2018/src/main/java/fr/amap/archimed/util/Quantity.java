package fr.amap.archimed.util;

/**
 * physical quantity (real value with a name and unit)
 * 
 * @author François Grand
 * 2018-09-28
 */
public class Quantity extends NamedDouble
{
    private String unit;

    public Quantity( String name )
    {
        super( name );
    }

    public String getUnit()
    {
        return unit;
    }

    public void setUnit( String unit )
    {
        this.unit = unit;
    }
}
