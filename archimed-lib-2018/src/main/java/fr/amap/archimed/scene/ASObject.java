package fr.amap.archimed.scene;

import jeeb.lib.formats.GWALoader;
import jeeb.lib.formats.OPFLoaderV2;
import jeeb.lib.structure.ArchiNode;
import jeeb.lib.structure.ArchiTree;

/**
 * Archimed scene node representing objects like plants, pavement, ...
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ASObject extends ASNode
{
    public ASObject( int fileId )
    {
        super( fileId );
    }

    private void createItems( ArchiTree tree ) throws Exception
    {
        // matrix to convert centimeters in OPF to meters (scene)

        //        Matrix4f cmToM = new Matrix4f();
        //        cmToM.setIdentity();
        //        cmToM.setScale( 0.01f );

        // create items for nodes with mesh

        for ( ArchiNode archiNode : tree.getNodes() )
            if ( archiNode.has3DMesh() )
            {
                ASItem item = new ASItem( archiNode );
                item.setParent( this );
            }
    }

    /**
     * load OPF file
     * @param path OPF path
     * @throws Exception
     */
    public void loadOPF( String path ) throws Exception
    {
        OPFLoaderV2 opfLoader = new OPFLoaderV2( path );
        ArchiTree tree = opfLoader.load();
        createItems( tree );
    }

    /**
     * load GWA file
     * @param path GWA file path
     * @throws Exception
     */
    public void loadGWA( String path ) throws Exception
    {
        GWALoader opfLoader = new GWALoader( path );
        ArchiTree tree = opfLoader.load();
        createItems( tree );
    }
}
