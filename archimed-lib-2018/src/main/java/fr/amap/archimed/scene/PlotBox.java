package fr.amap.archimed.scene;

import javax.vecmath.Point2f;
import javax.vecmath.Point2i;
import javax.vecmath.Point3f;

import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * PlotBox class Defines an (x,y) plotting area to reduce/expand the scene
 * canvas infCorner/supCorner: corners of he boxPlot clipping: elements of the
 * scene outside the specified plotBox are deleted instead of
 * translated/duplicated
 * 
 * @author Cresson, Dec. 2012
 */
public class PlotBox
{
    private Point2f infCorner;

    private Point2f supCorner;

    private boolean clipping;

    /**
     * côté du carré qui aurait la même surface qu'un pixel
     */
    private double pixelSizeUnit;

    /**
     * taille x y de la table de pixel en nombre de pixels
     */
    private Point2i tableSize;

    /**
     * dimension du rectangle du pixel.
     */
    private Point2f pixelSize;

    /**
     * surface d'un pixel dans la table.
     */
    private double pixelArea;

    /**
     * Defines a PlotBox with 2-Corner points
     * 
     * @param cornerInf
     * @param cornerSup
     */
    public PlotBox( Point2f cornerInf, Point2f cornerSup, int pixelCount )
    {
        this( cornerInf, cornerSup, false, pixelCount );
    }

    /**
     * Defines a PlotBox with 2-Corner points, and clipping option.
     * 
     * @param cornerInf
     * @param cornerSup
     * @param clipping  when sets to true, entities which are outside the plotbox
     *                  are erased
     */
    private PlotBox( Point2f cornerInf, Point2f cornerSup, boolean clipping, int pixelCount )
    {
        // Check inf && sup corners, switch if necessary
        Point2f sup = new Point2f( cornerSup );
        Point2f inf = new Point2f( cornerInf );

        if ( cornerInf.x > cornerSup.x )
        {
            sup.x = cornerInf.x;
            inf.x = cornerSup.x;
        }
        if ( cornerInf.y > cornerSup.y )
        {
            sup.y = cornerInf.y;
            inf.y = cornerSup.y;
        }

        this.infCorner = inf;
        this.supCorner = sup;
        this.clipping = clipping;

        setPixelCount( pixelCount );
    }

    public Point2f getInfCorner()
    {
        return infCorner;
    }

    public Point2f getSupCorner()
    {
        return supCorner;
    }

    public BoundingBox3f getBox()
    {
        BoundingBox3f box = new BoundingBox3f();
        box.update( new Point3f( infCorner.x, infCorner.y, -1f ) );
        box.update( new Point3f( supCorner.x, supCorner.y, +1f ) );
        return box;
    }

    public boolean getClipping()
    {
        return clipping;
    }

    /**
     * @return plotbox surface area
     */
    public double getArea()
    {
        return (supCorner.getX() - infCorner.getX()) * (supCorner.getY() - infCorner.getY());
    }

    private void setPixelCount( int count )
    {
        double plotArea = getArea();

        pixelSizeUnit = Math.sqrt( plotArea / count );

        tableSize = new Point2i( (int)((supCorner.x - infCorner.x) / pixelSizeUnit), (int)((supCorner.y - infCorner.y) / pixelSizeUnit) );

        pixelSize = new Point2f( (supCorner.x - infCorner.x) / tableSize.x, (supCorner.y - infCorner.y) / tableSize.y );

        pixelArea = plotArea / (tableSize.x * tableSize.y);
    }

    public Point2i getTableSize()
    {
        return tableSize;
    }

    public double getPixelArea()
    {
        return pixelArea;
    }

    public Point2f getPixelSize()
    {
        return pixelSize;
    }
}
