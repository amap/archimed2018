package fr.amap.archimed.scene;

/**
 * Archimed scene functional group node
 * eg. all palm trees of the scene
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ASGroup extends ASNode
{
    private String groupName;

    public ASGroup( int fileId, String groupName )
    {
        super( fileId );
        this.groupName = groupName;
    }

    public String getGroupName()
    {
        return groupName;
    }
}
