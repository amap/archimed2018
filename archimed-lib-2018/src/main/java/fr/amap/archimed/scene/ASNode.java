package fr.amap.archimed.scene;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Transformations;
import fr.amap.archimed.util.NamedValues;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * basic node of the Archimed scene tree
 * taken from Florian Hilaire
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ASNode
{
    /**
     * id from file
     */
    private int fileId;

    /**
     * parent node in tree
     */
    protected ASNode parent;

    /**
     * child nodes in tree
     */
    protected List<ASNode> kids;

    /**
     * attribute list
     */
    protected NamedValues attributes;

    /**
     * transformations from OPS file
     * */
    private double x = 0, y = 0, z = 0, scale = 1, inclinationAzimut = 0, inclinationAngle = 0, stemTwist = 0;

    /**
     * transformations as a matrix
     */
    private Matrix4f transformation;

    /**
     * bounding box including all child nodes
     */
    protected BoundingBox3f boundingBox;

    public ASNode( int id )
    {
        fileId = id;
        attributes = new NamedValues();
    }

    /**
     * @return the node id read from file
     */
    public int getFileId()
    {
        return fileId;
    }

    /**
     * @return the parent node
     */
    public ASNode getParent()
    {
        return parent;
    }

    /**
     * set parent node
     * @param prnt parent node
     */
    public void setParent( ASNode prnt )
    {
        if ( parent != null )
            parent.removeKid( this );

        parent = prnt;
        if ( parent != null )
            parent.addKid( this );
    }

    private List<ASNode> getKids()
    {
        if ( kids == null )
            kids = new ArrayList<>();
        return kids;
    }

    /**
     * @return true if node is in direct kid list
     */
    public boolean hasDirectKid( ASNode node )
    {
        for ( ASNode k : kids )
            if ( k == node )
                return true;
        return false;
    }

    /**
     * add a node to the direct kid list
     * @param node node to add
     */
    protected void addKid( ASNode node )
    {
        getKids().add( node );
    }

    /**
     * remove a node from direct kid list
     * @param node node to remove
     * @return true if the list contained the given node
     */
    protected boolean removeKid( ASNode node )
    {
        return getKids().remove( node );
    }

    public NamedValues getAttributes()
    {
        return attributes;
    }

    public void setPositionAndRotation( double x, double y, double z, double inclinationAzimut, double inclinationAngle, double stemTwist )
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.inclinationAzimut = inclinationAzimut;
        this.inclinationAngle = inclinationAngle;
        this.stemTwist = stemTwist;
    }

    public Matrix4f getTransformation()
    {
        if ( transformation == null )
            updateTransformMatrix();
        return transformation;
    }

    /**
     * update transformation matrix with own/ancestors translation, rotation, ....
     */
    public void updateTransformMatrix()
    {
        Transformations trans = new Transformations();

        scale( trans, scale );
        twist( trans, stemTwist );
        incline( trans, inclinationAzimut, inclinationAngle );
        translate( trans, x, y, z );

        ASNode ancestor = this.getParent();
        while ( ancestor != null )
        {
            scale( trans, ancestor.scale );
            twist( trans, ancestor.stemTwist );
            incline( trans, ancestor.inclinationAzimut, ancestor.inclinationAngle );
            translate( trans, ancestor.x, ancestor.y, ancestor.z );

            ancestor.transformation = trans.getMatrix();
            ancestor = ancestor.getParent();
        }

        this.x = trans.getMatrix().m03 / 100.0f;
        this.y = trans.getMatrix().m13 / 100.0f;
        this.z = trans.getMatrix().m23 / 100.0f;
        this.transformation = trans.getMatrix();
    }

    private static void scale( Transformations transformation, double scale )
    {
        if ( scale != 0 )
            transformation.setScale( (float)scale );
    }

    private static void twist( Transformations transformation, double angle )
    {
        if ( angle != 0 )
            transformation.setRotationAroundZ( (float)Math.toRadians( angle ) );
    }

    private static void incline( Transformations transformation, double azimutPlan, double inclination )
    {
        if ( inclination != 0 )
        {
            double inclinationAzimutRadAncestor = Math.toRadians( azimutPlan );
            double inclinationAngleRadAncestor = Math.toRadians( inclination );

            Vector3f zGlobal = new Vector3f( 0, 0, 1 );
            Vector3f azimut = new Vector3f( (float)Math.cos( inclinationAzimutRadAncestor ), (float)Math.sin( inclinationAzimutRadAncestor ), 0 );

            Vector3f azimutRotAxis = new Vector3f();
            azimutRotAxis.cross( zGlobal, azimut );

            transformation.setRotationAroundAxis( azimutRotAxis, (float)inclinationAngleRadAncestor );

        }
    }

    private static void translate( Transformations transformation, double x, double y, double z )
    {
        Vector3f trans = new Vector3f( (float)x * 100.0f, (float)y * 100.0f, (float)z * 100.0f );
        transformation.setTranslation( trans );
    }

    /**
     * get bounding box, optionally computing it recursively
     * @return bounding box of this node, including all kids
     */
    public BoundingBox3f getBoundingBox()
    {
        if ( boundingBox == null )
        {
            boundingBox = new BoundingBox3f();

            for ( ASNode k : kids )
            {
                BoundingBox3f kBB = k.getBoundingBox();
                boundingBox.update( kBB.min );
                boundingBox.update( kBB.max );
            }
        }

        return boundingBox;
    }

    /**
     * set bounding box to null, including parents all the way to root node
     * to force an update at next call to getBoundingBox()
     */
    public void invalidateBoundingBox()
    {
        boundingBox = null;
        if ( parent != null )
            parent.invalidateBoundingBox();
    }

    /**
     * create an iterator over all child nodes, starting from this
     */
    public ASTreeIterator treeIterator()
    {
        return new ASTreeIterator( this );
    }
}
