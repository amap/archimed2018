package fr.amap.archimed.scene;

import java.awt.Color;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

import jeeb.lib.formats.GeometryWithAttribute;
import jeeb.lib.structure.geometry.mesh.SimpleMesh;
import jeeb.lib.structure.geometry.mesh.SimpleMeshFactory;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * Pavage du sol défini par la plot box (xMin, yMin, xMax, yMax) et plotPaving du fichier ArchimedConfiguration.properties.
 * 
 * 2018-10-17 modif par F. Grand
 * @author Jean Dauzat
 */
public class BoxPaving
{
    private BoundingBox3f plotBox;

    private int cobbleCount;

    /**
     * @param infCorner first corner of the plot box
     * @param supCorner opposite corner of the plot box
     * @param nbCobbles number of cobblestones
     */
    public BoxPaving( Point2f infCorner, Point2f supCorner, int nbCobbles )
    {
        Point3f infPts = new Point3f( infCorner.x, infCorner.y, 0.005f );
        Point3f supPts = new Point3f( supCorner.x, supCorner.y, 100 );
        plotBox = new BoundingBox3f( infPts, supPts );

        this.cobbleCount = nbCobbles;
    }

    /** Generate a cobble paving of the box floor.
     * The number of cobbles and their size are fitted in order to have integer values of cobbles along X and Y.
     * Consequently the cobbles may not be exactly square.
     * @param box
     * @param cobbleCount	Approximative number of cobbles
     * @return Paving in GWA format
     */
    public GeometryWithAttribute generateCobblestonePaving()
    {
        double plotBoxX = (plotBox.max.x - plotBox.min.x);
        double plotBoxY = (plotBox.max.y - plotBox.min.y);
        double plotBoxArea = plotBoxX * plotBoxY;
        double cobbleArea = plotBoxArea / cobbleCount;
        double cobbleEdge = Math.sqrt( cobbleArea );

        double cobbleXSize = plotBoxX / (int)(plotBoxX / cobbleEdge);
        double cobbleYSize = plotBoxY / (int)(plotBoxY / cobbleEdge);

        return paveGround( cobbleXSize * 100, cobbleYSize * 100 );
    }

    /** Generate a cobble paving of the box floor.
     * Some cobbles may be cut at the border of the box in order to remain totally included in the box.
     * @param plotBox
     * @param cobbleXSize_cm
     * @param cobbleYSize_cm
     * @return Paving in GWA format
     */
    private GeometryWithAttribute paveGround( double cobbleXSize_cm, double cobbleYSize_cm )
    {
        GeometryWithAttribute gwa = new GeometryWithAttribute();

        double xMin = plotBox.min.x * 100;
        double yMin = plotBox.min.y * 100;
        double xMax = plotBox.max.x * 100;
        double yMax = plotBox.max.y * 100;
        double zMin = plotBox.min.z * 100;

        int i = 1;
        for ( double x = xMin; x < xMax; x += cobbleXSize_cm )
        {

            double xSize = cobbleXSize_cm;
            if ( x + cobbleXSize_cm > xMax )
            {
                xSize = xMax - x;
            }

            for ( double y = yMin; y < yMax; y += cobbleYSize_cm )
            {

                double ySize = cobbleYSize_cm;
                if ( y + cobbleYSize_cm > yMax )
                {
                    ySize = yMax - y;
                }

                // Create and locate the cobblestone
                double xCenter_cm = x + (xSize / 2d);
                double yCenter_cm = y + (ySize / 2d);

                Point2f[] points = new Point2f[ 4 ];
                float x0 = (float)-xSize / 2f;
                float x1 = (float)xSize / 2f;
                float y0 = (float)-ySize / 2f;
                float y1 = (float)ySize / 2f;
                points[ 0 ] = new Point2f( x0, y0 );
                points[ 1 ] = new Point2f( x1, y0 );
                points[ 2 ] = new Point2f( x1, y1 );
                points[ 3 ] = new Point2f( x0, y1 );

                SimpleMesh p = SimpleMeshFactory.polygon( points );

                p.translate( new Point3f( (float)xCenter_cm, (float)yCenter_cm, (float)zMin ) );

                Color cobblestoneColor = new Color( 120, 100, 50 );
                gwa.addNode( i, "Cobblestone", p, GeometryWithAttribute.KEEP_NORMALS );
                gwa.setAttribute( i, "Color", cobblestoneColor );

                i++; // next cobble
            }
        }

        return gwa;
    }
}
