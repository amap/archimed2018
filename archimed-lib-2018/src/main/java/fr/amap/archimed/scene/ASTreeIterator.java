package fr.amap.archimed.scene;

import java.util.Iterator;
import java.util.Stack;

/*
 cf. https://stackoverflow.com/questions/5278580/non-recursive-depth-first-search-algorithm#5278667
 
depth first:

  list nodes_to_visit = {root};
  while( nodes_to_visit isn't empty ) {
    currentnode = nodes_to_visit.take_first();
    nodes_to_visit.prepend( currentnode.children );
    //do something
  }

breadth first :

  list nodes_to_visit = {root};
  while( nodes_to_visit isn't empty ) {
    currentnode = nodes_to_visit.take_first();
    nodes_to_visit.append( currentnode.children );
    //do something
  }
*/

/**
 * depth first tree iterator
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ASTreeIterator implements Iterator<ASNode>
{
    /**
     * next few nodes to return
     * it is filled and emptied while iterating
     */
    private Stack<ASNode> toVisit;

    public ASTreeIterator( ASNode root )
    {
        toVisit = new Stack<>();
        toVisit.push( root );
    }

    @Override
    public boolean hasNext()
    {
        return toVisit.size() > 0;
    }

    @Override
    public ASNode next()
    {
        ASNode curr = toVisit.pop();
        if ( curr.kids != null )
            for ( int n = curr.kids.size() - 1; n >= 0; n-- )
                toVisit.push( curr.kids.get( n ) );
        return curr;
    }
}
