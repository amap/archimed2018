package fr.amap.archimed.scene;

import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.math.geometry.element.shape.mesh.TriangulatedMesh;
import fr.amap.archimed.model.NodeType;
import jeeb.lib.structure.ArchiNode;
import jeeb.lib.structure.geometry.mesh.Mesh;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * Archimed scene item, ie. node with a functional type and a mesh
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ASItem extends ASNode
{
    private final static Logger logger = LogManager.getLogger();

    /**
     * node functional type
     */
    private final NodeType nodeType;

    /**
     * mesh from ArchiNode
     */
    private final TriangulatedMesh mesh;

    /**
     * functional group name
     */
    private String groupName;

    /**
     * object (plant, pavement, ...) id
     */
    private int objectId = -1;

    public ASItem( int fileId, NodeType type, TriangulatedMesh tm )
    {
        super( fileId );
        nodeType = type;
        mesh = tm;
    }

    /**
     * create item from ArchiNode with mesh
     * @throws Exception 
     */
    public ASItem( ArchiNode archiNode ) throws Exception
    {
        super( archiNode.getId() );

        if ( !archiNode.has3DMesh() )
            throw new Exception( "ArchiNode has no geometry" );

        Mesh transfMesh = archiNode.getGeometry().getTransformedMesh();
        if ( transfMesh.getPoints().length == 0 )
            throw new Exception( "empty mesh found for node id=" + archiNode.getId() );

        // matrix to convert centimeters in OPF to meters (scene)
        Matrix4f cmToM = new Matrix4f();
        cmToM.setIdentity();
        cmToM.setScale( 0.01f );
        transfMesh.transform( cmToM );

        nodeType = new NodeType( archiNode.getType().getName() );
        mesh = new TriangulatedMesh( transfMesh );
    }

    public NodeType getNodeType()
    {
        return nodeType;
    }

    /**
     * @return mesh associated to this node
     */
    public TriangulatedMesh getMesh()
    {
        return mesh;
    }

    /**
     * @return functional group name of this node
     */
    public String getGroupName()
    {
        if ( groupName == null )
        {
            ASNode prnt = parent;
            while ( !(prnt instanceof ASGroup) )
                prnt = prnt.parent;

            groupName = ((ASGroup)prnt).getGroupName();
        }
        return groupName;
    }

    /**
     * @return id of parent "object" node (ie. plant, pavement, ... objects)
     */
    public int getObjectId()
    {
        if ( objectId == -1 )
        {
            ASNode prnt = parent;
            while ( !(prnt instanceof ASObject) )
                prnt = prnt.parent;

            objectId = ((ASObject)prnt).getFileId();
        }

        return objectId;
    }

    @Override
    public void setParent( ASNode prnt )
    {
        super.setParent( prnt );
        if ( parent != null )
            parent.invalidateBoundingBox();
    }

    @Override
    public BoundingBox3f getBoundingBox()
    {
        if ( boundingBox == null )
        {
            boundingBox = new BoundingBox3f();

            for ( Point3f point : mesh.getPoints() )
                if ( Float.isNaN( point.x ) || Float.isNaN( point.y ) || Float.isNaN( point.z ) )
                    logger.warn( "ASItem.getBoundingBox() : NaN point found for node id=" + getFileId() );
                else
                    boundingBox.update( point );
        }

        return boundingBox;
    }
}
