package fr.amap.archimed.scene;

import java.io.IOException;
import java.util.Collection;

import javax.vecmath.Point2f;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.formats.OPSReader;
import fr.amap.archimed.formats.OPSReader.OPSRecord;
import fr.amap.archimed.parameters.input.MainParameters;
import jeeb.lib.formats.GeometryWithAttribute;
import jeeb.lib.structure.ArchiNode;

/**
 * Archimed scene tree
 * 
 * @author François Grand
 * 2018-10-12
 */
public class ArchimedScene implements Iterable<ASNode>
{
    private final static Logger logger = LogManager.getLogger();

    /**
     * tree root
     */
    private ASNode root;

    /**
     * plot box
     */
    private PlotBox plotBox;

    public ArchimedScene()
    {
    }

    public ASNode getRoot()
    {
        return root;
    }

    /**
     * load an OPS scene file and referenced OPF files
     * @param path OPS file path
     * @throws Exception
     */
    public void loadOPS( MainParameters prms ) throws Exception
    {
        final String scenePath = prms.getScenePath();

        logger.info( "loading scene from " + scenePath );

        OPSReader opsLoader = new OPSReader( scenePath );

        root = new ASNode( -1 );
        ASNode group = null;
        int nTerrain = 0; // number of "terrain" lines
        int nPlant = 0; // number of "plant" lines

        for ( OPSRecord record : opsLoader.getRecords() )
        {
            String[] fields = record.getFields();

            switch ( record.getType() )
            {
                // T xOrigin yOrigin zOrigin xSize ySize flat
                // used for plotbox, "T", "flat" and "zOrigin" are ignored 
                case TERRAIN:
                    nTerrain++;

                    float x0 = Float.valueOf( fields[ 1 ] );
                    float y0 = Float.valueOf( fields[ 2 ] );
                    float x1 = x0 + Float.valueOf( fields[ 4 ] );
                    float y1 = y0 + Float.valueOf( fields[ 5 ] );
                    Point2f inf = new Point2f( x0, y0 );
                    Point2f sup = new Point2f( x1, y1 );
                    plotBox = new PlotBox( inf, sup, prms.getLightInterceptionParameters().getNumberOfPixels() );
                    break;

                // #[Archimed] <group name>
                case GROUP:
                    group = new ASGroup( -1, fields[ 1 ] );
                    group.setParent( root );
                    break;

                // sceneId plantId plantFileName x y z scale inclinationAzimut inclinationAngle stemTwist
                case PLANT:
                    if ( group == null )
                        throw new Exception( scenePath + ": no functional group definition found, please add a comment line with format #[Archimed] <group name> before first plant" );

                    nPlant++;

                    ASObject obj = new ASObject( Integer.valueOf( fields[ 1 ] ) );
                    obj.setParent( group );

                    String path = fields[ 2 ];
                    if ( FilenameUtils.getExtension( path ).toLowerCase().equals( "opf" ) )
                    {
                        obj.attributes.setBool( "opf", true );
                        obj.loadOPF( FilenameUtils.getFullPath( scenePath ) + path );
                    }
                    else if ( FilenameUtils.getExtension( path ).toLowerCase().equals( "gwa" ) )
                    {
                        obj.attributes.setBool( "opf", false );
                        obj.loadGWA( FilenameUtils.getFullPath( scenePath ) + path );
                    }
                    else
                        throw new IOException( "invalid geometry file " + path );

                    double x = Double.valueOf( fields[ 3 ] );
                    double y = Double.valueOf( fields[ 4 ] );
                    double z = Double.valueOf( fields[ 5 ] );
                    double azimut = Double.valueOf( fields[ 6 ] );
                    double inclination = Double.valueOf( fields[ 7 ] );
                    double stemTwist = Double.valueOf( fields[ 8 ] );
                    obj.setPositionAndRotation( x, y, z, azimut, inclination, stemTwist );
                    break;

                default:
                    throw new Exception( "ArchimedScene.loadOPS : unsupported OPSRecordType=" + record.getType() );
            }
        }

        if ( nTerrain > 1 )
            logger.warn( scenePath + ": plotbox defined multiple times ! Last definition will overwrite previous ones" );

        if ( nTerrain == 0 )
            throw new Exception( scenePath + ": no plotbox definition found" );

        if ( nPlant == 0 )
            throw new Exception( scenePath + ": no reference to OPF file(s) found" );
    }

    /**
     * generate a cobblestone paving according to parameters
     * @param prms 
     * @throws Exception 
     */
    public void generatePaving( MainParameters prms ) throws Exception
    {
        // "plotPaving" parameters
        int nCB = prms.getLightInterceptionParameters().getPlotPaving();

        if ( nCB > 0 )
        {
            logger.info( "generating plot box pavement with " + nCB + " cobblestones" );

            // create pavement group

            ASGroup pavementGroup = new ASGroup( -1, "pavement" );
            pavementGroup.setParent( root );

            // create pavement object

            ASObject pavementObject = new ASObject( -1 );
            pavementObject.setParent( pavementGroup );

            // create pavement GWA 

            GeometryWithAttribute pavingGWA = new BoxPaving( plotBox.getInfCorner(), plotBox.getSupCorner(), nCB ).generateCobblestonePaving();

            // create pavement items

            Collection<ArchiNode> kids = pavingGWA.getArchiTree().getNodes();
            for ( ArchiNode k : kids )
                if ( k.has3DMesh() )
                {
                    ASItem pavementItem = new ASItem( k );
                    pavementItem.setParent( pavementObject );
                }
        }
    }

    public PlotBox getPlotBox()
    {
        return plotBox;
    }

    /**
     * find node from its id
     * @param id node if
     * @throws IOException
     */
    public ASNode getNodeFromId( int id ) throws IOException
    {
        for ( ASNode node : this )
            if ( node.getFileId() == id )
                return node;

        throw new IOException( "invalid node id " + id );
    }

    // Iterable interface

    /**
     * create an iterator over all scene nodes, starting from root
     */
    @Override
    public ASTreeIterator iterator()
    {
        return root.treeIterator();
    }
}
