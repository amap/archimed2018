package fr.amap.archimed;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.amap.archimed.application.Application;
import fr.amap.archimed.parameters.input.MainParameters;

/**
 * Archimed entry point
 * 
 * @author François Grand
 * 2018-09-25
 */
public class ArchimedMain
{
    private final static Logger logger = LogManager.getLogger();

    /**
     * path to property file
     */
    private String propertiesPath;

    public static void main( String[] args )
    {
        try
        {
            new ArchimedMain().run( args );
            System.exit( 0 );
        }
        catch( Exception e )
        {
            logger.fatal( "error during execution : " + e.getMessage() );
            logger.fatal( "\n", e );
            System.exit( 1 );
        }
    }

    private void run( String[] args ) throws Exception
    {
        try
        {
            parseArgs( args );
        }
        catch( Exception e )
        {
            printHelp();
            throw e;
        }

        // load ".properties" main configuration file
        Properties props = loadConfigurationProperties();
        MainParameters mainParameters = new MainParameters( props );

        new Application( mainParameters ).execute();
        logger.info( "END" );
    }

    private Properties loadConfigurationProperties() throws IOException
    {
        logger.info( "loading configuration from '" + propertiesPath + "'" );

        try (FileReader fr = new FileReader( propertiesPath ))
        {
            Properties props = new Properties();
            props.load( fr );
            return props;
        }
    }

    private static void printHelp()
    {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( new PrintWriter( System.err, true ), 1000, "archimed <property file>", "\nLight interception and photosynthesis on 3D plant models\n\n", getOptions(), 0, 0, null, true );
    }

    private static Options getOptions()
    {
        Options res = new Options();

        //        Option opt1 = new Option( "s", "support", true, "template structure name" );
        //        opt1.setRequired( false );
        //        res.addOption( opt1 );
        //
        //        Option opt2 = new Option( "t", "target", true, "name of sequence to model" );
        //        opt2.setRequired( false );
        //        res.addOption( opt2 );
        //
        //        //        Option opt3 = new Option( "w", "scwrl", true, "generate SCWRL files (.pdb & .scw) with given base file name" );
        //        //        opt3.setRequired( false );
        //        //        res.addOption( opt3 );
        //
        //        Option opt3 = Option.builder( "w" ).longOpt( "scwrl" ).desc( "generate SCWRL files (.pdb & .scw) with given base file name. If no argument is given, base will be <alignment name>-<target name>-<query name>" ).hasArg().optionalArg( true ).build();
        //        res.addOption( opt3 );
        //
        //        Option opt4 = new Option( "d", "debug", false, "debug log messages" );
        //        opt4.setRequired( false );
        //        res.addOption( opt4 );
        //
        //        Option opt5 = new Option( null, "p2d", true, "read '2D structure prediction from 1D' file and output matching score with prediction from 3D structure" );
        //        opt5.setRequired( false );
        //        res.addOption( opt5 );
        //
        //        Option opt6 = new Option( null, "warn", false, "enable warning messages" );
        //        opt6.setRequired( false );
        //        res.addOption( opt6 );

        return res;
    }

    private void parseArgs( String[] args ) throws Exception
    {
        logger.info( "parsing options" );

        //        System.out.println( "cmd line args : " + Arrays.asList( args ) );

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( getOptions(), args, false );

        //        logDebug = cmd.hasOption( 'd' );
        //        logWarning = cmd.hasOption( "warn" );
        //
        //        if ( cmd.hasOption( "p2d" ) )
        //            pred2dfrom1dFilename = cmd.getOptionValue( "p2d" );
        //
        //        if ( cmd.hasOption( 's' ) )
        //            templateName = cmd.getOptionValue( 's' );
        //
        //        hasScwrlBase = false;
        //        if ( cmd.hasOption( 'w' ) )
        //        {
        //            generateSCWRL = true;
        //            scwrlBase = cmd.getOptionValue( 'w' );
        //            hasScwrlBase = scwrlBase != null;
        //        }
        //
        //        if ( cmd.hasOption( 't' ) )
        //            targetName = cmd.getOptionValue( 't' );
        //
        //        //        System.out.println( "cmd.getArgList() : " + cmd.getArgList() );
        //        //        System.out.println( "cmd.getOptions() : " + Arrays.asList( cmd.getOptions() ) );
        //
        //        if ( cmd.getArgList().size() > 1 )
        //            throw new VitoException( "invalid extra arguments " + cmd.getArgList() );
        //
        //        if ( cmd.getArgList().size() == 0 )
        //            throw new VitoException( "missing arguments" );

        if ( cmd.getArgList().size() == 0 )
            throw new Exception( "missing arguments" );

        propertiesPath = cmd.getArgList().get( 0 );
    }
}
