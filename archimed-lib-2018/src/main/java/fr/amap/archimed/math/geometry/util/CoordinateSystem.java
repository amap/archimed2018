/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.math.geometry.util;

/**
 *
 * @author Julien Heurtebize
 */
public enum CoordinateSystem
{
    /**
     * North to east, clockwise.
     */
    COMPASS( (short)0 ),
    /**
     * Trigonometric. East to north, anticlockwise.
     */
    TRIGO( (short)1 ),
    /**
     * North to west, anticlockwise.
     */
    NORTH_TRIGO( (short)2 );

    private final short mode;

    private CoordinateSystem( short mode )
    {
        this.mode = mode;
    }
}
