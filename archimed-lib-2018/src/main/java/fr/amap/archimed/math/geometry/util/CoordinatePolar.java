package fr.amap.archimed.math.geometry.util;

import javax.vecmath.Point3d;

/**
 * <p>Defines polar coordinates and provides tools to work with it.</p>
 * @author Jean DAUZAT, Julien Heurtebize (refactoring)
 *
 */
public class CoordinatePolar
{
    public double zenith; // radians

    public double azimut; // radians

    public CoordinatePolar()
    {
        zenith = 99999;
        azimut = 99999;
    }

    /**
     * @param zenith [radian] zenith angle
     * @param azimut [radian] azimut angle clockwise from Y axis
     */
    public CoordinatePolar( double zenith, double azimut )
    {
        this.zenith = zenith;
        this.azimut = azimut;
    }

    /**
     * @param x x coordinate in polar
     * @param y y coordinate in polar
     * @param convention Output coordinate system.
     *
     * @return azimut in radians, clockwise from Y axis
     */
    public static double getAzimutFromXY( float x, float y, CoordinateSystem convention )
    {
        if ( (x == 0) && (y == 0) )
            return 0;

        if ( convention == null )
            return Double.NaN;

        double azimuth;

        switch ( convention )
        {
            case TRIGO:
                azimuth = Math.atan( y / x );
                if ( x < 0. )
                    azimuth += Math.PI;
                else if ( y < 0. )
                    azimuth += Math.PI * 2;
                break;

            case NORTH_TRIGO:
                azimuth = Math.atan( y / -x );
                if ( y < 0. )
                    azimuth += Math.PI;
                else if ( -x < 0. )
                    azimuth += Math.PI * 2;
                break;

            case COMPASS:
            default:
                azimuth = Math.atan( x / y );
                if ( y < 0. )
                    azimuth += Math.PI;
                else if ( x < 0. )
                    azimuth += Math.PI * 2;
                break;
        }

        return azimuth;
    }

    /**
     * @param azimut in radians
     * @param zenith in radians
     * @return
     */
    public static Point3d polarToPoint3d( double zenith, double azimut )
    {
        double sinZen = Math.sin( zenith );

        Point3d direction = new Point3d();

        direction.x = Math.sin( azimut ) * sinZen;
        direction.y = Math.cos( azimut ) * sinZen;
        direction.z = Math.cos( zenith );

        return direction;
    }

    public Point3d polarToPoint3d()
    {
        double sinZen = Math.sin( zenith );

        Point3d direction = new Point3d();

        direction.x = Math.sin( azimut ) * sinZen;
        direction.y = Math.cos( azimut ) * sinZen;
        direction.z = Math.cos( zenith );

        return direction;
    }
}
