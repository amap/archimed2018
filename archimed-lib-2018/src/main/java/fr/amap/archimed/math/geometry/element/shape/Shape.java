package fr.amap.archimed.math.geometry.element.shape;

import fr.amap.archimed.math.geometry.Intersectable;
import fr.amap.archimed.math.geometry.Transformable;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * Interface for simple geometry.
 *
 * @author Cresson/Dauzat, August 2012, J. Heurtebize (refactoring)
 */
public interface Shape extends Transformable, Intersectable
{
    public boolean isFinite();

    public BoundingBox3f computeBoundingBox();
}
