/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.math.geometry;

/**
 * Used to manage the intersections computation
 * @author Remi Cresson, J. Heurtebize (refactoring)
 */
public class IntersectionContext {
    
    public boolean intersect;
    public float length;

    public IntersectionContext(boolean intersect, float length) {
        this.intersect = intersect;
        this.length = length;
    }
}
