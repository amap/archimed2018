/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.amap.archimed.math.geometry;

import java.util.ArrayList;

import fr.amap.archimed.math.geometry.element.line.LineElement;

/**
 *
 * @author Julien Heurtebize
 */
public interface Intersectable
{

    /**
     * Check if the shape is intersected by a line element. Can be faster than
     * getNearestIntersection when the intersection isn't needed.
     * @param linel
     * @return 
     */
    public boolean isIntersectedBy( LineElement linel );

    /**
     * Get all intersections between the given linel and the shape
     *
     * @param linel
     * @return
     */
    public ArrayList<Intersection> getIntersections( LineElement linel );

    /**
     * Get the nearest intersection between the given linel and the shape
     * @param linel
     * @return 
     */
    public Intersection getNearestIntersection( LineElement linel );
}
