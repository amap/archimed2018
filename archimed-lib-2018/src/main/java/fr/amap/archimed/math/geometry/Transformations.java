package fr.amap.archimed.math.geometry;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point4f;
import javax.vecmath.Tuple3f;
import javax.vecmath.Tuple4f;
import javax.vecmath.Vector3f;

/**
 * Construct a 4x4 transformation matrix combining successive transformations
 * (rotations, translations and ISOTROPIC scaling).
 * <li>The "apply" method applies the transformation to the tuple in argument.
 *
 * @author Dauzat -August 2012
 */
public class Transformations
{
    /**
     * transformation matrix
     */
    private Matrix4f mat;

    /**
     * used to avoid allocations (warning, not thread safe !)
     */
    private static Matrix3f tmp3 = new Matrix3f();

    /**
     * used to avoid allocations
     */
    private Matrix4f tmp4;

    /**
     * Single precision floating transforms
     */
    public Transformations()
    {
        mat = new Matrix4f();
        mat.setIdentity();

        tmp4 = new Matrix4f();
    }

    public Transformations( Vector3f translation )
    {
        this();
        setTranslation( translation );
    }

    /**
     * Set a counter clockwise rotation of tuple around the X axis
     */
    public void setRotationAroundX( float angle )
    {
        tmp4.setZero();
        tmp4.rotX( angle );
        mat.mul( tmp4, mat );
    }

    /**
     * Set a counter clockwise rotation of tuple around the Y axis
     */
    public void setRotationAroundY( float angle )
    {
        tmp4.setZero();
        tmp4.rotY( angle );
        mat.mul( tmp4, mat );
    }

    /**
     * Set a counter clockwise rotation of tuple around the Z axis
     */
    public void setRotationAroundZ( float angle )
    {
        tmp4.setZero();
        tmp4.rotZ( angle );
        mat.mul( tmp4, mat );
    }

    /**
     * Set a counter clockwise rotation of tuple around the specified axis
     */
    public void setRotationAroundAxis( Vector3f rotAxis, float angle )
    {
        AxisAngle4f axisAngle = new AxisAngle4f( rotAxis, angle );
        tmp4.setZero();
        tmp4.set( axisAngle );
        mat.mul( tmp4, mat );
    }

    /**
     * Set a translation
     */
    public void setTranslation( Vector3f translation )
    {
        tmp4.setIdentity();
        tmp4.setTranslation( translation );
        mat.mul( tmp4, mat );
    }

    /**
     * Set a scaling factor
     */
    public void setScale( float scale )
    {
        tmp4.setIdentity();
        tmp4.setIdentity();
        tmp4.setScale( scale );
        mat.mul( tmp4, mat );
    }

    /**
     * Set transformation matrix
     */
    public void setMatrix( Matrix4f matrix )
    {
        mat = matrix;
    }

    /**
     * Applies the transformations to the tuple
     *
     * @param tuple (Point3f or Vector3f) to transform
     */
    public void apply( Tuple3f tuple )
    {
        Tuple4f t = new Point4f( tuple.x, tuple.y, tuple.z, 1 );
        mat.transform( t );
        tuple.x = t.x;
        tuple.y = t.y;
        tuple.z = t.z;

    }

    public Matrix4f getMatrix()
    {
        return mat;
    }

    //==================== statics methods ====================//

    /**
     * Applies a counter clockwise rotation of tuple around the X axis
     */
    public static void rotateAroundX( Tuple3f tuple, float angle )
    {
        tmp3.setZero();
        tmp3.rotX( angle );
        tmp3.transform( tuple );
    }

    /**
     * Applies a counter clockwise rotation of tuple around the Y axis.
     */
    public static void rotateAroundY( Tuple3f tuple, float angle )
    {
        tmp3.setZero();
        tmp3.rotY( angle );
        tmp3.transform( tuple );
    }

    /**
     * Applies a counter clockwise rotation of tuple around the Z axis
     */
    public static void rotateAroundZ( Tuple3f tuple, float angle )
    {
        tmp3.setZero();
        tmp3.rotZ( angle );
        tmp3.transform( tuple );
    }

    /**
     * Applies a counter clockwise rotation of tuple around the specified axis
     */
    public static void rotateAroundAxis( Tuple3f tuple, Vector3f axis, float angle )
    {
        tmp3.setZero();
        AxisAngle4f axisAngle = new AxisAngle4f( axis, angle );
        tmp3.set( axisAngle );
        tmp3.transform( tuple );
    }

    /**
     * Applies a translation on the tuple
     */
    public static void translate( Tuple3f tuple, Tuple3f translation )
    {
        tuple.x += translation.x;
        tuple.y += translation.y;
        tuple.z += translation.z;
    }

    /**
     * Applies a scalar multiplication to tuple coordinates
     */
    public static void scale( Tuple3f tuple, float scale )
    {
        tuple.scale( scale );
    }
}
