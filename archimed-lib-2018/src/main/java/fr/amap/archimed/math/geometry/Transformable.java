package fr.amap.archimed.math.geometry;

/**
 * Interface for geometric transformations (for shapes, LineSegments, ...)
 *
 * @author Dauzat/Cresson, Oct. 2012
 */
public interface Transformable {

    public abstract void transform(Transformations transform);
}
