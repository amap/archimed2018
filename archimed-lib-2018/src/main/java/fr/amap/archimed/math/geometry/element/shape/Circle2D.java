package fr.amap.archimed.math.geometry.element.shape;

import javax.vecmath.Point2f;

/**
 * Circles intersections.
 *
 * @author J. Dauzat - May 2012
 */
public class Circle2D implements Cloneable
{
    private final float radius;

    private final Point2f center;

    /**
     * Constructor
     * @param radius
     * @param center
     */
    public Circle2D( float radius, Point2f center )
    {
        this.radius = radius;
        this.center = new Point2f( center );
    }

    /**
     * Calculates the area of the intersection (lumen) with circle2
     * (http://mathworld.wolfram.com/Circle-CircleIntersection.html)
     *
     * @param circle2
     * @return lumen area
     */
    public float intersectionCircleArea( Circle2D circle2 )
    {

        float d = center.distance( circle2.center );
        return lumenArea( d, this.radius, circle2.radius );
    }

    /**
     * Calculates the area of the intersection (lumen) between 2 circles
     * (http://mathworld.wolfram.com/Circle-CircleIntersection.html)
     *
     * @param centersDistance: distance between centers of circles
     * @param radiusCircle1: radius of first circle
     * @param radiusCircle2: radius of second circle
     * @return lumen area
     */
    public static float lumenArea( float centersDistance, float radiusCircle1, float radiusCircle2 )
    {
        double lumenArea = 0;

        // no intersection
        if ( centersDistance >= radiusCircle2 + radiusCircle1 )
        {
            return (float)lumenArea;
        }

        // simplified equation in that case
        if ( radiusCircle1 == radiusCircle2 )
        {
            double halfDist = centersDistance / 2.0;
            double h = (radiusCircle2 * radiusCircle2) - (halfDist * halfDist);
            h = Math.sqrt( h );
            double alpha = Math.acos( halfDist / radiusCircle2 );
            lumenArea = (float)(2 * ((alpha * (radiusCircle2 * radiusCircle2)) - (h * halfDist)));

            return (float)lumenArea;
        }

        // general case
        double d2 = centersDistance * centersDistance;
        double R2 = radiusCircle1 * radiusCircle1;
        double r2 = radiusCircle2 * radiusCircle2;

        // cases of one circle included in the second one
        if ( centersDistance + radiusCircle2 < radiusCircle1 )
        {
            return (float)(Math.PI * radiusCircle2 * radiusCircle2);
        }
        if ( centersDistance + radiusCircle1 < radiusCircle2 )
        {
            return (float)(Math.PI * radiusCircle1 * radiusCircle1);
        }
        // other cases
        lumenArea = (r2 * Math.acos( (d2 + r2 - R2) / (2 * centersDistance * radiusCircle2) )) + (R2 * Math.acos( (d2 + R2 - r2) / (2 * centersDistance * radiusCircle1) )) - (Math.sqrt( (-centersDistance + radiusCircle2 + radiusCircle1) * (centersDistance + radiusCircle2 - radiusCircle1) * (centersDistance - radiusCircle2 + radiusCircle1) * (centersDistance + radiusCircle2 + radiusCircle1) ) / 2);

        return (float)lumenArea;
    }
}
