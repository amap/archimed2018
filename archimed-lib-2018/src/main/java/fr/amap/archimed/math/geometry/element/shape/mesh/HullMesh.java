package fr.amap.archimed.math.geometry.element.shape.mesh;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.vecmath.Point3f;

import jeeb.lib.structure.ArchiNode;
import jeeb.lib.structure.geometry.mesh.Mesh;
import jeeb.lib.structure.geometry.mesh.convexhull.ConvexHull3D;
import jeeb.lib.structure.geometry.mesh.convexhull.SpatialPoint;

/**
 * Computes the convex hull ({@link ConvexHull3D}) of {@link Point3f} list or
 * {@link ArchiNode}
 *
 * @author Dauzat - August 2012, Julien Heurtebize (refactoring)
 */
public class HullMesh
{
    // found to give shorter computations, maximum number of points to perform the hull
    private final static int NB_MAX_POINTS = 2000;

    /**
     * Computes the convex hull ({@link ConvexHull3D}) of a {@link Point3f}
     * List.
     * Because the algorithm doesn't work for too large point lists, several
     * hulls are calculated for sublists if needed and then merged
     *
     * @param points
     * @return Mesh ({@link Mesh})
     */
    public static Mesh getMesh( List<Point3f> points )
    {
        int nbMeshes = (int)Math.ceil( points.size() / (float)NB_MAX_POINTS );

        List<Point3f> meshesPoints = new ArrayList<>();

        int nbPointsAdded = 0;

        for ( int m = 0; m < nbMeshes; m++ )
        {
            List<Point3f> subListPoints = new ArrayList<>();

            for ( int p = 0; p < points.size(); p++ )
            {
                subListPoints.add( points.get( p ) );
                nbPointsAdded++;

                if ( nbPointsAdded == NB_MAX_POINTS )
                {
                    nbPointsAdded = 0;
                    break;
                }
            }

            Point3f[] pts = getHull( subListPoints ).getPoints();

            meshesPoints.addAll( Arrays.asList( pts ) );
        }

        Mesh mesh = getHull( meshesPoints );

        return mesh;
    }

    /**
     * Computes the convex hull ({@link ConvexHull3D}) of a {@link Point3f}
     * List.
     * Because the algorithm doesn't work for too large point lists, several
     * hulls are calculated for sublists if needed and then merged
     *
     * @param points
     * @return Mesh ({@link Mesh})
     */
    public static Mesh getMesh( Point3f[] points )
    {
        return getMesh( Arrays.asList( points ) );
    }

    public static Mesh getHull( List<Point3f> points )
    {
        // creation of ptArray
        double[] ptArray = new double[ points.size() * 3 ];
        for ( int p = 0; p < points.size(); p++ )
        {
            int c = p * 3;
            ptArray[ c ] = points.get( p ).x;
            ptArray[ c + 1 ] = points.get( p ).y;
            ptArray[ c + 2 ] = points.get( p ).z;
        }

        ConvexHull3D cvHull = new ConvexHull3D( ptArray );
        int[][] faces;
        faces = cvHull.getHullFaces();
        SpatialPoint[] spts = cvHull.getHullFaceVertices();
        Point3f[] pts = new Point3f[ spts.length ];
        for ( int p = 0; p < spts.length; p++ )
        {
            pts[ p ] = new Point3f( (float)spts[ p ].x, (float)spts[ p ].y, (float)spts[ p ].z );
        }

        Mesh mesh = new Mesh( pts, faces );
        mesh.computeNormals();
        mesh.reverseNormals();

        return mesh;
    }
}
