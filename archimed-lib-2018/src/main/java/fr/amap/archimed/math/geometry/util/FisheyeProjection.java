package fr.amap.archimed.math.geometry.util;

import javax.vecmath.Point2f;
import javax.vecmath.Vector3f;

/**
 * <p>Azimuthal equidistant projection. Polar projection where the distance to center
 * is proportional to the zenith angle instead of proportional to its sinus</p>
 * Reference : <a href="https://en.wikipedia.org/wiki/Azimuthal_equidistant_projection">https://en.wikipedia.org/wiki/Azimuthal_equidistant_projection</a>
 *
 * @author DAUZAT, Julien Heurtebize (refactoring)
 */
public class FisheyeProjection
{
    /**
     * Normalized direction in polar coordinates from a point mapped with equidistant projection.
     * @param proj Projected point position.
     * @param center Center of the projection area.
     * @param radius Radius of the projection area.
     * @return A direction.
     */
    public static Vector3f fromProjToDir( Point2f proj, Point2f center, float radius )
    {
        Vector3f direction = new Vector3f();
        direction.x = (proj.x - center.x) / radius;
        direction.y = (proj.y - center.y) / radius;

        float hypot = (direction.x * direction.x) + (direction.y * direction.y);

        if ( hypot <= 1 )
        {

            hypot = (float)Math.sqrt( hypot );

            if ( hypot > 0f )
            {
                direction.x *= Math.sin( hypot * Math.PI / 2.0 ) / hypot;
                direction.y *= Math.sin( hypot * Math.PI / 2.0 ) / hypot;
            }

            direction.z = (float)Math.cos( hypot * Math.PI / 2.0 );
        }

        direction.normalize();

        return direction;
    }

    /**
     * Equidistant projection of a normalized direction.
     *
     * @param	direction
     * @return	projected point
     */
    public static Point2f fromDirToProj( Vector3f direction )
    {
        Point2f projection = new Point2f();
        float zenith = (float)Math.acos( direction.getZ() );

        if ( zenith == 0 )
        {
            return projection;
        }

        float coeff = (float)(zenith / (Math.PI / 2));
        coeff /= Math.sin( zenith );
        projection.x = direction.getX() * coeff;
        projection.y = direction.getY() * coeff;

        return projection;
    }
}
