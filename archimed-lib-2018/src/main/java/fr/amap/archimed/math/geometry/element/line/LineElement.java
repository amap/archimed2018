package fr.amap.archimed.math.geometry.element.line;

import java.util.ArrayList;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Intersection;
import fr.amap.archimed.math.geometry.element.shape.Shape;

/**
 * Interface for handling intersections between line elements (line, half-line
 * or line-segment) with "Shape" objects (polygon, sphere, mesh, ...)
 *
 * @author Dauzat/Cresson - August 2012
 */
public interface LineElement
{

    public Point3f getOrigin();

    public Point3f getEnd();

    public Vector3f getDirection();

    public float getLength();

    /**
     * @param shape
     * @return 
     */
    public boolean doesIntersect( Shape shape );

    /**
     * @param shape
     * @return a List of intersections (empty list if no intersection)
     */
    public ArrayList<Intersection> getIntersections( Shape shape );

    /**
     * @param shape
     * @return the nearest intersection from the point "origin" in the line
     * element direction
     */
    public Intersection getNearestIntersection( Shape shape );

    /**
     * Translate the line element
     *
     * @param translation	translation vector
     */
    public void translate( Vector3f translation );
}
