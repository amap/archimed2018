package fr.amap.archimed.math.geometry.element.shape;

import java.util.ArrayList;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Intersection;
import fr.amap.archimed.math.geometry.Transformations;
import fr.amap.archimed.math.geometry.element.line.HalfLine;
import fr.amap.archimed.math.geometry.element.line.Line;
import fr.amap.archimed.math.geometry.element.line.LineElement;
import fr.amap.archimed.math.geometry.element.line.LineSegment;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * Triangle class
 *
 * @author cresson, august 2012, J. Heurtebize (refactoring)
 */

public class Triangle implements Shape
{
    public static final float DEFAULT_FAR = -Float.MAX_VALUE;

    private Point3f[] vertices;

    private Vector3f normal;

    private boolean culling;

    //compute normal at runtime, lazy initialization
    private boolean normalComputed;

    public Triangle( Point3f point1, Point3f point2, Point3f point3 )
    {
        this( point1, point2, point3, false );
    }

    public Triangle( Point3f point1, Point3f point2, Point3f point3, boolean culling )
    {
        this.vertices = new Point3f[] { point1, point2, point3 };
        this.culling = culling;
    }

    @Override
    public boolean isFinite()
    {
        return true;
    }

    @Override
    public BoundingBox3f computeBoundingBox()
    {
        Point3f vertex1 = vertices[ 0 ];
        Point3f vertex2 = vertices[ 1 ];
        Point3f vertex3 = vertices[ 2 ];

        float xMin = vertex1.x;
        float yMin = vertex1.y;
        float zMin = vertex1.z;

        float xMax = vertex1.x;
        float yMax = vertex1.y;
        float zMax = vertex1.z;

        xMin = Float.min( xMin, vertex2.x );
        yMin = Float.min( yMin, vertex2.y );
        zMin = Float.min( zMin, vertex2.z );

        xMax = Float.max( xMax, vertex2.x );
        yMax = Float.max( yMax, vertex2.y );
        zMax = Float.max( zMax, vertex2.z );

        xMin = Float.min( xMin, vertex3.x );
        yMin = Float.min( yMin, vertex3.y );
        zMin = Float.min( zMin, vertex3.z );

        xMax = Float.max( xMax, vertex3.x );
        yMax = Float.max( yMax, vertex3.y );
        zMax = Float.max( zMax, vertex3.z );

        return new BoundingBox3f( new Point3f( xMin, yMin, zMin ), new Point3f( xMax, yMax, zMax ) );
    }

    private class IntersectionContext
    {
        public boolean intersect;

        public float length;

        public IntersectionContext( boolean intersect, float length )
        {
            this.intersect = intersect;
            this.length = length;
        }
    }

    /**
     * Manage intersectionContext to return a correct Intersection (depending on
     * instance of lineElement, e.g. test for segment is different from test for
     * half-line)      *
     * @param	lineElement	line element
     * @param	allowNegativeLengths	allow intersection point to be back to the
     * line element origin
     * @return	computed intersection (null if empty)
     */
    private Intersection computeIntersection( LineElement lineElement, boolean allowNegativeLengths )
    {
        IntersectionContext intersectionContext = computeIntersection( lineElement.getDirection(), lineElement.getOrigin(), culling, allowNegativeLengths );
        if ( intersectionContext.intersect )
        {
            float distance = intersectionContext.length;
            //boolean side = linel.getDirection ().dot (normal)>0;
            //return new Intersection(point,normal,side);
            if ( lineElement instanceof LineSegment )
            {
                if ( lineElement.getLength() < intersectionContext.length )
                {
                    return null;
                }
            }
            return new Intersection( distance, getNormal() );
        }
        return null;
    }

    /**
     * Computes normal of the triangle
     */
    private void computeNormal()
    {
        Vector3f vertex1 = new Vector3f( vertices[ 1 ].x - vertices[ 0 ].x, vertices[ 1 ].y - vertices[ 0 ].y, vertices[ 1 ].z - vertices[ 0 ].z );
        Vector3f vertex2 = new Vector3f( vertices[ 2 ].x - vertices[ 1 ].x, vertices[ 2 ].y - vertices[ 1 ].y, vertices[ 2 ].z - vertices[ 1 ].z );
        this.normal = new Vector3f();
        normal.cross( vertex1, vertex2 );
        normal.normalize();
    }

    /**
     * Computes intersection Triangle-Line.
     *
     * @param	direction	direction of the line element
     * @param	origin	origin of the line element
     * @param	cullingDesired	true if result depends of triangle orientation
     * (i.e. if triangle is back, there is no intersection). Faster code.
     * @param	allowNegativeLengths	true if result depends of line orientation
     * (i.e. allow intersection point to be back to the line element origin).
     * @return	IntersectionContext (length: distance between ray origin and ray
     * impact, boolean: true if there is an intersection, false if not)
     */
    private IntersectionContext computeIntersection( Vector3f direction, Point3f origin, boolean cullingDesired, boolean allowNegativeLengths )
    {
        // *************************************************************************************
        // Following code inspired by the paper :
        // "Fast, Minimum Storage Ray/Triangle intersection" from Thomas Möller and Ben Trumbore
        // (Prosolvia Clarus AB, Chalmers University of Technology / 
        //  Program of Computer Graphics, Cornell University)
        //
        // Link: http://www.graphics.cornell.edu/pubs/1997/MT97.html
        // *************************************************************************************

        // default length
        float length = DEFAULT_FAR;

        // find vectors for two edges sharing vertex0
        Vector3f edge1 = new Vector3f( vertices[ 1 ].x - vertices[ 0 ].x, vertices[ 1 ].y - vertices[ 0 ].y, vertices[ 1 ].z - vertices[ 0 ].z );
        Vector3f edge2 = new Vector3f( vertices[ 2 ].x - vertices[ 0 ].x, vertices[ 2 ].y - vertices[ 0 ].y, vertices[ 2 ].z - vertices[ 0 ].z );

        // begin computing determinant - also used to calculate u
        Vector3f pvec = new Vector3f();
        pvec.cross( direction, edge2 );

        // if determinant is zero, ray lies in plane of triangle
        float det = edge1.dot( pvec );
        if ( cullingDesired == true )
        { // if culling is desired
            if ( det < 0 )
                return new IntersectionContext( false, length );

            // calculate distance from vert0 to ray origin
            Vector3f tvec = new Vector3f();
            tvec.sub( origin, vertices[ 0 ] );

            // calculate u and test bounds
            float u = tvec.dot( pvec );
            if ( u < 0 || u > det )
                return new IntersectionContext( false, length );

            // prepare to test v
            Vector3f qvec = new Vector3f();
            qvec.cross( tvec, edge1 );

            // computing v and test bounds
            float v = qvec.dot( direction );
            if ( v < 0 || u + v > det )
                return new IntersectionContext( false, length );

            // calculate length, ray intersects triangle
            length = edge2.dot( qvec );
            length /= det;
        }
        else
        { // the non-culling branch
            if ( det == 0 )
                return new IntersectionContext( false, length );

            // invert of determinant
            float inv_det = 1.0f / det;

            // calculate distance from vert0 to ray origin
            Vector3f tvec = new Vector3f();
            tvec.sub( origin, vertices[ 0 ] );

            // calculate u and test bounds
            float u = tvec.dot( pvec );
            u *= inv_det;
            if ( u < 0.0f || u > 1.0f )
                return new IntersectionContext( false, length );

            // prepare to test v
            Vector3f qvec = new Vector3f();
            qvec.cross( tvec, edge1 );

            // calculate v and test bounds
            float v = qvec.dot( direction );
            v *= inv_det;
            if ( v < 0.0f || u + v > 1.0f )
                return new IntersectionContext( false, length );

            // calculate length, ray intersects triangle
            length = edge2.dot( qvec );
            length *= inv_det;
        }

        if ( allowNegativeLengths == true )
            return new IntersectionContext( true, length );

        if ( length > 0.0f )
            return new IntersectionContext( true, length );

        return new IntersectionContext( false, length );
    }

    /**
     * Tests if triangle is intersected by the specified line element
     *
     * @param	lineElement	line element
     * @return	true if triangle is intersected by the line element. false if
     * not.
     */
    @Override
    public boolean isIntersectedBy( LineElement lineElement )
    {
        if ( lineElement instanceof Line )
        {
            // allow negative distances
            return computeIntersection( lineElement.getDirection(), lineElement.getOrigin(), culling, true ).intersect;
        }
        else if ( lineElement instanceof HalfLine )
        {
            // dont allow negative distances
            return computeIntersection( lineElement.getDirection(), lineElement.getOrigin(), culling, false ).intersect;
        }
        // dont allow negative distance and compare length vs distance
        IntersectionContext intersectionContext = computeIntersection( lineElement.getDirection(), lineElement.getOrigin(), culling, false );

        return lineElement.getLength() > intersectionContext.length && intersectionContext.intersect == true;
    }

    /**
     * Get triangle intersections with the specified line element
     *
     * @param	lineElement	line element
     * @return	ArrayList of Intersection. (faked, because Triangles's maximum
     * intersection number never > 1). Use getNearestIntersection instead.
     * @see	getNearestIntersection
     */
    @Override
    public ArrayList<Intersection> getIntersections( LineElement lineElement )
    {
        ArrayList<Intersection> array = new ArrayList<>();
        array.add( computeIntersection( lineElement, true ) );
        return array;
    }

    /**
     * Get the nearest Intersection with the specified line element
     *
     * @param lineElement line element
     * @return The nearest intersection.
     */
    @Override
    public Intersection getNearestIntersection( LineElement lineElement )
    {
        if ( lineElement instanceof LineSegment || lineElement instanceof HalfLine )
        {
            // dont allow negative distances
            return computeIntersection( lineElement, false );
        }
        // allow negative distances
        return computeIntersection( lineElement, true );
    }

    /**
     * transform the shape's position
     *
     * @param transform	transformation
     */
    @Override
    public void transform( Transformations transform )
    {
        for ( int v = 0; v < 3; v++ )
            transform.apply( vertices[ v ] );
        computeNormal();
    }

    /**
     * get the barycentre of the triangle
     *
     * @return barycentre
     */
    public Point3f getBarycentre()
    {
        Point3f barycentre = new Point3f( 0f, 0f, 0f );
        for ( int v = 2; v >= 0; v-- )
            barycentre.add( vertices[ v ] );
        barycentre.scale( 1.0f / 3.0f );
        return barycentre;
    }

    /**
     * Get the normal vector of the triangle, computes it if not initialized.
     * @return 
     */
    public Vector3f getNormal()
    {
        if ( !normalComputed )
        {
            computeNormal();
            normalComputed = true;
        }
        return normal;
    }

    public void setNormal( Vector3f normal )
    {
        this.normal = normal;
        normalComputed = true;
    }

    public Point3f[] getVertices()
    {
        return vertices;
    }

    public Point3f getVertex1()
    {
        return vertices[ 0 ];
    }

    public Point3f getVertex2()
    {
        return vertices[ 1 ];
    }

    public Point3f getVertex3()
    {
        return vertices[ 2 ];
    }

    public void setVertex1( Point3f point )
    {
        vertices[ 0 ] = point;
    }

    public void setVertex2( Point3f point )
    {
        vertices[ 1 ] = point;
    }

    public void setVertex3( Point3f point )
    {
        vertices[ 2 ] = point;
    }

    /**
     * Compute triangle area by cross product method.
     * If triangle is defined by A,B,C vertices, S = norm( cross(AB, AC) ) / 2
     */
    public float computeArea()
    {
        // vertices[] : A B C

        // AB vector
        Vector3f ab = new Vector3f( vertices[ 1 ] );
        ab.sub( vertices[ 0 ] );

        // AC vector
        Vector3f ac = new Vector3f( vertices[ 2 ] );
        ac.sub( vertices[ 0 ] );

        // cross(AB, AC)
        Vector3f s = new Vector3f();
        s.cross( ab, ac );

        // S = norm( cross(AB, AC) ) / 2
        return s.length() / 2;
    }
}
