package fr.amap.archimed.math.geometry.util;

import static fr.amap.archimed.math.geometry.util.CoordinateSystem.COMPASS;
import static fr.amap.archimed.math.geometry.util.CoordinateSystem.TRIGO;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import org.apache.commons.math3.util.MathUtils;

/**
 * Converts from polar to cartesian coordinates or from cartesian coordinates to
 * polar coordinates.
 * Rotation is clockwise direction.
 *
 * @author dauzat
 */
public class CoordinatesConversion
{
    /**
     * Converts polar to cartesian coordinates.
     * North trigo coordinate system.
     *
     * @param zenith zenith angle (radians)
     * @param azimut azimut angle (radians) in trigo convention
     * @param coordinateSystem input coordinate system
     * @return cartesian direction
     */
    public static Vector3f polarToCartesian( double zenith, double azimut, CoordinateSystem coordinateSystem )
    {
        Point3f dir = new Point3f();

        dir.z = (float)Math.cos( zenith );

        switch ( coordinateSystem )
        {
            case NORTH_TRIGO:
                dir.x = (float)(Math.sin( zenith ) * -Math.sin( azimut ));
                dir.y = (float)(Math.sin( zenith ) * Math.cos( azimut ));
                break;
            case TRIGO:
                dir.x = (float)(Math.sin( zenith ) * Math.cos( azimut ));
                dir.y = (float)(Math.sin( zenith ) * Math.sin( azimut ));
                break;
            case COMPASS:
                dir.x = (float)-(Math.sin( zenith ) * -Math.sin( azimut ));
                dir.y = (float)(Math.sin( zenith ) * Math.cos( azimut ));
                break;
            default:
                throw new UnsupportedOperationException( "The coordinate system conversion is not implemented yet." );
        }

        Vector3f direction = new Vector3f( dir );
        return direction;
    }

    //    public static void main( String[] args )
    //    {
    //        for ( int i = 0; i <= 360; i++ )
    //        {
    //            Vector3f polarToCartesian = CoordinatesConversion.polarToCartesian( Math.PI / 2.0, Math.toRadians( i ), CoordinateSystem.NORTH_TRIGO );
    //            System.out.println( i + "\t" + polarToCartesian.toString() );
    //
    //            Point2f cartesianToPolar = CoordinatesConversion.cartesianToPolar( polarToCartesian );
    //            System.out.println( polarToCartesian.toString() + "\t" + Math.toDegrees( cartesianToPolar.x ) + " " + Math.toDegrees( cartesianToPolar.y ) );
    //        }
    //
    //        System.out.println( "test" );
    //    }

    /**
     * Converts cartesian to polar coordinates.
     * Compass coordinate system.
     *
     * @param normDir normalized direction
     * @return zenith and azimuth in radians
     */
    public static Point2f cartesianToPolar( Vector3f normDir )
    {
        float zenith = (float)Math.acos( normDir.z );
        Vector2f projection = new Vector2f( normDir.x, normDir.y );
        projection.normalize();
        float azimut = projection.angle( new Vector2f( 0, 1 ) );

        return new Point2f( zenith, azimut );
    }

    public static double convertAzimut( double azimut, CoordinateSystem inputSystem, CoordinateSystem outputSystem )
    {
        double convertedAzimut;

        if ( inputSystem == TRIGO )
        {
            if ( outputSystem == COMPASS )
            {

                convertedAzimut = Math.PI / 2.0 - azimut;

                convertedAzimut = MathUtils.normalizeAngle( convertedAzimut, Math.PI );

                return convertedAzimut;
            }
            else if ( outputSystem == TRIGO )
            {
                return azimut;
            }
            else
            {
                throw new UnsupportedOperationException( "The coordinate system conversion is not implemented yet." );
            }

        }
        else if ( inputSystem == COMPASS )
        {
            if ( outputSystem == TRIGO )
            {
                convertedAzimut = -azimut + Math.PI / 2.0;
                convertedAzimut = MathUtils.normalizeAngle( convertedAzimut, Math.PI );
                return convertedAzimut;
            }
            else if ( outputSystem == COMPASS )
            {
                return azimut;
            }
            else
            {
                throw new UnsupportedOperationException( "The coordinate system conversion is not implemented yet." );
            }
        }
        else
        {
            throw new UnsupportedOperationException( "The coordinate system conversion is not implemented yet." );
        }
    }
}
