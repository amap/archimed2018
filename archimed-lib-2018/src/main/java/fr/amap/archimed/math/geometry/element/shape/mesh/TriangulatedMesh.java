package fr.amap.archimed.math.geometry.element.shape.mesh;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import fr.amap.archimed.math.geometry.Intersection;
import fr.amap.archimed.math.geometry.IntersectionContext;
import fr.amap.archimed.math.geometry.Transformations;
import fr.amap.archimed.math.geometry.element.line.HalfLine;
import fr.amap.archimed.math.geometry.element.line.Line;
import fr.amap.archimed.math.geometry.element.line.LineElement;
import fr.amap.archimed.math.geometry.element.line.LineSegment;
import fr.amap.archimed.math.geometry.element.shape.Shape;
import fr.amap.archimed.math.geometry.element.shape.Triangle;
import jeeb.lib.structure.geometry.mesh.SimpleMesh;
import jeeb.lib.structure.geometry.util.BoundingBox3f;

/**
 * Triangle-fashioned mesh
 *
 * @author Cresson, sept. 2012, J. Heurtebize (refactoring)
 */
public class TriangulatedMesh extends SimpleMesh implements Shape
{
    //    private int nbOftriangles;

    protected boolean culling;

    //    private boolean normalsInitialized;

    protected Vector3f[] triNormals;

    @Override
    public boolean isFinite()
    {
        return true;
    }

    /**
     * Constructor with points array and paths
     *
     * @param points	points array
     * @param paths	paths
     */
    public TriangulatedMesh( Point3f[] points, int[][] paths )
    {

        super.setPoints( points );
        super.setPaths( paths );

        computeTrianglesNormals();

        verify( this.points, this.paths, this.triNormals );
    }

    /**
     * Constructor with points array, paths and culling option
     *
     * @param points	points array
     * @param paths	paths
     * @param culling	culling opt. When sets to true, the intersection happens
     * only when face_normal[dot]ray >0
     */
    public TriangulatedMesh( Point3f[] points, int[][] paths, boolean culling )
    {
        super.setPoints( points );
        super.setPaths( paths );

        computeTrianglesNormals();

        verify( this.points, this.paths, this.triNormals );

        this.culling = culling;
    }

    /**
     * 
     * @param points
     * @param paths
     * @param normals vertices normales
     */
    public TriangulatedMesh( Point3f[] points, int[][] paths, Vector3f[] normals )
    {
        super.setPoints( points );
        super.setPaths( paths );
        super.setNormals( normals );

        computeTrianglesNormals();

        verify( this.points, this.paths, this.triNormals );
    }

    /**
     * 
     * @param points
     * @param paths
     * @param normals vertices normales
     * @param culling 
     */
    public TriangulatedMesh( Point3f[] points, int[][] paths, Vector3f[] normals, boolean culling )
    {
        super.setPoints( points );
        super.setPaths( paths );
        super.setNormals( normals );

        computeTrianglesNormals();

        verify( this.points, this.paths, this.triNormals );

        this.culling = culling;
    }

    /**
     * <p>Initialize a {@link TriangulatedMesh} with a {@link SimpleMesh}.</p>
     * This last one should be triangulated, otherwise an exception will be thrown.
     * @param mesh 
     */
    public TriangulatedMesh( SimpleMesh mesh )
    {
        super.setPoints( mesh.getPoints() );
        super.setPaths( mesh.getPaths() );
        super.setNormals( mesh.getNormals() );

        computeTrianglesNormals();

        verify( this.points, this.paths, this.triNormals );
    }

    private static void verify( Point3f[] points, int[][] paths, Vector3f[] normals )
    {
        if ( normals != null )
            if ( paths.length != normals.length )
                throw new IllegalArgumentException( "Normals array size is different from points array size, " + points.length + "/" + normals.length );

        for ( int[] path : paths )
        {
            if ( path.length > 3 )
                throw new IllegalArgumentException( "Paths should contains 3 indices, found : " + path.length );

            for ( int index : path )
                if ( index >= points.length )
                    throw new ArrayIndexOutOfBoundsException( "Face index is out of bounds, found : " + index );
        }
    }

    @Override
    public void transform( Transformations transform )
    {
        for ( int i = 0; i < points.length; i++ )
            transform.apply( points[ i ] );
    }

    @Override
    public boolean isIntersectedBy( LineElement linel )
    {
        for ( int i = 0; i < paths.length; i++ )
            if ( isIntersectedBy( i, linel ) )
                return true;

        return false;
    }

    @Override
    public ArrayList<Intersection> getIntersections( LineElement linel )
    {
        ArrayList<Intersection> intersections = new ArrayList<>();
        for ( int i = 0; i < paths.length; i++ )
        {
            Intersection intersection = getTriangleIntersection( i, linel );
            if ( intersection != null )
                intersections.add( intersection );
        }
        return intersections;
    }

    @Override
    public Intersection getNearestIntersection( LineElement linel )
    {
        Intersection nearestIntersection = null;
        float distance = Float.MAX_VALUE;
        for ( int i = 0; i < paths.length; i++ )
        {
            Intersection intersection = getTriangleIntersection( i, linel );
            if ( intersection != null )
            {
                float intersectionDistance = intersection.getDistance();
                if ( intersectionDistance < distance )
                {
                    distance = intersectionDistance;
                    nearestIntersection = intersection;
                }
            }
        }
        return nearestIntersection;
    }

    @Override
    public Object clone()
    {
        TriangulatedMesh tri = new TriangulatedMesh( (SimpleMesh)super.clone() );

        tri.culling = culling;
        tri.triNormals = new Vector3f[ triNormals.length ];
        for ( int i = 0; i < triNormals.length; i++ )
            tri.triNormals[ i ] = new Vector3f( triNormals[ i ] );

        tri.paths = new int[ paths.length ][ 3 ];
        for ( int i = 0; i < paths.length; i++ )
            for ( int j = 0; j < 3; j++ )
                tri.paths[ i ][ j ] = paths[ i ][ j ];

        tri.points = new Point3f[ points.length ];
        for ( int i = 0; i < points.length; i++ )
            tri.points[ i ] = new Point3f( points[ i ] );

        // on renvoie le clone
        return tri;
    }

    //////////////////////////////////// TRIANGLE ROUTINES //////////////////////////////////////
    /*
     * Computes normal of all triangles
     */
    private void computeTrianglesNormals()
    {
        triNormals = new Vector3f[ paths.length ];

        for ( int i = 0; i < paths.length; i++ )
            computeNormal( i );
    }

    /*
     * Computes normal of the triangle
     * 
     * @param tri	triangle indice
     */
    private void computeNormal( int tri )
    {
        Point3f point0 = points[ paths[ tri ][ 0 ] ];
        Point3f point1 = points[ paths[ tri ][ 1 ] ];
        Point3f point2 = points[ paths[ tri ][ 2 ] ];
        Vector3f edge1 = new Vector3f( point1.x - point0.x, point1.y - point0.y, point1.z - point0.z );
        Vector3f edge2 = new Vector3f( point2.x - point1.x, point2.y - point1.y, point2.z - point1.z );
        Vector3f normal = new Vector3f();
        normal.cross( edge1, edge2 );
        normal.normalize();
        this.triNormals[ tri ] = normal;
    }

    /*
     * Computes intersection Triangle-Line.
     * 
     * @param	tri							triangle indice
     * @param	direction					direction of the line element
     * @param	origin						origin of the line element
     * @param	cullingDesired				true if result depends of triangle orientation (i.e. if triangle is back, there is no intersection). Faster code.
     * @param	allowNegativeLengths		true if result depends of line orientation (i.e. allow intersection point to be back to the line element origin).
     * @return	IntersectionContext (length: distance between ray origin and ray impact, boolean: true if there is an intersection, false if not)
     */
    private IntersectionContext computeIntersection( int tri, Vector3f direction, Point3f origin, boolean cullingDesired, boolean allowNegativeLengths )
    {
        /* **************************************************************************************************
        	Following code inspired by the paper :
        	"Fast, Minimum Storage Ray/Triangle intersection" from Ben Trumbore && Thomas Möller
        	(Program of Computer Graphics, Cornell University / Prosolvia Clarus AB, Chalmers University of Technology)
        
        	Link: http://www.graphics.cornell.edu/pubs/1997/MT97.html
        ****************************************************************************************************/

        // default length
        float length = Float.MAX_VALUE;

        // find triangle vertex
        Point3f point0 = points[ paths[ tri ][ 0 ] ];
        Point3f point1 = points[ paths[ tri ][ 1 ] ];
        Point3f point2 = points[ paths[ tri ][ 2 ] ];

        // find vectors for two edges sharing vertex0
        Vector3f edge1 = new Vector3f( point1.x - point0.x, point1.y - point0.y, point1.z - point0.z );
        Vector3f edge2 = new Vector3f( point2.x - point0.x, point2.y - point0.y, point2.z - point0.z );

        // begin computing determinant - also used to calculate u
        Vector3f pvec = new Vector3f();
        pvec.cross( direction, edge2 );

        // if determinant is zero, ray lies in plane of triangle
        float det = edge1.dot( pvec );
        if ( cullingDesired == true )
        { // if culling is desired
            if ( det < 0 )
                return new IntersectionContext( false, length );

            // calculate distance from vert0 to ray origin
            Vector3f tvec = new Vector3f();
            tvec.sub( origin, point0 );

            // calculate u and test bounds
            float u = tvec.dot( pvec );
            if ( u < 0 || u > det )
                return new IntersectionContext( false, length );

            // prepare to test v
            Vector3f qvec = new Vector3f();
            qvec.cross( tvec, edge1 );

            // computing v and test bounds
            float v = qvec.dot( direction );
            if ( v < 0 || u + v > det )
                return new IntersectionContext( false, length );

            // calculate length, ray intersects triangle
            length = edge2.dot( qvec );
            length /= det;
        }
        else
        { // the non-culling branch
            if ( det == 0 )
                return new IntersectionContext( false, length );

            // invert of determinant
            float inv_det = 1.0f / det;

            // calculate distance from vert0 to ray origin
            Vector3f tvec = new Vector3f();
            tvec.sub( origin, point0 );

            // calculate u and test bounds
            float u = tvec.dot( pvec );
            u *= inv_det;
            if ( u < 0.0f || u > 1.0f )
                return new IntersectionContext( false, length );

            // prepare to test v
            Vector3f qvec = new Vector3f();
            qvec.cross( tvec, edge1 );

            // calculate v and test bounds
            float v = qvec.dot( direction );
            v *= inv_det;
            if ( v < 0.0f || u + v > 1.0f )
                return new IntersectionContext( false, length );

            // calculate length, ray intersects triangle
            length = edge2.dot( qvec );
            length *= inv_det;
        }

        if ( allowNegativeLengths == true )
            return new IntersectionContext( true, length );

        if ( length > 0.0f )
            return new IntersectionContext( true, length );

        return new IntersectionContext( false, length );
    }

    /*
     * Tests if triangle is intersected by the specified line element
     * 
     * @param	tri						triangle indice
     * @param	lineElement				line element
     * @return	true if triangle is intersected by the line element. false if not.
     */
    private boolean isIntersectedBy( int tri, LineElement lineElement )
    {
        if ( lineElement instanceof Line )
        {
            // allow negative distances
            return computeIntersection( tri, lineElement.getDirection(), lineElement.getOrigin(), culling, true ).intersect;
        }
        else if ( lineElement instanceof HalfLine )
        {
            // dont allow negative distances
            return computeIntersection( tri, lineElement.getDirection(), lineElement.getOrigin(), culling, false ).intersect;
        }
        // dont allow negative distance and compare length vs distance
        IntersectionContext intersectionContext = computeIntersection( tri, lineElement.getDirection(), lineElement.getOrigin(), culling, false );

        if ( lineElement.getLength() > intersectionContext.length && intersectionContext.intersect == true )
            return true;
        return false;
    }

    /*
     *  Manage intersectionContext to return a correct Intersection 
     * (depending on instance of lineElement, e.g. test for segment is different from test for half-line)
     * 
     * @param	tri							triangle indice
     * @param	lineElement					line element
     * @param	allowNegativeLengths		allow intersection point to be back to the line element origin
     * @return	computed intersection (null if empty)
     */
    private Intersection computeIntersection( int tri, LineElement lineElement, boolean allowNegativeLengths )
    {
        IntersectionContext intersectionContext = computeIntersection( tri, lineElement.getDirection(), lineElement.getOrigin(), culling, allowNegativeLengths );
        if ( intersectionContext.intersect )
        {
            float distance = intersectionContext.length;
            //boolean side = linel.getDirection ().dot (normal)>0;
            //return new Intersection(point,normal,side);
            if ( lineElement instanceof LineSegment )
                if ( lineElement.getLength() < intersectionContext.length )
                    return null;

            return new Intersection( distance, triNormals[ tri ] );
        }
        return null;
    }

    /*
     * Get the intersection of the triangle with the line element
     * 
     * @param	tri							triangle indice
     * @param	lineElement					line element
     * @return	Intersection
     */
    private Intersection getTriangleIntersection( int tri, LineElement lineElement )
    {
        if ( lineElement instanceof LineSegment || lineElement instanceof HalfLine )
        {
            // dont allow negative distances
            return computeIntersection( tri, lineElement, false );
        }
        // allow negative distances
        return computeIntersection( tri, lineElement, true );
    }

    /**
     * Get a list of the triangles of the mesh.
     * @return 
     */
    public List<Triangle> getTriangles()
    {
        List<Triangle> triangles = new ArrayList<>();

        //        int triangleID = 0;

        for ( int[] path : super.paths )
        {
            Triangle triangle = new Triangle( new Point3f( super.points[ path[ 0 ] ] ), new Point3f( super.points[ path[ 1 ] ] ), new Point3f( super.points[ path[ 2 ] ] ) );
            triangles.add( triangle );
            //            triangleID++;
        }

        //normalsInitialized = true;

        return triangles;
    }

    public Triangle getTriangle( int triangleID )
    {
        return new Triangle( new Point3f( points[ paths[ triangleID ][ 0 ] ] ), new Point3f( points[ paths[ triangleID ][ 1 ] ] ), new Point3f( points[ paths[ triangleID ][ 2 ] ] ) );
    }

    /**
     * Computes the barycenter of the mesh points (different from area
     * barycenter)
     *
     * @param mesh
     * @return barycenter
     */
    public static Point3f computePointsBarycentre( SimpleMesh mesh )
    {
        Point3f barycenter = new Point3f();
        for ( Point3f pt : mesh.getPoints() )
            barycenter.add( pt );
        barycenter.scale( 1 / (float)mesh.getPoints().length );

        return barycenter;
    }

    @Override
    public BoundingBox3f computeBoundingBox()
    {
        BoundingBox3f bbox = new BoundingBox3f();

        Point3f[] pts = getPoints();
        for ( Point3f p : pts )
            bbox.update( p );

        return bbox;
    }

    /**
     * Calculation of the barycentre of a triangulated mesh.
     * Defined as average of barycentres of the triangles of the mesh, each triangle barycentre being weighted by triangle area.
     * @return barycentre coordinates, null if mesh area is zero
     */
    public Point3f computeAreaWeigthedBarycentre()
    {
        float meshArea = 0;
        Point3f barycentre = new Point3f();

        for ( Triangle triangle : getTriangles() )
        {
            float s = triangle.computeArea();
            if ( s > 0 )
            {
                Point3f bary = triangle.getBarycentre();
                bary.scale( s );
                barycentre.add( bary );
                meshArea += s;
            }
        }

        if ( meshArea > 0 )
        {
            barycentre.scale( 1.0f / meshArea );
            return barycentre;
        }

        return null;
    }
}
