package fr.amap.archimed.formats;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OPSReader
{
    /**
     * type of line
     */
    public enum OPSRecordType
    {
    /**
     * "terrain" record (T xOrigin yOrigin zOrigin xSize ySize flat)
     */
    TERRAIN,

    /**
     * "plant" record (sceneId plantId plantFileName x y z scale inclinationAzimut inclinationAngle stemTwist)
     */
    PLANT,

    /**
     * "group" record (comment starting with #[Archimed] group:<group name>)
     */
    GROUP
    }

    public class OPSRecord
    {
        private OPSRecordType type;

        private String[] fields;

        public OPSRecord( OPSRecordType type, String[] fields )
        {
            this.type = type;
            this.fields = fields;
        }

        public OPSRecordType getType()
        {
            return type;
        }

        public String[] getFields()
        {
            return fields;
        }
    }

    private List<OPSRecord> records;

    public OPSReader( String fileName ) throws FileNotFoundException, IOException
    {
        records = new ArrayList<>();
        readOPS( fileName );
    }

    private void readOPS( String fileName ) throws FileNotFoundException, IOException
    {
        try (BufferedReader in = new BufferedReader( new FileReader( fileName ) ))
        {
            String line;

            do
            {
                line = in.readLine();
                if ( line != null )
                {
                    line = line.trim();
                    if ( line.length() > 0 )
                    {
                        String[] fields = line.split( "[ \t]+" );

                        if ( fields[ 0 ].equals( "#[Archimed]" ) )
                            records.add( new OPSRecord( OPSRecordType.GROUP, fields ) );
                        else if ( !fields[ 0 ].startsWith( "#" ) )
                        {
                            if ( fields[ 0 ].equals( "T" ) )
                                records.add( new OPSRecord( OPSRecordType.TERRAIN, fields ) );
                            else if ( fields.length == 10 )
                                records.add( new OPSRecord( OPSRecordType.PLANT, fields ) );
                        }
                    }
                }
            }
            while ( line != null );
        }
    }

    public List<OPSRecord> getRecords()
    {
        return records;
    }
}
