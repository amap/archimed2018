# retrouve la valeur du champ outputDirectory= dans un fichier
# in : $1 chemin du fichier
# out : valeur du champ, output-<process id> si $1 non renseigné
function get_output_dir()
{
  if [[ -z $1 ]]; then
    local res=output-$$
  else
    local prop_file=$1
    local res=$(grep outputDirectory $prop_file|cut -f2 -d=)
    if [[ -z $res ]]; then
      res=output-$$
    fi
  fi
  echo $res
}

# compare un répertoire de résultats attendus avec un répertoire de sortie
# vérifie que les fichiers présents dans le répertoire de référence sont présents et identiques dans le répertoire à vérifier
# si un répertoire "filters" est présent dans le répertoire de référence, les fichiers contenus sont exécutés pour filter les données à comparer (nécessaire si des dates ou informations variables mais non significatives peuvent perturber la comparaison)
# par ex, si le répertoire de référence s'appelle "ref" et le répertoire à vérifier "toto" et qu'un fichier ref/filters/log_filter est présent, il est appliqué sur ref/log et toto/log avant comparaison
# in : $1 répertoire de référence
#      $2 répertoire de sortie à vérifier
# out : code de retour, 0 si tous les fichiers comparés sont identiques, 1 sinon
function check_expected()
{
  local ref_dir=$1
  local checked_dir=$2

  # répertoire de travail (filtrage, etc...)
  rm -rf $ref_dir/tmp
  mkdir $ref_dir/tmp

  IFS=$'\n'

  local res=0
  local files=$(find $ref_dir \( -path "$ref_dir/tmp" -o -path "$ref_dir/filters" \) -prune -o -type f -print)
  for f in $files; do
    # reference file
    local ref_file=$f
    local ref_file_comp=$ref_file

    # remove the first dir of the path
    f="${f#*/}"

    local bf=$(basename $f)

    # checked (output) file
    local checked_file=$checked_dir/$f
    local checked_file_comp=$checked_file

    # apply filters ?
    local filter=$ref_dir/filters/"$bf"_filter

    # common filter for ref/output
    if [[ -f $filter ]]; then
      chmod u+x $filter

      ref_file_comp=$ref_dir/tmp/$bf
      $filter $ref_file $ref_file_comp

      checked_file_comp=$ref_dir/tmp/"$bf"_checked
      $filter "$checked_file" "$checked_file_comp"
    fi

    # reference filter
    filter=$ref_dir/filters/"$bf"_filter_ref
    if [[ -f $filter ]]; then
      chmod u+x $filter

      ref_file_comp=$ref_dir/tmp/$bf
      $filter "$ref_file" "$ref_file_comp"
    fi

    # output filter
    filter=$ref_dir/filters/"$bf"_filter_out
    if [[ -f $filter ]]; then
      chmod u+x $filter

      checked_file_comp=$ref_dir/tmp/"$bf"_checked
      $filter "$checked_file" "$checked_file_comp" "$ref_file"
    fi

    # compare
    comp_log=/tmp/check_expected-$$.log
    cmp $ref_file_comp $checked_file_comp >$comp_log 2>&1
    if (( $? != 0 )); then
      res=1
      more $comp_log
      echo "ref   : $ref_file_comp"
      echo "check : $checked_file_comp"
    fi
  done
  return $res
}

# chemin vers exécutable de l'appli
ARCHIMED_JAR=../../target/archimed-lib-2018-0.0.1-SNAPSHOT.jar
